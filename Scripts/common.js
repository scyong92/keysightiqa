﻿function GetLiveURL(page, url) {
    $(".content").prev().empty()
    $(".content").prev().append("<div class='row' style='font-size:50px;'>" + page + " page has moved to the official site. <br/>Please click this url <a href='http://manufacturing.png.is.keysight.com" + url + "'>http://manufacturing.png.is.keysight.com" + url + "</a> to proceed.</div>")
}

function GetData_SP(obj) {
    //if (globalDivision != undefined && globalDivision[0] == "8") {
    //    return PostData_Ajax(obj.wMsg, "/LTS/api/Global/" + obj.api, true);
    //} else {
    return PostData_Ajax(obj.wMsg, "/api/Common/" + obj.api, true);
    //}
}

function GetData_Ajax(obj, sync) {

    $("#" + obj.id).addClass("loader2");
    $("#" + obj.id).css("min-height", "400px");

    var start = new Date();
    var query = queryBuilder(obj);
    return $.ajax({
        type: "get",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: obj.url,
        async: sync == undefined ? true : sync,
        success: function (object) {
            //$spinner.removeClass('loader');
            $("#" + obj.id).removeClass("loader2");
            $("#" + obj.id).css("min-height", "");
            //var responseTime = (new Date() - start) / 1000;
            if (object.Status != -1) {
                //logWebAPI(obj.url, query, "GET", responseTime, "");
            }
            else {
                //logWebAPI(obj.url, query, "GET", responseTime, object.Description);
                //sendMail(ErrorEmailContent(obj.url, JSON.stringify(object)));
            }
            return object;
        },
        error: function (object) {
            $("#" + obj.id).removeClass("loader2");
            $("#" + obj.id).css("min-height", "");

            if (object.statusText != "abort") {
                //var responseTime = (new Date() - start) / 1000;
                //logWebAPI(obj.url, query, "GET", responseTime, JSON.stringify(object));
                ShowNotification(object.status + ' ' + object.statusText, "error");
                //sendMail(ErrorEmailContent(obj.url, JSON.stringify(object)));
            }

            return object;
        },
        beforeSend: function (jqXHR, settings) { // before jQuery send the request we will push it to our array
            if (settings.url != "/api/AdminPanel/AcknowledgementTable/" && settings.url != "/api/General/GetNotificationLog") {
                $.xhrPool.push(jqXHR);
            }
        },
        complete: function (jqXHR) { // when some of the requests completed it will splice from the array
            var index = $.xhrPool.indexOf(jqXHR);
            if (index > -1) {
                $.xhrPool.splice(index, 1);
            }
        }
    });
}

function PostData_Ajax(wMsg, url, sync) {
    if (globalDivision != undefined && globalDivision[0] == "8" && wMsg.Body.wQuery != undefined) {
        //if (url.indexOf("Raw/") != -1) {
        //    url = "/LTS" + url.replace("Raw/", "LTSRaw/");
        //} else {
        //    url = "/LTS" + url.replace("/LEAN/api/FirstPassYield/", "/api/Xcadia/").replace("/ANALYTICS/API/FailureTest/", "/api/Xcadia/").replace("/ANALYTICS/API/UnitHistory/", "/api/Xcadia/").replace("/ANALYTICS/API/Process/", "/api/Xcadia/");
        //}

        var tableName = wMsg.Body.wQuery.TableOrViewName;
        switch (tableName) {
            //case "mc_petafield_global_taxonomy_view":
            //    wMsg.Body.wQuery.TableOrViewName = "lts_test_raw_data_model_view";
            //    break;
            case "mf_first_pass_yield_raw_completion_view":
                wMsg.Body.wQuery.TableOrViewName = "lts_first_pass_yield_raw_completion_view";
                break;
            case "mf_first_pass_yield_raw_completion2_view":
                wMsg.Body.wQuery.TableOrViewName = "lts_first_pass_yield_raw_completion_view";
                break;
            case "mf_test_data_raw_view":
                wMsg.Body.wQuery.TableOrViewName = "lts_test_data_raw_view";
                break;
            case "mf_test_raw_data_model_view":
                wMsg.Body.wQuery.TableOrViewName = "lts_test_raw_data_model_view";
                break;
            case "mf_test_data_raw_process1":
                wMsg.Body.wQuery.TableOrViewName = "lts_test_data_raw_process1_view";
                break;
            case "mf_test_data_raw_process1_view":
                wMsg.Body.wQuery.TableOrViewName = "lts_test_data_raw_process1_view";
                break;
            case "mf_test_data_raw_process2_aging_view":
                wMsg.Body.wQuery.TableOrViewName = "lts_test_data_raw_process2_aging_view";
                break;
        }
    }
    //if (globalDivision == 7 && (getUrlParameter("MenuId") == "menuDashProdLDB" || getUrlParameter("MenuId") == "menuDashProdCYA" || getUrlParameter("MenuId") == "menuDashProdLT" || getUrlParameter("MenuId") == "menuDashProdWIPA" || getUrlParameter("MenuId") == "menuDashProdCSU")) {
    //    if (wMsg.Body.wQuery == undefined) {
    //        if (wMsg.Body.monthly < "2018-05") {
    //            wMsg.Body.monthly = "";
    //        }
    //    }
    //    else {
    //        for (var i = 0; i < wMsg.Body.wQuery.Filters.length; i++) {
    //            if (wMsg.Body.wQuery.Filters[i].ColumnName == "Date_Time" || wMsg.Body.wQuery.Filters[i].ColumnName == "TestDateTime") {
    //                if (new Date(wMsg.Body.wQuery.Filters[i].Criterias[0].Value) < new Date("05/01/2018")) {
    //                    wMsg.Body.wQuery.Filters[i].Criterias[0].Value = "05/01/2018";
    //                    break;
    //                }
    //            }
    //        }
    //    }
    //}
    var start = new Date();
    var query = queryBuilder(wMsg.Body);
    return $.ajax({
        type: "post",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        crossDomain: true,
        data: JSON.stringify(wMsg),
        url: url,
        async: sync == undefined ? true : sync,
        success: function (object) {
            //$spinner.removeClass('loader');
            //var responseTime = (new Date() - start) / 1000;
            if (object.Status != -1) {
                //logWebAPI(url, query, "POST", responseTime, "");
                //ShowNotification(object.Description, "success");
                return false;
            }
            else {
                //logWebAPI(url, query, "POST", responseTime, object.Description);
                //sendMail(ErrorEmailContent(url, JSON.stringify(object)));
                return object;
            }
        },
        error: function (object) {
            $spinner.removeClass('loader');

            if (object.statusText != "abort") {
                //var responseTime = (new Date() - start) / 1000;
                //logWebAPI(url, query, "POST", responseTime, JSON.stringify(object));
                ShowNotification(object.status + ' ' + object.statusText, "error");
                //sendMail(ErrorEmailContent(url, JSON.stringify(object)));
            }
            return object;
        },
        beforeSend: function (jqXHR) { // before jQuery send the request we will push it to our array
            $.xhrPool.push(jqXHR);
        },
        complete: function (jqXHR) { // when some of the requests completed it will splice from the array
            var index = $.xhrPool.indexOf(jqXHR);
            if (index > -1) {
                $.xhrPool.splice(index, 1);
            }
        }
    });
}

function ErrorEmailContent(api, error) {
    var reqMsg = new gSendMailMsg();
    reqMsg.To.push("thean-sze.cheah6@keysight.com");
    reqMsg.Subject = "Error Notification";
    reqMsg.Body = "PageURL: " + window.location.href
        + "<br>API: " + api
        + "<br>Division: " + (globalDivision != undefined ? globalDivision[0] : "")
        + "<br>User: " + ($("#myP").text().trim() == "" ? "Guest" : $("#myP").text().trim())
        + "<br>Description: " + error;
    return reqMsg;
}

function gSendMailMsg() {
    this.To = [];
    this.Cc = [];
    this.Bcc = [];
    this.Attach = [];
    this.Body = "";
    this.Subject = "";
}

function sendMail(reqMsg) {
    if (reqMsg.Body.indexOf('{"readyState":0,"responseText":"","status":0,"statusText":"error"}') == -1) {
        $.ajax({
            type: "post",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(reqMsg),
            url: "/api/Mail/ExternalSendEmail",
            async: true,
            success: function (valid) {
                if (valid) {
                    //show that id is valid
                } else {
                    //show that id is not valid
                }
            }
        });
    }
}

function GetData(obj, sync) {
    return Petafield_BLL_Proxy.SendMsg(obj.query, obj.APIModuleName, "POST", sync);
}

function operator(op) {
    var obj = { eq: "=", ne: "<>", gt: ">", ge: ">=", lt: "<", le: "<=" };
    return obj[op];
}

function queryBuilder(obj) {
    if (obj.wQuery != undefined && obj.wQuery.Filters != undefined) {

        var query = "select " + obj.wQuery.Select + " from " + obj.wQuery.TableOrViewName;
        if (obj.wQuery.Filters.length > 0) {
            query += " where ";
        }
        var where = "";
        for (var i = 0; i < obj.wQuery.Filters.length; i++) {
            if (where != "") {
                where += " and ";
            }
            var criteria = "(";
            for (var j = 0; j < obj.wQuery.Filters[i].Criterias.length; j++) {
                if (criteria != "(") {
                    criteria += " or ";
                }
                criteria += "[" + obj.wQuery.Filters[i].ColumnName + "] " + operator(obj.wQuery.Filters[i].Criterias[j].Command) + " '" + obj.wQuery.Filters[i].Criterias[j].Value + "'";
            }
            criteria += ")";
            where += criteria;
        }
        return query + where;
    }
    else if (obj.url != undefined) {
        return obj.url;
    }
    else {
        return JSON.stringify(obj);
    }
}

function logWebAPI(webapi, strMsg, method, responseTime, error) {
    var wMsg = new wMessage();
    var content = {};
    content.PageUrl = window.location.href;
    content.Port = window.location.port;
    content.WebAPI = webapi;
    content.SqlQuery = webapi == "/api/Account/NTSignIn/" ? "" : queryBuilder(strMsg);
    content.Remark = error;
    content.Method = method;
    content.TimeTaken_Seconds = responseTime;
    content.Theme = theme;
    wMsg.Body = content;

    if (window.location.port != "54324" && (content.SqlQuery.indexOf("GetAmbientReading") == -1 || content.Remark != "")) {
        $.ajax({
            type: "post",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(wMsg),
            url: "/api/Log/LogWebAPI",
            async: true,
            success: function (valid) {
                if (valid) {
                    //show that id is valid
                } else {
                    //show that id is not valid
                }
            }
        });
    }
}

function CheckData(obj) {
    if (obj.data.Status == 1) {
        obj.data = obj.data.Body;

        if (obj.data.length != undefined && obj.data.length != 0) {
            return true;
        }
        else if (obj.data.TotalRecords != 0 && obj.data.length != 0) {
            return true;
        }
        else {
            if (typeof $FromDate != "undefined" && typeof $ToDate != "undefined") {
                obj.description = "No Record Found within " + $FromDate + " - " + $ToDate;
            }
            else if (typeof $FromMonth != "undefined" && typeof $ToMonth != "undefined") {
                obj.description = "No Record Found within " + $FromMonth + " - " + $ToMonth;
            }
            else if (obj.data.TotalRecords == 0 || obj.data.length == 0) {
                obj.description = "No Record Found";
            }
            return false;
        }
    }
    else {
        obj.description = obj.data.Description;
        return false;
    }
}

function RenderChart(obj) {
    if (obj.id == undefined) {
        ShowNotification("Undefined ID!", "error");
    }

    Highcharts.setOptions({
        chart: {
            zoomType: 'xy',
            styledMode: obj.styledMode == false ? false : true
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false,
            chartOptions: {
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                if (this.y != 0) {
                                    return this.y;
                                }
                            }
                        }
                    }
                },
                subtitle: {
                    text: ' '
                }
            },
            fallbackToExportServer: false
        }
    });

    if (typeof obj.id === 'number') {
        if (obj.options.plotOptions != undefined) {
            if (obj.options.plotOptions.series != undefined) {
                if (obj.options.plotOptions.series.point != undefined) {
                    delete obj.options.plotOptions.series.cursor;
                    delete obj.options.plotOptions.series.point;
                }

            }
            else if (obj.options.plotOptions.column != undefined) {
                if (obj.options.plotOptions.column.point != undefined) {
                    delete obj.options.plotOptions.column.cursor;
                    delete obj.options.plotOptions.column.point;
                }
            }
        }
    }
    $('#' + obj.id).highcharts(obj.options);

    //obj.query = JSON.stringify(obj.query);
    $($("#" + obj.id).closest(".panel.panel-default")).data("object", obj);
}

//render table
function RenderTable(obj) {
    if (obj.id == undefined) {
        ShowNotification("Undefined ID!", "error");
    }

    if (obj.data.Records != undefined) {
        data = obj.data.Records;
    }
    else {
        data = obj.data;
    }

    if (obj.note != undefined) {
        $("#" + obj.id).append(obj.note);
    }

    var table = document.createElement("table");
    table.id = "table" + obj.id;
    table.className = "table table-bordered table-condensed table-responsive";
    var thead = document.createElement("thead");
    var tr = document.createElement("tr");
    tr.className = "bg_category";
    var columns = data[0];
    for (var column in columns) {
        var th = document.createElement("th");
        th.innerHTML = column;
        tr.appendChild(th);
    }

    thead.appendChild(tr);
    table.appendChild(thead);

    var tbody = document.createElement("tbody");

    for (var i = 0; i < data.length; i++) {
        var tr2 = document.createElement("tr");
        var columns = data[i];
        for (var column in columns) {
            var td = document.createElement("td");
            td.innerHTML = columns[column] != null && columns[column].toString().indexOf("T00:00:00") != -1 ? columns[column].replace("T00:00:00", "") : columns[column];
            tr2.appendChild(td);
        }
        tbody.appendChild(tr2);
    }

    table.appendChild(tbody);

    var div = document.getElementById(obj.id);
    div.appendChild(table);

    if (obj.clickable != undefined) {
        $("#table" + obj.id).addClass("table-hover");
    }
    var tableWidth = $("#table" + obj.id).width();
    var containerWidth = $($("#" + obj.id).closest(".panel.panel-default")).width();

    if (tableWidth > containerWidth) {
        $($("#" + obj.id).closest(".panel.panel-default")).css("overflow", "auto");
    }

    //obj.query = JSON.stringify(obj.query);
    $($("#" + obj.id).closest(".panel.panel-default")).data("object", obj);
}

function RenderBootstrapTable(obj) {
    if (obj.id == undefined) {
        ShowNotification("Undefined ID!", "error");
    }

    if (obj.data.Records != undefined) {
        data = obj.data.Records;
    }
    else {
        data = obj.data;
    }

    if (obj.note != undefined) {
        $("#" + obj.id).append(obj.note);
    }

    var table = document.createElement("table");
    table.id = "table" + obj.id;
    if (obj.displayPagination != undefined) {
        table.setAttribute("data-pagination", obj.displayPagination);
    } else {
        table.setAttribute("data-pagination", "true");
    }
    table.setAttribute("data-page-list", "[10, 25, 50, 100, 500]");
    table.setAttribute("data-page-size", 10);
    table.setAttribute("data-search", "true");
    if (obj.groupBy != undefined) {
        table.setAttribute("data-group-by", true);
        table.setAttribute("data-group-by-field", obj.groupBy);
    }
    if (obj.columnFilter != undefined) table.setAttribute("data-show-columns", true);
    if (obj.sortName != undefined) table.setAttribute("data-sort-name", obj.sortName);
    if (obj.sortOrder != undefined) table.setAttribute("data-sort-order", obj.sortOrder);
    var thead = document.createElement("thead");
    var tr = document.createElement("tr");

    if (obj.type == "table-checkbox") {
        var th = document.createElement("th");
        th.setAttribute("data-field", "state");
        th.setAttribute("data-checkbox", "true");
        th.setAttribute("data-formatter", "stateFormatter");
        tr.appendChild(th);
        var th2 = document.createElement("th");
        th2.setAttribute("data-formatter", "actionFormatter");
        th2.setAttribute("data-events", "actionEvents");
        th2.innerHTML = "Action";
        tr.appendChild(th2);
    }
    //else {
    //var th = document.createElement("th");
    //th.setAttribute("data-field", "number");
    //th.setAttribute("data-sortable", "true");
    //th.innerHTML = "#";
    //tr.appendChild(th);
    //}
    if (obj.action != undefined) {
        var th2 = document.createElement("th");
        th2.setAttribute("data-formatter", "actionFormatter");
        th2.setAttribute("data-events", "actionEvents");
        th2.innerHTML = "Action";
        tr.appendChild(th2);
    }

    var columns = data[0];
    for (var column in columns) {
        var th2 = document.createElement("th");
        th2.setAttribute("data-field", column);
        th2.setAttribute("data-sortable", "true");
        if (obj.hideColumn != undefined) {
            if (obj.hideColumn.indexOf(column) > -1) {
                th2.setAttribute("data-visible", "false");
            }
        }
        if (column.toLowerCase() == "url" || column.toLowerCase() == "link") {
            th2.setAttribute("data-searchable", "false");
        }
        th2.innerHTML = wordCaseSlicer(column.replace('_', ' '));
        tr.appendChild(th2);
    }

    thead.appendChild(tr);
    table.appendChild(thead);
    var div = document.getElementById(obj.id);
    div.appendChild(table);

    for (var i = 0, m = data.length; i < m; i++) {
        var columns = data[i];
        for (var column in columns) {
            if (column.indexOf("Date") != -1 && data[i][column] != null) {
                var date = new Date(data[i][column]);
                if (date != "Invalid Date") {
                    data[i][column] = GetDateFormat(date);
                }
            }
        }
    }

    $("#table" + obj.id).bootstrapTable();
    $("#table" + obj.id).bootstrapTable('load', data);
    $("#table" + obj.id).bootstrapTable('hideLoading');

    if (obj.class == undefined) {
        $("#table" + obj.id + " thead tr").addClass("bg_category");
    }
    else {
        $("#table" + obj.id + " thead tr").addClass(obj.class);
    }


    if (obj.clickable != undefined) {
        $("#table" + obj.id).addClass("table-hover");
    }
    var tableWidth = $("#table" + obj.id).width();
    var containerWidth = $($("#" + obj.id).closest(".panel.panel-default")).width();

    if (tableWidth > containerWidth) {
        $($("#" + obj.id).closest(".panel.panel-default")).css("overflow", "auto");
    }

    //obj.query = JSON.stringify(obj.query);
    $($("#" + obj.id).closest(".panel.panel-default")).data("object", obj);

    HideEmptyColumn("#table" + obj.id);


    if (obj.showExport != undefined) {
        var btnExport = '<div class="export btn-group" onclick="exportExcel(this)" title="Export to Excel">'
            + '<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button">'
            + '<i class="glyphicon glyphicon-export"></i>'
            + '</button>'
            + '</div>';
        $("#" + obj.id + " .fixed-table-toolbar .columns-right").append(btnExport);
    }
}

function rowSpan(tableId, col) {
    var table = $("#" + tableId);
    var rows = table.find($("tr"));
    var colsLength = col;
    var removeLater = new Array();
    for (var i = 0; i < colsLength; i++) {
        var startIndex = 0;
        var lastIndex = 0;
        var startText = $($(rows[0]).find("td")[i]).text();
        for (var j = 1; j < rows.length; j++) {
            var cRow = $(rows[j]);
            var cCol = $(cRow.find("td")[i]);
            var currentText = cCol.text();
            if (currentText == startText) {
                cCol.css("background", "gray");
                removeLater.push(cCol);
                lastIndex = j;
            } else {
                var spanLength = lastIndex - startIndex;
                if (spanLength >= 1) {
                    $($(rows[startIndex]).find("td")[i]).attr("rowspan", spanLength + 1);
                }
                lastIndex = j;
                startIndex = j;
                startText = currentText;
            }

        }
        var spanLength = lastIndex - startIndex;
        if (spanLength >= 1) {
            $($(rows[startIndex]).find("td")[i]).attr("rowspan", spanLength + 1);
        }
    }

    for (var i in removeLater) {
        $(removeLater[i]).remove();
    }

}

function HideEmptyColumn(id) {
    var data = $(id).bootstrapTable('getData');
    var columns = data[0];
    for (var column in columns) {
        var notEmpty = data.filter(function (obj) { return obj[column] != '-' && obj[column] != null; }).length;
        if (notEmpty == 0) {
            $(id).bootstrapTable('hideColumn', column);
        }
    }
}

function CreateContainer(obj) {
    //var data;
    //if (obj.data.Records != undefined) {
    //    data = obj.data.Records;
    //}
    //else {
    //    data = obj.data
    //}

    if (obj.id == undefined) {
        ShowNotification("Undefined ID!", "error");
    }

    if ($("#" + obj.id).length == 0) {
        if (obj.size == undefined) {
            obj.size = "col-md-10";
        }
        var div = "";
        div += "<div class=\"" + obj.size + "\">";
        div += "<div class=\"panel panel-default\">";
        div += "<div class=\"panel-heading\" >";
        div += "<div class=\"dropdown\" style=\"float:right;\"></div>";
        div += "<div id=\"" + obj.id + "headerName\"><h4><b>" + obj.title + "</b></h4></div>";
        div += "</div>"; //panel-heading
        div += "<div class=\"panel-body\">";
        if (obj.type == "table") div += "<div id=\"" + obj.id + "\" class=\"table-responsive\"></div>";
        else div += "<div id=\"" + obj.id + "\"></div>";
        if (obj.option == "checkbox") div += "<div id=\"" + obj.option + "\"></div>";
        div += "</div>"; //panel-body
        div += "</div>"; //panel panel-default
        div += "</div>"; //obj.size

        var rowNo = $(".panel-row").length;
        var contentNo = 0;
        if (rowNo > 0) contentNo = $($(".panel-row")[rowNo - 1]).children().length;

        //only have 1 panel if size is 10 or 12
        if ($($(".panel-row")[rowNo - 1]).children('div[class$=10]').length == 1 || $($(".panel-row")[rowNo - 1]).children('div[class$=12]').length == 1) {
            var rowFull = true;
        }
        //have 2 panels if size is 5 or 6
        else if ($($(".panel-row")[rowNo - 1]).children('div[class$=5]').length == 2 || $($(".panel-row")[rowNo - 1]).children('div[class$=6]').length == 2) {
            var rowFull = true;
        }
        else if ($($(".panel-row")[rowNo - 1]).children('div[class$=4]').length == 3 || $($(".panel-row")[rowNo - 1]).children('div[class$=3]').length == 4) {
            var rowFull = true;
        }
        if (rowNo == 0 || rowFull || !$($(".panel-row")[rowNo - 1]).children().hasClass(obj.size)) {
            divRow = "<div class=\"panel-row\">";
            divRow += div;
            divRow += "</div>"; //panel-row
            $(".content").append(divRow);
        } else {
            $($(".panel-row")[rowNo - 1]).append(div);
        }

        //if (obj.id != "TestRunResultChart" && obj.id != "StationChart") {
        //$(".content").append(div);
        //}
        //else {
        //    $(".content").prepend(div);
        //}

        //if (data.length > 0) {
        //if (obj.id != "TestRunResultChart" && obj.id != "StationChart") {
        $($(".panel.panel-default")[$(".panel.panel-default").length - 1]).data("object", obj);
        //}
        //else {
        //    $($(".panel.panel-default")[1]).data("object", obj);
        //}

        if (obj.save == undefined) {
            ReloadDropDownMenu(obj);
        }

        //}
        //else {
        //    $("#" + obj.id).empty();
        //    $("#" + obj.id).append("<h4><span style=\"color:red; font-size:large;\">No Record Found.</span></h4>")
        //}
    }
    else {
        $("#" + obj.id + "headerName").empty();
        $("#" + obj.id + "headerName").append("<h4><b>" + obj.title + "<b></h4>");
        $("#" + obj.id).empty();
        //$("#" + obj.id).addClass("loader2");
    }
}

function ReloadDropDownMenu(object) {
    //var obj = {};
    //obj.url = "/api/Common/GetDashboardGroup";
    //GetData_Ajax(obj).then(function (obj) {
    //    if (obj.Status == -1) {
    //        ShowNotification(obj.Description, "error");
    //    }
    //    else {
    var container = $($("#" + object.id).closest(".panel.panel-default"));

    var dropdownMenu = "<button class='btn btn-dropdown dropdown-toggle' type='button' data-toggle='dropdown'>"
    dropdownMenu += "<span class='caret'></span>";
    dropdownMenu += "</button>";
    dropdownMenu += "<ul class='dropdown-menu multi-level' style='right:0;left:auto;'>";

    //dropdownMenu += "<li class='dropdown-submenu'>";

    //dropdownMenu += "<button class='buttonMenu' disabled tabindex='-1'>Save to </button>";
    //dropdownMenu += "<ul class='dropdown-menu'>";
    //dropdownMenu += "<li class='dropdown-submenu'>";

    //dropdownMenu += "<button class='buttonMenu' title='My Dashboard menu is located inside the username menu which at right top of the page' disabled>My Dashboard</button>";
    //dropdownMenu += "<ul class='dropdown-menu dashboardGroup'></ul>";
    //dropdownMenu += "</li>";
    //dropdownMenu += "</ul>";
    //dropdownMenu += "</li>";



    if (object.type == "table" || object.type == "customize_table") {
        dropdownMenu += "<li><button class='buttonMenu' onclick=\"exportExcel(this)\">Export to Excel</button></li>";
    }
    else if (object.type == "chart") {
        dropdownMenu += "<li><button class='buttonMenu' onclick=\"exportPNG(this)\">Export to JPG</button></li>";
    }
    else if (object.type == "chart_table") {
        dropdownMenu += "<li><button class='buttonMenu' onclick=\"exportPNG(this)\">Export to JPG</button></li>";
        dropdownMenu += "<li><button class='buttonMenu' onclick=\"exportExcel(this)\">Export to Excel</button></li>";
    }
    else if (object.type == "customize_tableExcel") {
        dropdownMenu += "<li><button class='buttonMenu' onclick=\"exportTableFinder(this)\">Export to Excel</button></li>";

    }
    dropdownMenu += "</ul>";
    container.find(".dropdown").append(dropdownMenu);


    //for (var i = 0; i < obj.Body.length; i++) {
    //    var page = JSON.parse(obj.Body[i]);
    //    var group = page[0].group;
    //    container.find(".dashboardGroup").append("<li><a onclick=\"save(this, 'Dashboard', '" + page[0].group + "')\">" + page[0].group + "</a></li>");
    //}

    //container.find(".dashboardGroup").append("<button class='buttonMenu' onclick=\"newGroupItem(this, 'Dashboard')\">Save to New Group</button>");
    //    }
    //});
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
function RenderDynamicTable(obj) {
    var data = obj.data;

    var table = document.createElement("table");
    table.id = "table";
    table.setAttribute("data-sort-order", "acs");
    table.setAttribute("data-search", "true");
    table.setAttribute("data-toolbar", "#toolbar");
    table.setAttribute("data-pagination", "true");
    table.setAttribute("data-query-params", "queryParams");

    var thead = document.createElement("thead");
    var tr = document.createElement("tr");
    tr.class = "bg_category";

    if (obj.checkbox) {
        var th = document.createElement("th");
        th.setAttribute("data-field", "state");
        th.setAttribute("data-checkbox", "true");
        th.setAttribute("data-formatter", "stateFormatter");
        tr.appendChild(th);
    }
    if (obj.actionFormatter) {
        var th2 = document.createElement("th");
        th2.setAttribute("data-formatter", "actionFormatter");
        th2.setAttribute("data-events", "actionEvents");
        th2.innerHTML = "Action";
        tr.appendChild(th2);
    }

    for (var i = 0; i < data.length; i++) {
        if (obj.DisplayColumn.indexOf(data[i].column_name) != -1 || data[i].column_name == "Id") {
            var th3 = document.createElement("th");
            th3.setAttribute("data-field", data[i].column_name);
            th3.setAttribute("data-sortable", "true");
            th3.innerHTML = wordCaseSlicer(data[i].column_name);
            tr.appendChild(th3);
        }
    }

    thead.appendChild(tr);
    table.appendChild(thead);
    var div = document.getElementById("tableData");
    div.appendChild(table);

    GetData_Ajax(obj, false).then(function (data) {
        if (data.Status == -1) {
            ShowNotification(data.Description, "error");
        }
        else {
            $("#table").bootstrapTable();
            $("#table").bootstrapTable('load', data);
            $("#table").bootstrapTable('hideLoading');
            if (obj.clickable != undefined) {
                $("#table" + obj.id).addClass("table-hover");
            }
            var tableWidth = $("#table" + obj.id).width();
            var containerWidth = $($("#" + obj.id).closest(".panel.panel-default")).width();

            if (tableWidth > containerWidth) {
                $($("#" + obj.id).closest(".panel.panel-default")).css("overflow", "auto");
            }

            obj.query = JSON.stringify(obj.query);
            $($("#" + obj.id).closest(".panel.panel-default")).data("object", obj);
        }
    });
}

function RenderInputField(obj) {
    return GetData_Ajax(obj).then(function (data) {
        if (data.Status == -1) {
            ShowNotification(data.Description, "error");
        }
        else {
            var obj = JSON.parse(data.Body[0].criteria);
            newJson = obj;
            obj.id = "form";
            //if ($("#" + obj.id + " .form-group").length == 0 || $("#form .glyphicon-edit").length == 0) {
            //$("#form").empty();
            RenderForm(obj);
            var count = 1;
            $(".section").each(function () {
                $(this).attr("id", "section" + count);
                $(this).addClass("collapse");
                $(this).addClass("in");
                //$(this).prev().attr("data-toggle", "collapse");
                //$(this).prev().attr("data-target", "#section" + count);
                $(this).prev().append('<i class="fa fa-angle-down rotate-icon" data-toggle="collapse" data-target="#section' + count + '"></i><i class="glyphicon glyphicon-edit"></i>');
                count++;
            });
            //}
        }
    });
}

function RenderForm(obj) {
    var title = "-";
    var a = document.getElementById(obj.id);

    for (var i = 0; i < obj.Title.split('<br/>').length; i++) {
        var h3 = document.createElement("h3");
        var b = document.createElement("b");
        b.appendChild(document.createTextNode(obj.Title.split('<br/>')[i]));
        h3.appendChild(b);
        a.appendChild(h3);
    }
    //$("#"+obj.id).prepend("<p><span style='float:right; color:red;'>* Mandatory Field (Please enter N/A if is no applicable)</span></p>");
    var section;
    for (var i = 0; i < obj.InputField.length; i++) {

        if (title != obj.InputField[i].ParentTitle) {
            if (title != "-") {
                //a.appendChild(section);
                a.appendChild(document.createElement("hr"));
            }
            section = document.createElement("div");
            section.className = "section";
            a.appendChild(section);

            var h3 = document.createElement("h3");
            h3.style.marginLeft = "100px";
            var b = document.createElement("b");
            var u = document.createElement("u");
            u.appendChild(document.createTextNode(obj.InputField[i].ParentTitle));
            b.appendChild(u);
            h3.appendChild(b);

            a.appendChild(h3);
            title = obj.InputField[i].ParentTitle;
        }

        var div;
        if (NextLine(obj.LabelClass, obj.InputClass)) {
            div = document.createElement("div");
            div.className = "form-group";
            if (obj.InputField[i].Visible == "FALSE") {
                div.className = "form-group hidden";
            }
        }
        else {
            var divs = document.getElementsByClassName("form-group");
            div = divs[divs.length - 1];
        }

        var label = document.createElement("label");
        label.className = obj.LabelClass + " control-label";
        if (obj.InputField[i].Mandatory == "TRUE") {
            var span = document.createElement("span");
            span.style.color = "red";
            span.appendChild(document.createTextNode("* "));
            label.appendChild(span);
        }
        var text = document.createTextNode(obj.InputField[i].DisplayAs + ":");
        label.appendChild(text);

        div.appendChild(label);

        var div2 = document.createElement("div");
        div2.className = obj.InputClass;
        var input;
        var required = "";
        if (obj.InputField[i].Mandatory == "TRUE") {
            required = " required";
        }
        switch (obj.InputField[i].InputType) {
            case "number":
                input = document.createElement("input");
                input.className = "form-control" + required;
                input.type = "number";
                input.min = 0;
                input.name = obj.InputField[i].ColumnName;
                input.id = obj.InputField[i].Id;
                input.placeholder = obj.InputField[i].PlaceHolderValue;
                input.defaultValue = obj.InputField[i].DefaultValue;
                break;
            case "date":
                input = document.createElement("input");
                if (obj.InputField[i].DisplayAs.indexOf("Month") > -1) {
                    input.className = "form-control hasMonthPicker" + required;
                }
                else {
                    input.className = "form-control hasDatePicker" + required;
                }
                input.type = "text";
                input.name = obj.InputField[i].ColumnName;
                input.id = obj.InputField[i].Id;
                input.placeholder = obj.InputField[i].PlaceHolderValue;
                input.defaultValue = obj.InputField[i].DefaultValue;
                input.autocomplete = "off";
                break;
            case "datetime":
                input = document.createElement("input");
                input.className = "form-control datetimePicker" + required;
                input.type = "text";
                input.name = obj.InputField[i].ColumnName;
                input.id = obj.InputField[i].Id;
                input.placeholder = obj.InputField[i].PlaceHolderValue;
                input.defaultValue = obj.InputField[i].DefaultValue;
                input.autocomplete = "off";
                break;
            case "select":
                input = document.createElement("select");
                input.className = "form-control" + required;
                input.name = obj.InputField[i].ColumnName;
                input.id = obj.InputField[i].Id;
                for (var k = 0; k < obj.InputField[i].Options.length; k++) {
                    var option = document.createElement("option");
                    option.value = obj.InputField[i].Options[k].Value;
                    var itemName = document.createTextNode(obj.InputField[i].Options[k].ItemName);
                    option.appendChild(itemName);
                    input.appendChild(option);
                }
                break;
            case "checkbox":
                for (var k = 0; k < obj.InputField[i].Options.length; k++) {
                    var lbl = document.createElement("label");
                    lbl.className = "col-lg-1";
                    input = document.createElement("input");
                    input.className = "form-control" + required;
                    input.type = "checkbox";
                    input.name = obj.InputField[i].ColumnName;
                    input.value = obj.InputField[i].Options[k].Value;
                    lbl.appendChild(input);
                    var text = document.createTextNode(obj.InputField[i].Options[k].ItemName);
                    lbl.appendChild(text);
                    div2.appendChild(lbl);
                }
                break;
            case "radio":
                for (var k = 0; k < obj.InputField[i].Options.length; k++) {
                    var lbl = document.createElement("label");
                    lbl.className = "col-lg-3";
                    input = document.createElement("input");
                    input.className = "form-control" + required;
                    input.type = "radio";
                    input.name = obj.InputField[i].ColumnName;
                    input.value = obj.InputField[i].Options[k].Value;
                    lbl.appendChild(input);
                    var text = document.createTextNode(obj.InputField[i].Options[k].ItemName);
                    lbl.appendChild(text);
                    div2.appendChild(lbl);
                }
                break;
            case "image":
                input = document.createElement("input");
                input.className = "form-control" + required;
                input.type = "file";
                input.accept = ".png, .jpg, .jpeg";
                input.name = obj.InputField[i].ColumnName;
                input.id = obj.InputField[i].Id;
                input.onchange = function () {
                    if (this.files && this.files[0]) {
                        var reader = new FileReader();
                        reader.onload = imageIsLoaded;
                        reader.readAsDataURL(this.files[0]);
                    }
                };
                var canvas = document.createElement('canvas');
                canvas.id = "myImg";
                canvas.height = 0;
                canvas.width = 0;
                div2.appendChild(canvas);
                break;
            case "file":
                input = document.createElement("input");
                input.className = "form-control" + required;
                input.type = "file";
                input.accept = "application/pdf, application/vnd.ms-powerpoint";
                input.name = obj.InputField[i].ColumnName;
                input.id = obj.InputField[i].Id;

                break;
            case "select-ds":
                input = document.createElement("select");
                input.className = "form-control" + required;
                input.name = obj.InputField[i].ColumnName;
                input.id = obj.InputField[i].Id;
                input.DataSource = obj.InputField[i].DataSource;
                input.dCol = obj.InputField[i].DataColumn;
                input.dpCol = obj.InputField[i].DataDispColumn;
                input.setAttribute("data-ds", obj.InputField[i].DataSource);
                input.setAttribute("data-dCol", obj.InputField[i].DataColumn);
                input.setAttribute("data-dpCol", obj.InputField[i].DataDispColumn);
                input.setAttribute("data-table", obj.InputField[i].TableName);
                input.LinkedObj = obj.InputField[i].LinkedObj;
                var newInput = input;
                var queryParam = "";
                if (input.LinkedObj != undefined && $("#" + input.LinkedObj).length != 0) {
                    var numbers = new RegExp("^[0-9]+$");
                    var value = $("#" + input.LinkedObj).val() == undefined ? "" : $("#" + input.LinkedObj).val();
                    if (value != "")
                        value = numbers.test(value) ? value : "'" + value + "'";
                    if (value != "" && value != "'-1'") {
                        queryParam = "&queryParam=" + $("#" + input.LinkedObj).attr("name") + "_equals_" + encodeURI(value);
                    }
                    $("#" + input.LinkedObj)[0].setAttribute("data-downlink", input.id);
                    $("#" + input.LinkedObj).on("change", function (e) {
                        AddOptionList($(this));
                    });
                }
                var object = {};
                var divWhere = ""
                if (globalDivision !== undefined)
                    divWhere = "&div=" + globalDivision[0];
                object.url = "../api/Common/GetOptionList?table=" + newInput.DataSource + "&dCol=" + newInput.dCol + "&dpCol=" + newInput.dpCol + queryParam + divWhere;
                GetData_Ajax(object, false).then(function (data) {
                    $(newInput).find("option").remove();
                    for (var k = 0; k < data.Body.length; k++) {
                        var option = document.createElement("option");
                        option.value = data.Body[k].Value;
                        var itemName = document.createTextNode(data.Body[k].ItemName);
                        option.appendChild(itemName);
                        newInput.appendChild(option);
                    }
                });
                break;
            case "textarea":
                input = document.createElement("textarea");
                input.className = "form-control" + required;
                input.rows = "5";
                input.name = obj.InputField[i].ColumnName;
                input.id = obj.InputField[i].Id;

                break;
            default:
                input = document.createElement("input");
                input.className = "form-control" + required;
                input.type = "text";
                input.name = obj.InputField[i].ColumnName;
                input.id = obj.InputField[i].Id;
                input.placeholder = obj.InputField[i].PlaceHolderValue;
                input.defaultValue = obj.InputField[i].DefaultValue;
                break;
        }
        if (obj.InputField[i].Disabled == "TRUE") {
            input.disabled = true;
        }
        for (var j = 0; j < obj.InputField[i].CBFunction.length; j++) {
            var event = obj.InputField[i].CBFunction[j].EventName;
            var fn = obj.InputField[i].CBFunction[j].FunctionName;
            //input.addEventListener(event, eval("(" + fn + ")"), false);//obj.InputField[i].CBFunction[0].onchange;
            if (obj.InputField[i].InputType == "radio") {
                $(document).on(event, "input[name='" + input.name + "']", eval("(" + fn + ")"));
            }
            else {
                $(document).on(event, "#" + input.id, eval("(" + fn + ")"));
            }
        }
        if (obj.InputField[i].DataType == "int") {
            var fn = "function(){\nif($(this).val().length > 10 ){\n$(this).val('')\nShowNotification('Max 10 digits allowed','error')\n}\n}";
            input.addEventListener("blur", eval("(" + fn + ")"), false);
        }
        if (obj.InputField[i].InputType != "checkbox" && obj.InputField[i].InputType != "radio") {
            div2.appendChild(input);
        }
        div.appendChild(div2);

        if ($("#preview").length != 0) {
            var d = document.createElement("div");
            d.className = "col-lg-1";
            var edit = document.createElement("i");
            edit.className = "edit2 glyphicon glyphicon-edit control-label";
            d.appendChild(edit);
            div.appendChild(d);
            var d2 = document.createElement("div");
            d2.className = "col-lg-1";
            var del = document.createElement("i");
            del.className = "delete2 col-lg-1 glyphicon glyphicon-trash control-label";
            d2.appendChild(del);
            div.appendChild(d2);
            var d3 = document.createElement("div");
            d3.className = "col-lg-1";
            var drag = document.createElement("i");
            drag.className = "move glyphicon glyphicon-move control-label";
            d3.appendChild(drag);
            div.appendChild(d3);
            var li = document.createElement("li");
            li.appendChild(div);
            section.appendChild(li);
        }
        else {
            //var pin = document.createElement("i");
            //pin.className = "pin glyphicon glyphicon-star-empty control-label";
            //pin.title = "keep this value for next record";
            //div.appendChild(pin);

            document.getElementsByClassName("section")[document.getElementsByClassName("section").length - 1].appendChild(div);
            //section.appendChild(div);

            //a.appendChild(div);
        }

        if (obj.InputField[i].Multiple == "TRUE") {
            $("#" + obj.InputField[i].Id + " option[value='-1']").remove();
            $("#" + obj.InputField[i].Id).attr("multiple", "multiple");
            $("#" + obj.InputField[i].Id).multiselect({
                includeSelectAllOption: true
            });
            $("#" + obj.InputField[i].Id).multiselect('deselectAll', false);
            $("#" + obj.InputField[i].Id).multiselect('updateButtonText');
        }

        $($("#" + obj.id + " li")[i]).data("object", obj.InputField[i]);
    }
    a.appendChild(section);
    if (obj.OnLoadEvent != "") {
        eval(obj.OnLoadEvent);
    }

    $(".hasDatePicker").datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function () {
            $(this).click();
        },
        beforeShowDay: function (date) {
            var string = GetDateFormat(new Date(date));
            try {
                return [disabledDates.indexOf(string) == -1]
            } catch (e) {
                return [true]
            }
        },
        minDate: obj.disablePastDate == true ? 0 : obj.disablePastDate == false || obj.disablePastDate == undefined ? -365 : obj.disablePastDate
    });
	
    $(".datetimePicker").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        timePicker: true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        },
        minDate: obj.disablePastDate == true ? moment() : moment().subtract(1, 'years')
    });
}

function AddOptionList(linkedInput) {
    var newInput = linkedInput.data("downlink");
    linkedInput = linkedInput.attr("id");
    var numbers = new RegExp("^[0-9]+$");
    var value = $("#" + linkedInput).val() == undefined ? "" : $("#" + linkedInput).val();
    if (value != "")
        value = numbers.test(value) ? value : "'" + value + "'";
    if (value != "" && value != "'-1'") {
        queryParam = "&queryParam=" + $("#" + linkedInput).attr("name") + "_equals_" + encodeURIComponent(value).replace(/[!'()*]/g, escape);
    }
    if ($("#" + newInput).length != 0) {
        var object_event = {};
        var divWhere = "", division = globalDivision[0];
        if (division != 0)
            divWhere = "&div=" + division;
        object_event.url = "../api/Common/GetOptionList?table=" + $("#" + newInput).data("ds") + "&dCol=" + $("#" + newInput).data("dcol") + "&dpCol=" + $("#" + newInput).data("dpcol") + queryParam + divWhere;
        //$.get("../api/Management/GetOptionList?table=" + input.DataSource + "&dCol=" + input.dCol + "&dpCol=" + input.dpCol + "&queryParam=" + queryParam, function (data) {
        GetData_Ajax(object_event, false).then(function (data) {
            $("#" + newInput).find("option").remove();
            for (var k = 0; k < data.Body.length; k++) {
                var option = document.createElement("option");
                option.value = data.Body[k].Value;
                var itemName = document.createTextNode(data.Body[k].ItemName);
                option.appendChild(itemName);
                $("#" + newInput)[0].appendChild(option);
                if ($("#" + newInput + "_chosen").length == 1) {
                    $("#" + newInput).chosen("destroy").chosen().change();
                }
            }
        });
    }
}

function NextLine(label, input) {
    var space = parseFloat(label.replace("col-lg-", "")) + parseFloat(input.replace("col-lg-", ""));
    if ($("h3").last().next().length == 0 || $(".form-group").length == 0) {
        return true;
    }
    else {
        var label2 = 0, input2 = 0;
        $("#form .form-group").last().find("label").each(function () {
            label2 += parseFloat(this.className.replace(" control-label", "").replace("col-lg-", ""));
            input2 += parseFloat($(this).next()[0].className.replace("col-lg-", ""));
        })

        var occupy = label2 + input2;
        if (12 - occupy >= space) {
            return false;
        }
        else {
            return true;
        }
    }
}

function imageIsLoaded(e) {
    //$('#myImg').attr('src', e.target.result);
    var image = new Image;
    image.src = e.target.result;
    image.onload = function (a) {
        var canvas = document.getElementById("myImg");
        canvas.height = 160;
        canvas.width = 400;
        canvas.getContext('2d').drawImage(image, 0, 0, 400, 160);
        $('#btnPhotoReset').remove();
        $("#myImg").closest(".form-group").append("<button type=\"button\" id=\"btnPhotoReset\">Photo Reset</button>");
    };
}

function InputField_Validation(id) {
    var flag = true;
    var empty = [];
    $(id + " .form-control.required").each(function (e) {
        if ($(this).val() == null || $(this).val() == "" || $(this).val() == "-1") {
            flag = false;
            empty.push(e);
        }
        else if ($($(this).closest(".form-group")).find("span.error").length != 0) {
            flag = false;
            empty.push(e);
        }
    });
    if (flag) {
        return flag;
    }
    else {
        ShowNotification("Please select or enter N/A if is not applicable", "error");
        $(id + " .form-control.required")[empty[0]].focus();
        var SupportDiv = $($(".form-control.required")[empty[0]])[0];
        //window.scroll(0, findPos(SupportDiv) * 0.7);
        $('html, body').animate({ scrollTop: findPos(SupportDiv) * 0.7 }, 'slow');
        return false;
    }
}

function getNewData() {
    //var i = 0;
    $("#form .form-group").each(function (e) {
        var obj = {};
        var l = $(this).find(".form-control").length;
        $(this).find(".form-control").each(function (e2) {
            var i = getIndex(newJson.InputField, "ColumnName", $(this)[0].name);
            if (newJson.InputField[i].Disabled != "TRUE") {
                if ($(this)[0].type == "file" && $(this)[0].accept == ".png, .jpg, .jpeg") {
                    newJson.InputField[i].Value = $(this).prev()[0].toDataURL() == "data:," ? "" : $(this).prev()[0].toDataURL();
                }
                else if ($(this)[0].type == "file") {
                    newJson.InputField[i].Value = $(this).val() == "" ? $(this).val() : "C:\\eGrant\\upload\\" + $(this).val().split("\\")[2];
                }
                else if ($(this)[0].type == "checkbox") {
                    if ($(this)[0].checked) {
                        obj[$(this).val()] = "Yes";
                    }
                    else {
                        obj[$(this).val()] = "";
                    }
                    if (e2 == l - 1) {
                        newJson.InputField[i].Value = JSON.stringify(obj);
                    }
                }
                else if ($(this)[0].type == "radio") {
                    if ($(this)[0].checked) {
                        newJson.InputField[i].Value = $(this).val();
                        //i++;
                        return false;
                    }
                    else {
                        //i--;
                    }
                }

                else if ($(this)[0].type == "number" && $(this).closest(".form-group").find("table").length != 0) {
                    obj[$(this).attr("name")] = isNaN(parseFloat($(this).val())) ? "" : parseFloat($(this).val());
                    if (e2 == l - 1) {
                        newJson.InputField[i].Value = JSON.stringify(obj);
                    }
                }
                else {
                    newJson.InputField[i].Value = $(this).val() != null ? Array.isArray($(this).val()) ? $(this).val().join(';') : $(this).val().replace(/&/g, "_").replace(/\?/g, "_") : $(this).val();
                }
            }
            i++;
        });


    });
}

function getIndex(list, property, value) {
    var index;
    for (var i = 0; i < list.length; ++i) {
        if (list[i][property] == value) {
            index = i;
            break;
        }
    }
    return index;
}

function PopulateFormValue(columns) {
    for (var i = 0; i < newJson.InputField.length; i++) {
        if (newJson.InputField[i].ColumnName.indexOf("Criteria") != -1 && columns[newJson.InputField[i].ColumnName] != undefined) {
            var criterias = JSON.parse(columns[newJson.InputField[i].ColumnName]);
            if (criterias != null) {
                for (var j = 0; j < criterias.length; j++) {
                    newJson.InputField[i].Value = JSON.stringify(criterias[j]);
                    i++;
                }
            }
        }
        else {
            newJson.InputField[i].Value = columns[newJson.InputField[i].ColumnName];
        }
    }

    for (var i = 0; i < newJson.InputField.length; i++) {
        var column = newJson.InputField[i].ColumnName;
        var value = newJson.InputField[i].Value;
        var type = newJson.InputField[i].InputType;
        if (column == "Photo") {
            //$("#myImg").attr("src", columns[column]);
            if (value != "") {
                var image = new Image;
                image.src = value;
                var canvas = document.getElementById("myImg");
                canvas.height = 80;
                canvas.width = 200;
                canvas.getContext('2d').drawImage(image, 0, 0, 200, 80);
                $("#myImg").closest(".form-group").append("<button id=\"btnPhotoReset\">Photo Reset</button>");
            }
            else {
                var canvas = document.getElementById('myImg');
                canvas.height = 0;
                canvas.width = 0;
                canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
            }
        }
        else if (column == "Indicate") {
            var checkedValue = JSON.parse(value);
            for (var cv in checkedValue) {
                if (checkedValue[cv].toUpperCase() == "YES") {
                    $("input[type=checkbox][name='" + column + "'][value='" + cv + "'").prop("checked", true);
                }
                else {
                    $("input[type=checkbox][name='" + column + "'][value='" + cv + "'").prop("checked", false);
                }
            }
        }

        else if (column == "DateHour") {
            var dateHour = JSON.parse(value);
            for (var dh in dateHour) {
                $("[name='" + dh + "']").val(dateHour[dh]);
            }
        }

        else if (column == "MonthHour") {
            var monthHour = JSON.parse(value);
            for (var mh in monthHour) {
                $("[name='" + mh + "']").val(monthHour[mh]);
            }
        }

        else if (column == "Approval_Letter") {

        }
        else if (type == "radio") {
            if (value != null) {
                var checkedValue = JSON.parse(value);
                $("input[type=radio][name='" + column + "'][value='" + (checkedValue.value != undefined ? checkedValue.value : checkedValue) + "'").prop("checked", true);
                $("input[type=radio][name='" + column + "'][value='" + (checkedValue.value != undefined ? checkedValue.value : checkedValue) + "'").change();
            }
        }
        else if (type == "file") {

        }
        else {
            if (value != null && value.toString().indexOf("T00:00:00") != -1) {
                var date = new Date(value);
                var date2 = GetDateFormat(date);//(date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
                $("[name='" + column + "']").val(date2);
            }
            else {
                if (value != null) {
                    $("[name='" + column + "']").val(value.toString());
                }
                if ($("[name='" + column + "']").attr("id") != undefined && $("[name='" + column + "']").attr("id").indexOf("ddl") != -1) {
                    var ev = document.createEvent('Event');
                    ev.initEvent('change', true, false);
                    $("[name='" + column + "']")[0].dispatchEvent(ev);
                }
            }
        }
    }
}

function clearAll() {
    $("#form .form-control").each(function (e) {
        if ($(this).attr("id") != undefined && $(this).attr("id") != "txtDivision") {
            if ($(this).attr("id").indexOf("ddl") != -1) {
                $(this).val("-1");
            }
            else if ($(this).attr("id") == "txtPhoto") {
                var canvas = document.getElementById('myImg');
                canvas.height = 0;
                canvas.width = 0;
                canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
                var imagePath = document.getElementById('txtPhoto');
                imagePath.value = imagePath.defaultValue;
            }
            else {
                $(this).val("");
            }
        }
        else {
            if ($(this).attr("type") == "checkbox") {
                $(this).prop("checked", false);
            }
            else if ($(this)[0].type == "number" && $(this).closest(".form-group").find("table").length != 0) {
                $(this).val("");
            }
        }

    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
function findPos(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        do {
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return [curtop];
    }
}

function GetDateFormat(dateString) {
    //var dateFormat = ("0" + (dateString.getMonth() + 1)).slice(-2) + "/" + ("0" + (dateString.getDate())).slice(-2) + "/" + dateString.getFullYear();
    var dateFormat = dateString.getFullYear() + "-" + ("0" + (dateString.getMonth() + 1)).slice(-2) + "-" + ("0" + (dateString.getDate())).slice(-2);
    return dateFormat;
}

function GetMonthFormat(dateString) {
    var dateFormat = dateString.getFullYear() + "-" + ("0" + (dateString.getMonth() + 1)).slice(-2);
    return dateFormat;
}
function GetDate(datetime) {
    //var dt = new Date(datetime.replace("T", " "));
    var dt = new Date(datetime);
    return dt;
}

function GetDateToString(datetime) {
    var dt = new Date(datetime);
    return dt.toDateString();
}

function GetDaysDiff(date1, date2) {
    dt1 = new Date(date1);
    dt2 = new Date(date2);
    return ((dt2.getTime() - dt1.getTime()) / (1000 * 60 * 60 * 24)).toFixed(1);
    //return (Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate(), dt2.getUTCHours()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate(), dt1.getUTCHours())) / (1000 * 60 * 60 * 24).toFixed(1);
}
function clone(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function createGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

var tableSaved, exportContent;
function ExportHTMLTabletoExcel(tableID, tableName, guid) {
    if (tableSaved == guid) {
        return exportContent;
    } else {
        exportContent = [];
        var tableheader = [];
        var tablerows = [];
        var wMsg = new wMessage();
        // For Table header
        //tableHeader obj : value, rowspan, colspan, font-weight(fw), color,bgcolor, size, font-style(fs), align(text-align)
        var value, rowspan, colspan, fw, color, bgcolor, size, fs, align, temparr = [];
        var tablehead_length = $("#" + tableID + " th").length;
        for (var i = 0; i < tablehead_length; i++) {
            if ($($("#" + tableID + " th")[i]).is(":visible")) {
                value = $("#" + tableID + " th")[i].innerText;
                rowspan = parseInt($($("#" + tableID + " th")[i]).attr("rowspan"));
                rowspan = isNaN(rowspan) ? 0 : rowspan;
                colspan = parseInt($($("#" + tableID + " th")[i]).attr("colspan"));
                colspan = isNaN(colspan) ? 0 : colspan;
                fw = $($("#" + tableID + " th")[i]).css("font-weight").toString();
                color = rgbStringtoHex($($("#" + tableID + " th")[i]).css("color"));
                bgcolor = rgbStringtoHex($($("#" + tableID + " th")[i]).css("background-color"));
                size = parseInt($($("#" + tableID + " th")[i]).css("font-size").substring(0, 2));
                fs = $($("#" + tableID + " th")[i]).css("font-style");
                align = $($("#" + tableID + " th")[i]).css("text-align");
                tableheader.push({ value: value, rowspan: rowspan, colspan: colspan, fw: fw, color: color, bgcolor: bgcolor, size: size, fs: fs, align: align });
            }
        }
        exportContent.push(tableheader);
        //For Table Rows

        if ($("span.glyphicon.glyphicon-plus").length > 0 && $("span.glyphicon.glyphicon-minus").length == 0) {
            $("span.glyphicon.glyphicon-plus").each(function () {
                $(this).click();
            });
        } else {
            $("span.glyphicon.glyphicon-minus").each(function () {
                $(this).click();
            });
            $("span.glyphicon.glyphicon-plus").each(function () {
                $(this).click();
            });
        }
        var row_num = $("#" + tableID + " tbody ").find("tr").length - 1;
        var tcol_num = $("#" + tableID + " tbody tr:eq(0)").find("td").length;

        var tablerows = [];
        var nextRow = 0;
        var colCount = 0;
        var maxTD = $("#" + tableID + " tbody td").length - 1;
        $("#" + tableID + " tbody td").each(function (item) {
            var currentRow = $(this).parent("tr").index();
            var increment = false;
            var nextTDrow = $($("#" + tableID + " tbody td")[item + 1]).parent("tr").index();
            if (currentRow > nextRow) {
                colCount = 0;
                nextRow = currentRow;
                exportContent.push(tablerows);
                tablerows = [];
            }
            if ($(this).is(":visible")) {
                value = $(this)[0].innerText;
                rowspan = parseInt($(this).attr("rowspan"));
                rowspan = isNaN(rowspan) ? 0 : rowspan;
                colspan = parseInt($(this).attr("colspan"));
                colspan = isNaN(colspan) ? 0 : colspan;
                if ($(this).children("u").length > 0)
                    fw = "bold";
                else
                    fw = $(this).css("font-weight").toString();
                color = rgbStringtoHex($(this).css("color"));
                bgcolor = rgbStringtoHex($(this).css("background-color"));
                size = parseInt($(this).css("font-size").substring(0, 2));
                fs = $(this).css("font-style");
                align = $(this).css("text-align");
                tablerows.push({ value: value, rowspan: rowspan, colspan: colspan, fw: fw, color: color, bgcolor: bgcolor, size: size, fs: fs, align: align });

                if (rowspan > 0) {
                    var tempvalue = {};
                    tempvalue.pos = colCount; //Position to repeat
                    //if (colCount != tcol_num) {
                    //    var diff = tcol_num - colCount;
                    //    tempvalue.pos = colCount + diff;
                    //}
                    tempvalue.obj = tablerows[colCount];
                    tempvalue.time = rowspan - 1;
                    tempvalue.skip = true;//Time to repeat the value
                    temparr.push(tempvalue);
                }
            }

            if (nextTDrow > currentRow) {
                if (temparr.length > 0) {
                    for (var count = 0; count < temparr.length; count++) {
                        if (temparr[count].time > 0 && temparr[count].skip == false) {
                            tablerows.splice(temparr[count].pos, 0, temparr[count].obj);
                            temparr[count].time -= 1;
                        }
                        else {
                            if (temparr[count].time != 0 && temparr[count].skip == true) {
                                temparr[count].skip = false;
                            }
                            if (temparr[count].time == 0) {
                                temparr.splice(count, 1);
                                count--;
                            }

                        }
                    }
                }
            }
            colCount++;
        });
        //Push the last row;
        exportContent.push(tablerows);

        var TableName = tableName.substring(0, 30);
        exportContent.push(TableName);
        if ($("span.glyphicon.glyphicon-minus").length > 0) {
            $("span.glyphicon.glyphicon-minus").each(function () {
                $(this).click();
                return;
            });
        }

        tableSaved = guid;
        return exportContent;

    }
}

//function ExportBootstrapTabletoExcel(e) {
//    //Declarations
//    var content = [];
//    var tableheader = [];
//    var tablerows = [];
//    var wMsg = new wMessage();
//    //Get Bootstrap Table name
//    var table = $(e).parents("div").find("table:contains(#)")[0];
//    var tableTitle = $($(e).parents("div.panel-heading").children())[1].innerText;
//    //Get Data
//    var all_data = $(table).bootstrapTable('getData');
//    //Sort and Insert Data according to Table Header
//    // For Table header
//    //tableHeader obj : value, rowspan, colspan, font-weight(fw), color,bgcolor, size, font-style(fs), align(text-align)
//    var value, rowspan, colspan, fw, color, bgcolor, size, fs, align;
//    var tablehead_length = $(table).find("th").length;
//    for (var i = 0; i < tablehead_length; i++) {


//        value = $(table).find("th")[i].innerText;
//        value = value.substring(0, value.length - 1);
//        rowspan = parseInt($($(table).find("th")[i]).attr("rowspan"));
//        if (isNaN(rowspan))
//            rowspan = 0;
//        colspan = parseInt($($(table).find("th")[i]).attr("colspan"));
//        if (isNaN(colspan))
//            colspan = 0;
//        fw = $($(table).find("th")[i]).css("font-weight").toString();
//        color = rgbStringtoHex($($(table).find("th")[i]).css("color"));
//        bgcolor = rgbStringtoHex($($(table).find("th")[i]).css("background-color"));
//        if (bgcolor == "#000000")
//            bgcolor = rgbStringtoHex($($(table).find("tr")[0]).css("background-color"));
//        size = parseInt($($(table).find("th")[i]).css("font-size").substring(0, 2));
//        fs = $($(table).find("th")[i]).css("font-style");
//        align = $($(table).find("th")[i]).css("text-align");

//        tableheader.push({
//            value: value, rowspan: rowspan, colspan: colspan, fw: fw, color: color, bgcolor: bgcolor, size: size, fs: fs, align: align
//        });

//    }
//    console.log(tablehead_length);
//    console.log(tableheader);


//    //Enter Data of table Rows into array
//    //Return content

//}

function rgbStringtoHex(RGB) {
    var temp = RGB.replace("rgb(", "").replace(")", "").split(",");
    var color_code = [3];

    for (var i = 0; i < 3; i++) {
        color_code[i] = parseInt(temp[i]);
    }

    return "#" + (toHex(color_code[0]) + toHex(color_code[1]) + toHex(color_code[2]))
}

function toHex(n) {
    n = parseInt(n, 10);
    if (isNaN(n)) return "00";
    n = Math.max(0, Math.min(n, 255));
    return "0123456789ABCDEF".charAt((n - n % 16) / 16)
        + "0123456789ABCDEF".charAt(n % 16);
}

function exportExcel(e) {
    var object = {
    };
    object.title = $($(e).closest('.panel.panel-default')).data('object').title;
    object.id = $($(e).closest('.panel.panel-default')).data('object').id;

    if ($("#" + object.id).html().indexOf("<span style=\"color:red; font-size:large;\">") == -1) {
        var len = object.title.length;

        if (len > 128)
            len = 128;

        var filename = object.title.substring(0, len);
        filename = replaceAll(filename, "-br-", "-");
        filename = replaceAll(filename, "<br>", "-");

        var wMsg = new wMessage();
        wMsg.Body.title = filename;

        object.query = $($(e).closest('.panel.panel-default')).data('object').query;

        var url = "";

        //if (object.query != undefined && object.title.indexOf("CCAFR") == -1) {
        //    wMsg.Body.query = JSON.parse(object.query);
        //    url = "/api/Common/ExportToExcelByQuery";
        //}
        //else {
        object.data = $($(e).closest('.panel.panel-default')).data('object').data;

        var json = JSON.stringify(object.data).replace(/<span class='hidden'>(.{19})<\/span>/g, "").replace(new RegExp("<span style='color:red;'>", "g"), "").replace(new RegExp("</span>", "g"), "").replace("<b>", "").replace(new RegExp(" &#8595;</b>", "g"), "");
        //if (globalDivision != undefined) {
        //&& object.query.indexOf('{"ColumnName":"Division","Criterias":[{"Command":"eq","Value":1}]}') != -1
        if (object.query != undefined && JSON.stringify(object.query).indexOf('{"ColumnName":"Division","Criterias":[{"Command":"eq","Value":1}]}') != -1) {
            object.data = JSON.parse(json.replace(new RegExp('Serial#', 'g'), 'Production ID').replace(new RegExp('Serial Number', 'g'), 'Production ID').replace(new RegExp('SerialNumber', 'g'), 'Production ID'));
        }
        else {
            object.data = JSON.parse(json);
        }
        //}

        for (var i = 0; i < object.data.length; i++) {
            delete object.data[i].number;
            delete object.data[i].Link;
            delete object.data[i]._data;
        }
        wMsg.Body.data = object.data;
        url = "/api/Common/ExportToExcelByData";
        //}
        $spinner.addClass('loader');
        PostData_Ajax(wMsg, url).then(function (guid) {
            var popUp = window.open("/api/Common/DownloadExcel?guid=" + guid + "&filename=" + filename.replace(',', ' '));
            if (popUp == null || typeof (popUp) == 'undefined') {
                alert('Please allow pop-ups from ' + location.origin + ' and try again');
            }
            else {
                popUp.focus();
            }
            $spinner.removeClass('loader');
        });
    }
    else {
        ShowNotification("No record to export", "error");
    }
}


function replaceAll(originalString, find, replace) {
    return originalString.replace(new RegExp(find, 'g'), replace);
};

function exportPNG(e) {
    object = {
    };
    object.id = $($(e).closest('.panel.panel-default')).data('object').id;
    object.title = $($(e).closest('.panel.panel-default')).data('object').title;

    if ($("#" + object.id).html().indexOf("<span style=\"color:red; font-size:large;\">") == -1) {
        var len = object.title.length;

        if (len > 128)
            len = 128;

        var filename = object.title.substring(0, len);
        filename = replaceAll(filename, ",", "_"); //filename.replace(",", "_");
        filename = replaceAll(filename, "_br_", "_");
        filename = replaceAll(filename, "<br>", "_");
        var chart = $("#" + object.id).highcharts();

        Highcharts.setOptions({
            exporting: {
                enabled: true,
                //sourceWidth: 900
                sourceWidth: chart.chartWidth,
                sourceHeight: chart.chartHeight
            },
        });

        chart.setTitle({
            text: object.title
        });

        var setRotation = 0;
        var setFrontSize = '12px';

        if (typeof object.id != 'number') {
            if (object.id.toString().indexOf("CCAFR") >= 0) {
                var categoriesLength = chart.xAxis[0].categories.length;

                if (categoriesLength < 8) {
                    setRotation = 0;
                }
                else if (categoriesLength < 19) {
                    setRotation = -45;
                }
                else if (categoriesLength < 27) {
                    setRotation = -90;
                }
                else {
                    setRotation = -90;
                    setFrontSize = '8px';
                }

                chart.xAxis[0].update({
                    labels: {
                        rotation: setRotation, style: {
                            fontSize: '9.9px'
                        }
                    }
                });
            }
        }


        chart.exportChartLocal({
            type: 'image/jpg',
            filename: filename
        }
            //, {
            //    chart: {
            //        events: null
            //    }
            //}
        );

        chart.setTitle({
            text: ""
        });


        if (typeof object.id != 'number') {
            if (object.id.indexOf("CCAFR") >= 0) {

                if (categoriesLength < 15) {
                    setRotation = 0;
                }
                else {
                    setRotation = -45;
                }
                chart.xAxis[0].update({
                    labels: {
                        rotation: setRotation, style: {
                            fontSize: '13px'
                        }
                    }
                });

            }
        }

    }
    else {
        ShowNotification("No record to export", "error");
    }
}

function appendUserInput(obj) {
    //col -> disp_name,col_name, type(for select: optionUrl, itemID,optionName)
    var col_arr = obj.col_arr;
    var input_row = "";
    input_row += "<div id=\"input_row\" class=\" col-lg-12 form-group justify-content-center\" style=\"margin-bottom:3em;\">";
    if (obj.formTitle != undefined)
        input_row += "<h3>" + obj.formTitle + "</h3><hr>";
    input_row += "<form role=\"form\" class=\"form-horizontal\">";
    var col_length = col_arr.length;
    for (var i = 0; i < col_length; i++) {
        switch (col_arr[i].type) {
            case "text":
                input_row += "<div id=\"div_" + col_arr[i].col_name + " \" class=\"form-group\">";
                input_row += "<label class=\"col-sm-offset-1 col-lg-2 control-label\">" + col_arr[i].disp_name + ": </label>";
                input_row += "<div class=\" col-lg-6\"><input required name=\"" + col_arr[i].col_name + "\" type=\"text\" class=\"form-control\" id=\"input_" + col_arr[i].col_name + "\" placeholder=\"Enter " + col_arr[i].disp_name + "\"/></div>";
                input_row += "</div>";
                break;
            case "select":
                var obj = { url: location.origin + col_arr[i].optionUrl };
                GetData_Ajax(obj, false).then(function (options) {
                    if (options.status == -1) {
                        showNotification(options.Description, "error");
                    } else {
                        input_row += "<div id=\"div_" + col_arr[i].disp_name + " \" class=\"form-group\">";
                        input_row += "<label class=\"col-sm-offset-1 col-lg-2 control-label\">" + col_arr[i].disp_name + ": </label>";
                        input_row += "<div class=\"col-lg-6\"><select name=\"" + col_arr[i].disp_name + "\" class=\"form-control\" id=\"ddl" + col_arr[i].col_name + "\">";
                        if (col_arr[i].col_optionFirst == true) {
                            input_row += "<option value=\"null\">None</option>";
                        }
                        var optionsLength = options.Body.length;
                        for (var j = 0; j < optionsLength; j++) {
                            if (options.Body[j]["DispName"] != undefined)
                                input_row += "<option value=" + options.Body[j]["Value"] + ">" + toTitleCase(options.Body[j]["DispName"]) + "</option>"
                            else
                                input_row += "<option value=" + options.Body[j]["Value"] + ">" + toTitleCase(options.Body[j]["Value"]) + "</option>"
                        }
                        input_row += "</select></div>";
                        input_row += "</div>";
                    }
                });
                break;
        }
    }
    input_row += "</form>";
    input_row += "</div>";

    // Submit & Reset Button
    //input_row += "<br/><br/><br/>";

    input_row += "<div style=\"margin-bottom:1em;\"  id=\"input_btnrow\" class=\" row form-group  \">";

    input_row += "<input type=\"button\" class=\"btn btn-primary pull-right\" id=\"btnClear\" value=\"Clear\" style=\"margin-left:10px;\">";

    input_row += "<input type=\"submit\" class=\"btn btn-primary pull-right\" id=\"btnSubmit\" value=\"Submit\" style=\"margin-left:10px;\">";

    if (obj.extra == "rework") {
        input_row += "<input type=\"button\" class=\"btn btn-primary pull-right\" id=\"btnRework\"  value=\"Rework\">";
    }
    input_row += "</div>";


    return input_row;

}

// Converts concatenated strings to sentence words. E.g : GLNaturalAccount --> GL Natural Account
function wordCaseSlicer(str) {
    var caseRegex = new RegExp("\[a-z][A-Z]", "g");
    var caseRegex2 = new RegExp("([A-Z]{2}[a-z])", "g");
    var arrWord = str.match(caseRegex);
    if (arrWord != null) {
        for (var i = 0; i < arrWord.length; i++) {
            str = str.replace(arrWord[i], arrWord[i][0] + " " + arrWord[i][1]);

        }
    }
    var arrWord2 = str.match(caseRegex2)
    if (arrWord2 != null) {
        for (var i = 0; i < arrWord2.length; i++) {
            var text = arrWord2[i];
            text2 = text.replace(text.substr(text.length - 2), " " + text.substr(text.length - 2));
            str = str.replace(text, text2);
        }
    }
    return toTitleCase(str.replace('_', ' '));
}

function toTitleCase(str) {
    var newArr = str.split(" ");
    var newStr = "";
    for (var i = 0; i < newArr.length; i++) {
        newStr += newArr[i].charAt(0).toUpperCase() + newArr[i].substr(1).toLowerCase() + " ";
    }
    return newStr.trim();
}

function exportTableFinder(e) {
    var tableName = $(e).parents("div.panel-heading").children("div[id$=headerName]").text();
    var tableID = "";
    var tableContent;
    var tableCount = $(e).parents("div.panel-heading").next().find("table").length;
    var wMsg = new wMessage();
    var content = {};

    if (tableCount > 1) {
        tableContent = [];
        for (var i = 0; i < tableCount; i++) {
            tableID = $($(e).parents("div.panel-heading").next().find("table")[i]).attr("id");
            tableContent.push(exportTableToExcel(tableID, tableName, createGuid()));
        }
        content["Table"] = tableContent;
        content["multiple"] = true;
    }
    else {
        tableID = $($(e).parents("div.panel-heading").next().find("table")[0]).attr("id");
        content["Table"] = exportTableToExcel(tableID, tableName, createGuid());
        content["multiple"] = false;
    }

    wMsg.Body = content;

    var url = "/api/Common/ExportTableToExcel";
    PostData_Ajax(wMsg, url).then(function (data) {
        if (data.Status == -1)
            ShowNotification(data.Description, "error");
        else {
            var guid = data.Body.guid;
            var fileName = data.Body.fileName;
            var popUp = window.open("/api/Common/DownloadExcel?guid=" + guid + "&filename=" + tableName);
            if (popUp == null || typeof (popUp) == 'undefined') {
                alert('Please allow pop-ups from ' + location.origin + ' and try again');
            }
            else {
                popUp.focus();
            }
            $spinner.removeClass('loader');
        }
    });
}


function exportTableToExcel(tableID, tableName, guid) {
    function rgbStringtoHex(RGB) {
        var temp = RGB.replace("rgb(", "").replace(")", "").split(",");
        var color_code = [3];
        for (var i = 0; i < 3; i++)
            color_code[i] = parseInt(temp[i]);
        return "#" + (toHex(color_code[0]) + toHex(color_code[1]) + toHex(color_code[2]))
    }

    function toHex(n) {
        n = parseInt(n, 10);
        if (isNaN(n)) return "00";
        n = Math.max(0, Math.min(n, 255));
        return "0123456789ABCDEF".charAt((n - n % 16) / 16) + "0123456789ABCDEF".charAt(n % 16);
    }
    if (tableSaved == guid) {
        return exportContent;
    } else {
        exportContent = [];
        var tableheader = [];
        var tablerows = [];
        var wMsg = new wMessage();
        // For Table header
        //tableHeader obj : value, rowspan, colspan, font-weight(fw), color,bgcolor, size, font-style(fs), align(text-align)
        var value, rowspan, colspan, fw, color, bgcolor, size, fs, align, temparr = [];
        var tablehead_length = $("#" + tableID + " th").length;
        for (var i = 0; i < tablehead_length; i++) {
            if ($($("#" + tableID + " th")[i]).is(":visible")) {
                value = $("#" + tableID + " th")[i].innerText;
                rowspan = parseInt($($("#" + tableID + " th")[i]).attr("rowspan"));
                rowspan = isNaN(rowspan) ? 0 : rowspan;
                colspan = parseInt($($("#" + tableID + " th")[i]).attr("colspan"));
                colspan = isNaN(colspan) ? 0 : colspan;
                fw = $($("#" + tableID + " th")[i]).css("font-weight").toString();
                color = rgbStringtoHex($($("#" + tableID + " th")[i]).css("color"));
                bgcolor = rgbStringtoHex($($("#" + tableID + " th")[i]).css("background-color"));
                size = parseInt($($("#" + tableID + " th")[i]).css("font-size").substring(0, 2));
                fs = $($("#" + tableID + " th")[i]).css("font-style");
                align = $($("#" + tableID + " th")[i]).css("text-align");
                tableheader.push({ value: value, rowspan: rowspan, colspan: colspan, fw: fw, color: color, bgcolor: bgcolor, size: size, fs: fs, align: align });
            }
        }
        exportContent.push(tableheader);
        //For Table Rows

        if ($("span.glyphicon.glyphicon-chevron-down").length > 0 && $("span.glyphicon.glyphicon-chevron-up").length == 0) {
            $("span.glyphicon.glyphicon-chevron-down").each(function () {
                $(this).click();
            });
        } else {
            $("span.glyphicon.glyphicon-chevron-up").each(function () {
                $(this).click();
            });
            $("span.glyphicon.glyphicon-chevron-down").each(function () {
                $(this).click();
            });
        }
        var row_num = $("#" + tableID + " tbody ").find("tr").length - 1;
        var tcol_num = $("#" + tableID + " tbody tr:eq(0)").find("td").length;
        var tablerows = [];
        var nextRow = 0;
        var colCount = 0;
        var maxTD = $("#" + tableID + " tbody td").length - 1;
        $("#" + tableID + " tbody td").each(function (item) {
            var currentRow = $(this).parent("tr").index();
            var increment = false;
            var nextTDrow = $($("#" + tableID + " tbody td")[item + 1]).parent("tr").index();
            if (currentRow > nextRow) {
                colCount = 0;
                nextRow = currentRow;
                exportContent.push(tablerows);
                tablerows = [];
            }
            if ($(this).is(":visible")) {
                value = $(this)[0].innerText;
                rowspan = parseInt($(this).attr("rowspan"));
                rowspan = isNaN(rowspan) ? 0 : rowspan;
                colspan = parseInt($(this).attr("colspan"));
                colspan = isNaN(colspan) ? 0 : colspan;
                if ($(this).children("u").length > 0)
                    fw = "bold";
                else
                    fw = $(this).css("font-weight").toString();
                color = rgbStringtoHex($(this).css("color"));
                bgcolor = rgbStringtoHex($(this).css("background-color"));
                size = parseInt($(this).css("font-size").substring(0, 2));
                fs = $(this).css("font-style");
                align = $(this).css("text-align");
                tablerows.push({ value: value, rowspan: rowspan, colspan: colspan, fw: fw, color: color, bgcolor: bgcolor, size: size, fs: fs, align: align });

                if (rowspan > 0) {
                    var tempvalue = {};
                    tempvalue.pos = colCount; //Position to repeat
                    //if (colCount != tcol_num) {
                    //    var diff = tcol_num - colCount;
                    //    tempvalue.pos = colCount + diff;
                    //}
                    tempvalue.obj = tablerows[colCount];
                    tempvalue.time = rowspan - 1;
                    tempvalue.skip = true;//Time to repeat the value
                    temparr.push(tempvalue);
                }
            }

            if (nextTDrow > currentRow || nextTDrow == -1) {
                if (temparr.length > 0) {
                    for (var count = 0; count < temparr.length; count++) {
                        if (temparr[count].time > 0 && temparr[count].skip == false) {
                            tablerows.splice(temparr[count].pos, 0, temparr[count].obj);
                            temparr[count].time -= 1;
                        }
                        else {
                            if (temparr[count].time != 0 && temparr[count].skip == true) {
                                temparr[count].skip = false;
                            }
                            if (temparr[count].time == 0) {
                                temparr.splice(count, 1);
                                count--;
                            }

                        }
                    }
                }
            }
            colCount++;
        });
        //Push the last row;
        exportContent.push(tablerows);
        var TableName = tableName.substring(0, 30);
        exportContent.push(TableName);
        if ($(".glyphicon-chevron-up").length > 0) {
            $(".glyphicon-chevron-up").each(function () {
                $(this).click();
                return;
            });
        }
        tableSaved = guid;
        return exportContent;
    }
}

function loadImage(light, dark) {
    var images = $(".div_change").find("img");
    for (i = 0; i < images.length; i++) {
        if (theme == 'light') {
            $(images[i]).attr('src', $(images[i]).attr('src').substr(0, $(images[i]).attr('src').indexOf('_')) + '_light.png');
        } else {
            $(images[i]).attr('src', $(images[i]).attr('src').substr(0, $(images[i]).attr('src').indexOf('_')) + '_dark.png');
        }
    }
}

function makeCard(obj) {
    var div_card = document.createElement("div");
    div_card.className = "card";

    var card_header = document.createElement("div");
    card_header.className = "card_header";
    if (obj.icon != undefined) {
        var card_icon = document.createElement("img");
        card_icon.src = obj.icon;
        card_header.appendChild(card_icon);
    }
    var card_page = document.createElement("h4");
    card_page.className = "card_page";

    if (obj.id != undefined) {
        var card_id = ("card_" + obj.id).replace(/ /g, "_");
        div_card.id = card_id;
        card_page.innerText = obj.id;
        card_header.appendChild(card_page);
    }
    div_card.appendChild(card_header);

    var card_body = document.createElement("div");
    card_body.className = "card_body";

    if (obj.title != undefined) {
        card_page.innerText = obj.title;
        card_body.appendChild(card_page);
    }
    if (obj.type != undefined) {
        var card_type = document.createElement("h4");
        card_type.className = "card_type";
        card_type.innerText = obj.type;
        card_body.appendChild(card_type);
    }

    var card_desc = document.createElement("p");
    card_desc.innerHTML = obj.desc;
    card_body.appendChild(card_desc);
    div_card.appendChild(card_body);

    if (obj.href != undefined || obj.link != undefined) {
        div_card.classList.add("clickable");
        var card_footer = document.createElement("div");
        card_footer.className = "card_footer";
        var card_link = document.createElement("a");
        card_link.href = obj.href;
        card_link.innerText = obj.link;
        card_footer.appendChild(card_link);
        div_card.appendChild(card_footer);

        div_card.addEventListener("click", function () {
            window.location.href = obj.href;
            //window.open(obj.href, '_blank');
        });
    }

    //if ($(obj.parent).children('div').length % 3 != 0) {
    //    var separator = document.createElement("span");
    //    separator.className = "separator";
    //    (obj.parent).appendChild(separator);
    //}

    (obj.parent).appendChild(div_card);
}

function makeSeparator(container, number) {
    var separator = $(container).children('span');
    if (separator.length > 0) {
        separator.remove();
    }
    for (var i = 0; i < $(container).children('div').length; i++) {
        if (i % number != 0) {
            var separator = document.createElement("span");
            separator.className = "separator";
            $($(container).children('div')[i - 1]).after(separator);
        }
    }
}

function updateConfluenceLink(link) {
    if (link != null) {
        $("a.tools.confluence").removeClass("hide");
        $("a.tools.confluence").attr("href", "https://confluence.it.keysight.com/" + link);
    } else {
        $("a.tools.confluence").addClass("hide");
        $("a.tools.confluence").attr("href", "#");
    }
}

function updateDateUpdated(datetime) {
    if (datetime != null) {
        $(".tools.updated").removeClass("hide");
        var date = datetime;
        var time = "";
        if (datetime.indexOf('T') > -1) {
            date = datetime.split('T')[0];
            time = datetime.split('T')[1];
        }
        $(".tools.updated").html("Last Updated: " + date + " " + time);
    } else {
        $(".tools.updated").html("");
    }
}

function toggleFilterIcon(state) {
    if (state === "show") $(".tools.filter").removeClass("hide");
    else if (state === "hide") $(".tools.filter").addClass("hide");
}


function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    form.setAttribute("target", "_blank");

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}