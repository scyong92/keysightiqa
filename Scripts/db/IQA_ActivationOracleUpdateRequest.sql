CREATE PROCEDURE [dbo].[IQA_ActivationOracleUpdateRequest]
AS
BEGIN
	
select concat(c.PartNumber collate SQL_Latin1_General_CP1_CI_AS,' (',FORMAT (a.ActivationDate, 'yyyy-MM-dd') ,')') as Activation, 'Oracle' as TableName
from dbo.iqa_activation_request as a 
left join dbo.iqa_inventory_item as c on c.Id = a.PartId
where ((a.OracleData like '%"KeyX1Q1R1":false%' and a.Status = 1) or (a.OracleData like '%"KeyX1WPR1":false%' and a.Status = 4)) and a.IsCancel = 0

END
GO