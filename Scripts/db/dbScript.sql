INSERT INTO dbo.p_menu_ui (Id, Name, ParentId, SortOrder, Controller, Action, Visible, Guid, MenuId, Type, MenuRoot, Demo)
values (126, 'IQA', 19, 20, '-', '-', 1, NEWID(), '', '', 1, 0)

INSERT INTO dbo.p_menu_ui (Id, Name, ParentId, SortOrder, Controller, Action, Visible, Guid, MenuId, Type, MenuRoot, Demo)
values (30000, 'Dashboard', 126, 1, 'IQA/Main', 'Dashboard', 0, NEWID(), 'menuIQADashboard', 'Product', 1, 0)

INSERT INTO dbo.p_menu_ui (Id, Name, ParentId, SortOrder, Controller, Action, Visible, Guid, MenuId, Type, MenuRoot, Demo)
values (30001, 'Configuration', 126, 2, 'IQA/Main', 'Configuration', 0, NEWID(), 'menuIQAConfiguration', 'Product', 1, 0)

INSERT INTO dbo.p_menu_ui (Id, Name, ParentId, SortOrder, Controller, Action, Visible, Guid, MenuId, Type, MenuRoot, Demo)
values (30002, 'Activation', 126, 3, 'IQA/Main', 'Activation', 0, NEWID(), 'menuIQAActivation', 'Product', 1, 0)

INSERT INTO dbo.p_menu_ui (Id, Name, ParentId, SortOrder, Controller, Action, Visible, Guid, MenuId, Type, MenuRoot, Demo)
values (30003, 'Inspection', 126, 4, 'IQA/Main', 'Inspection', 0, NEWID(), 'menuIQAInspection', 'Product', 1, 0)

INSERT INTO dbo.p_menu_ui (Id, Name, ParentId, SortOrder, Controller, Action, Visible, Guid, MenuId, Type, MenuRoot, Demo)
values (30004, 'Report', 126, 5, 'IQA/Main', 'Report', 0, NEWID(), 'menuIQAReport', 'Product', 1, 0)

CREATE TABLE [dbo].[iqa_inventory_item](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	[PartNumber] [varchar](100) not null,
	Status bit not null default 1,
	Description [varchar](500)
)

CREATE TABLE [dbo].[iqa_manufacturer](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(255) not null unique,
	Status bit not null default 1,
	Description nvarchar(1000)
)

CREATE TABLE [dbo].[iqa_inventory](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(255) not null unique,
	Status bit not null default 1,
	Description nvarchar(1000)
)

CREATE TABLE [dbo].[iqa_setting](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(255) not null unique,
	Description nvarchar(1000) not null,
	Value nvarchar(1000) not null,
	UpdatedTime datetime not null default getdate(),
)

CREATE TABLE [dbo].[iqa_part_location](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(255) not null unique,
	Status bit not null default 1,
	Description nvarchar(1000)
)

CREATE TABLE [dbo].[iqa_inspection_record_category](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(255) not null unique,
	Status bit not null default 1,
	Description nvarchar(1000)
)

CREATE TABLE [dbo].[iqa_activation_status](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(255) not null unique,
)

CREATE TABLE [dbo].[iqa_activation_request](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	PartId int not null,
	InventoryId int not null,
	PartLocationId int not null,
	ActivationUserId int not null,
	ActivationDate datetime  not null default getdate(),
	DeactivationDate datetime,
	ConsecutiveLotCount int not null,
	AdditionalConsecutiveLotCount int not null,
	Remark nvarchar(1000),
	RequestDocument nvarchar(1000) not null,
	TrainingDocument nvarchar(1000),
	DocumentVersion int not null default 1,
	Status int not null,
	IsCancel bit not null default 1,
	IsSafetyComponent bit not null default 0,
	OracleData varchar(200),
	CreatedTime datetime not null default getdate(),
	UpdatedTime datetime not null default getdate(),
	CONSTRAINT FK_Activation_Part FOREIGN KEY (PartId) REFERENCES [iqa_inventory_item](Id),
	CONSTRAINT FK_Activation_PartLocation FOREIGN KEY (PartLocationId) REFERENCES [iqa_part_location](Id),
	CONSTRAINT FK_Activation_Inventory FOREIGN KEY (InventoryId) REFERENCES [iqa_inventory](Id),
	CONSTRAINT FK_Activation_User FOREIGN KEY (ActivationUserId) REFERENCES [u_user_informations](user_id),
	CONSTRAINT FK_Activation_Status FOREIGN KEY (Status) REFERENCES [iqa_activation_status](Id)
)

CREATE TABLE [dbo].[iqa_inspection_record](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	ReceivedTime datetime  not null default getdate(),
	InspectionTime datetime  not null default getdate(),
	PartId int not null,
	ManufacturerId int not null,
	RequestorId int not null,
	InspectorId int not null,
	ActivationId int not null,
	InspectedQty int not null,
	AcceptedQty int not null,
	CategoryId int not null,
	MtiFormNumber nvarchar(255),
	GPBNumber nvarchar(255),
	BatchNumber nvarchar(255),
	Remark nvarchar(1000),
	RejectedPartDescription nvarchar(1000),
	RejectReason nvarchar(1000),
	NmrNumber nvarchar(50),
	CreatedTime datetime not null default getdate(),
	UpdatedTime datetime not null default getdate(),
	Status bit not null default 1,
	CONSTRAINT FK_InspectionRecord_Category FOREIGN KEY (CategoryId) REFERENCES [iqa_inspection_record_category](Id),
	CONSTRAINT FK_InspectionRecord_Activation FOREIGN KEY (ActivationId) REFERENCES [iqa_activation_request](Id),
	CONSTRAINT FK_InspectionRecord_Part FOREIGN KEY (PartId) REFERENCES [iqa_inventory_item](Id),
	CONSTRAINT FK_InspectionRecord_Manufacturer FOREIGN KEY (ManufacturerId) REFERENCES [iqa_manufacturer](Id),
	CONSTRAINT FK_InspectionRecord_Requestor FOREIGN KEY (RequestorId) REFERENCES [u_user_informations](user_id),
	CONSTRAINT FK_InspectionRecord_Inspector FOREIGN KEY (InspectorId) REFERENCES [u_user_informations](user_id)
)

CREATE TABLE [dbo].[iqa_lot_extension_request](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	ActivationId int not null,
	UserId int not null,
	LotCount int not null,
	Remark nvarchar(1000),
	CreatedTime datetime not null default getdate(),
	UpdatedTime datetime not null default getdate(),
	CONSTRAINT FK_ExtensionRequest_Activation FOREIGN KEY (ActivationId) REFERENCES [iqa_activation_request](Id),
	CONSTRAINT FK_ExtensionRequest_User FOREIGN KEY (UserId) REFERENCES [u_user_informations](user_id)
)

CREATE TABLE [dbo].[iqa_activation_document_update_request](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	ActivationId int not null,
	UserId int not null,
	TargetUserId int,
	ApproverId int,
	Status int not null default 1,
	Document nvarchar(255) not null,
	Remark nvarchar(1000),
	CreatedTime datetime not null default getdate(),
	UpdatedTime datetime not null default getdate(),
	CONSTRAINT FK_UpdateRequest_Activation FOREIGN KEY (ActivationId) REFERENCES [iqa_activation_request](Id),
	CONSTRAINT FK_UpdateRequest_User FOREIGN KEY (UserId) REFERENCES [u_user_informations](user_id),
	CONSTRAINT FK_UpdateRequest_TargetUser FOREIGN KEY (TargetUserId) REFERENCES [u_user_informations](user_id),
	CONSTRAINT FK_UpdateRequest_Approval FOREIGN KEY (ApproverId) REFERENCES [u_user_informations](user_id)
)

CREATE TABLE [dbo].[iqa_lot_extension_request_approval](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	RequestId int not null,
	UserId int not null,
	Status int not null default 0,
	Remark nvarchar(1000),
	CreatedTime datetime not null default getdate(),
	UpdatedTime datetime not null default getdate(),
	CONSTRAINT FK_LotExtensionApproval_Request FOREIGN KEY (RequestId) REFERENCES [iqa_lot_extension_request](Id),
	CONSTRAINT FK_LotExtensionApproval_User FOREIGN KEY (UserId) REFERENCES [u_user_informations](user_id)
)

CREATE TABLE [dbo].[iqa_activation_log](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	UserId int not null,
	ActivationId int not null,
	Description nvarchar(1000),
	CreatedTime datetime not null default getdate(),
	CONSTRAINT FK_IqaActivationLog_User FOREIGN KEY (UserId) REFERENCES [u_user_informations](user_id),
	CONSTRAINT FK_IqaActivationLog_Activation FOREIGN KEY (ActivationId) REFERENCES [iqa_activation_request](Id)
)

CREATE TABLE [dbo].[iqa_employee_manager](
	Id INT IDENTITY(1,1) PRIMARY KEY,
	UserId int not null,
	ManagerId int not null,
)

insert into dbo.[iqa_activation_status] (name) values ('Active')
insert into dbo.[iqa_activation_status] (name) values ('Pending Approval')
insert into dbo.[iqa_activation_status] (name) values ('Pending Lot Extension Approval')
insert into dbo.[iqa_activation_status] (name) values ('Completed')
insert into dbo.[iqa_activation_status] (name) values ('Pending Training Record')
insert into dbo.[iqa_activation_status] (name) values ('Request Activation')
insert into dbo.[iqa_activation_status] (name) values ('Reject')
insert into dbo.[iqa_activation_status] (name) values ('Pending Oracle Update')

insert into dbo.g_criteria (criteria, module, ready, type)
values('{"Database":"petafield","TableName":"iqa_inventory_item","Title":"","DisplayColumn":["Id", "PartNumber","Description"],"OnLoadEvent":"","LabelClass":"col-lg-3","InputClass":"col-lg-9","InputField":[{"ParentTitle":"","DisplayAs":"Id","Database":"petafield","TableName":"iqa_inventory_item","Id":"idTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Id","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":true,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Part Number","Database":"petafield","TableName":"iqa_inventory_item","Id":"partNumberTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"PartNumber","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Description","Database":"petafield","TableName":"iqa_inventory_item","Id":"descriptionTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Description","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Status","Database":"petafield","TableName":"iqa_inventory_item","Id":"statusTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Status","DefaultValue":"true","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null}]}',
'iqa_inventory_item', 1, 'form')

insert into dbo.g_criteria (criteria, module, ready, type)
values('{"Database":"petafield","TableName":"iqa_manufacturer","Title":"","DisplayColumn":["Id", "Name","Description"],"OnLoadEvent":"","LabelClass":"col-lg-3","InputClass":"col-lg-9","InputField":[{"ParentTitle":"","DisplayAs":"Id","Database":"petafield","TableName":"iqa_manufacturer","Id":"idTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Id","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":true,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Manufacturer Name","Database":"petafield","TableName":"iqa_manufacturer","Id":"nameTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Name","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Description","Database":"petafield","TableName":"iqa_manufacturer","Id":"descriptionTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Description","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null}, {"ParentTitle":"","DisplayAs":"Status","Database":"petafield","TableName":"iqa_manufacturer","Id":"statusTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Status","DefaultValue":"true","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null}]}',
'iqa_manufacturer', 1, 'form')

insert into dbo.g_criteria (criteria, module, ready, type)
values('{"Database":"petafield","TableName":"iqa_inventory","Title":"","DisplayColumn":["Id", "Name","Description"],"OnLoadEvent":"","LabelClass":"col-lg-3","InputClass":"col-lg-9","InputField":[{"ParentTitle":"","DisplayAs":"Id","Database":"petafield","TableName":"iqa_inventory","Id":"idTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Id","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":true,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Subinventory Name","Database":"petafield","TableName":"iqa_inventory","Id":"nameTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Name","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Description","Database":"petafield","TableName":"iqa_inventory","Id":"descriptionTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Description","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null}, {"ParentTitle":"","DisplayAs":"Status","Database":"petafield","TableName":"iqa_inventory","Id":"statusTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Status","DefaultValue":"true","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null}]}',
'iqa_inventory', 1, 'form')

insert into dbo.g_criteria (criteria, module, ready, type)
values('{"Database":"petafield","TableName":"iqa_inspection_record_category","Title":"","DisplayColumn":["Id", "Name","Description"],"OnLoadEvent":"","LabelClass":"col-lg-3","InputClass":"col-lg-9","InputField":[{"ParentTitle":"","DisplayAs":"Id","Database":"petafield","TableName":"iqa_inspection_record_category","Id":"idTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Id","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":true,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Category Name","Database":"petafield","TableName":"iqa_inspection_record_category","Id":"nameTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Name","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Description","Database":"petafield","TableName":"iqa_inspection_record_category","Id":"descriptionTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Description","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null}, {"ParentTitle":"","DisplayAs":"Status","Database":"petafield","TableName":"iqa_inspection_record_category","Id":"statusTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Status","DefaultValue":"true","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null}]}',
'iqa_inspection_record_category', 1, 'form')

insert into dbo.g_criteria (criteria, module, ready, type)
values('{"Database":"petafield","TableName":"iqa_part_location","Title":"","DisplayColumn":["Id", "Name","Description"],"OnLoadEvent":"","LabelClass":"col-lg-3","InputClass":"col-lg-9","InputField":[{"ParentTitle":"","DisplayAs":"Id","Database":"petafield","TableName":"iqa_part_location","Id":"idTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Id","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":true,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Part Location Name","Database":"petafield","TableName":"iqa_part_location","Id":"nameTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Name","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Description","Database":"petafield","TableName":"iqa_part_location","Id":"descriptionTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Description","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null}, {"ParentTitle":"","DisplayAs":"Status","Database":"petafield","TableName":"iqa_part_location","Id":"statusTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Status","DefaultValue":"true","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null}]}',
'iqa_part_location', 1, 'form')

insert into dbo.g_criteria (criteria, module, ready, type)
values('{"Database":"petafield","TableName":"iqa_setting","Title":"","DisplayColumn":["Id", "Name","Description", "Value"],"OnLoadEvent":"","LabelClass":"col-lg-3","InputClass":"col-lg-9","InputField":[{"ParentTitle":"","DisplayAs":"Id","Database":"petafield","TableName":"iqa_setting","Id":"idTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Id","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":true,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Setting Name","Database":"petafield","TableName":"iqa_setting","Id":"nameTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Name","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Description","Database":"petafield","TableName":"iqa_setting","Id":"descriptionTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Description","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null},
{"ParentTitle":"","DisplayAs":"Value","Database":"petafield","TableName":"iqa_setting","Id":"valueTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Value","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null},  {"ParentTitle":"","DisplayAs":"Updated Time","Database":"petafield","TableName":"iqa_setting","Id":"updateTimeTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"UpdatedTime","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":true,"CustomColumnProd":null,"CustomColumnId":null}]}',
'iqa_setting', 1, 'form')

insert into dbo.g_criteria (criteria, module, ready, type)
values('{"Database":"petafield","TableName":"iqa_employee_manager","Title":"","DisplayColumn":["Id", "ManagerId","UserId"],"OnLoadEvent":"","LabelClass":"col-lg-3","InputClass":"col-lg-9","InputField":[{"ParentTitle":"","DisplayAs":"Id","Database":"petafield","TableName":"iqa_employee_manager","Id":"idTxt","DataType":"string","InputType":"text","LinkedObj":"","Visible":"FALSE","Mandatory":"FALSE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"Id","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":true,"CustomColumnProd":null,"CustomColumnId":null},{"ParentTitle":"","DisplayAs":"Manager","Database":"petafield","TableName":"iqa_employee_manager","Id":"ManagerTxt","DataType":"string","InputType":"select-ds","DataSource":"u_user_informations","DataColumn":"user_id","DataDispColumn":"email","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"ManagerId","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null},
{"ParentTitle":"","DisplayAs":"User","Database":"petafield","TableName":"iqa_employee_manager","Id":"UserTxt","DataType":"string","InputType":"select-ds", "DataSource":"u_user_informations","DataColumn":"user_id","DataDispColumn":"email","LinkedObj":"","Visible":"TRUE","Mandatory":"TRUE","Disabled":"FALSE","PlaceHolderValue":"","ColumnName":"UserId","DefaultValue":"","Options":[],"Others":[],"CBFunction":[],"Excluded":false,"CustomColumnProd":null,"CustomColumnId":null}]}',
'iqa_employee_manager', 1, 'form')

--important if the startup is not executed successfully please change optimizeCompilations to false and restart then only change back to true

exec [CreateUserRole] 3000, 'iqa_module_manager', 'Division=1000&Manufacturer=1000&Site=1000&GeoLocation=1000' 
exec [CreateUserRole] 3001, 'iqa_module_inspector', 'Division=1000&Manufacturer=1000&Site=1000&GeoLocation=1000' 
exec [CreateUserRole] 3002, 'iqa_module_engineer', 'Division=1000&Manufacturer=1000&Site=1000&GeoLocation=1000' 
exec [CreateUserRole] 3003, 'iqa_module_employee', 'Division=1000&Manufacturer=1000&Site=1000&GeoLocation=1000' 
exec [CreateUserRole] 3004, 'iqa_module_personnel', 'Division=1000&Manufacturer=1000&Site=1000&GeoLocation=1000' 
exec [CreateUserRole] 3005, 'iqa_module_visitor', 'Division=1000&Manufacturer=1000&Site=1000&GeoLocation=1000'

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQADashboard'), 163000,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAConfiguration'), 163000,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAActivation'), 163000,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 163000,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 163000,1)

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQADashboard'), 123000,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAConfiguration'), 123000,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAActivation'), 123000,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 123000,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 123000,1)

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQADashboard'), 163001,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 163001,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 163001,1)

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQADashboard'), 123001,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 123001,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 123001,1)

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQADashboard'), 163002,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAConfiguration'), 163002,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAActivation'), 163002,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 163002,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 163002,1)

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQADashboard'), 123002,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAConfiguration'), 123002,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAActivation'), 123002,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 123002,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 123002,1)

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQADashboard'), 163003,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAActivation'), 163003,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 163003,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 163003,1)

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQADashboard'), 123003,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAActivation'), 123003,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 123003,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 123003,1)

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQADashboard'), 163004,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAActivation'), 163004,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 163004,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 163004,1)

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQADashboard'), 123004,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAActivation'), 123004,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 123004,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 123004,1)

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAActivation'), 163005,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 163005,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 163005,1)

insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAActivation'), 123005,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAInspection'), 123005,1)
insert into dbo.p_user_access_option_ui (user_access_option_id, guid, extended_role_id, status) values(newid(), (select Guid from dbo.p_menu_ui where MenuId = 'menuIQAReport'), 123005,1)

update dbo.u_extended_roles_ui set url = null where extended_custom_role_id >= 3000 and extended_custom_role_id <= 3005

ALTER TABLE dbo.iqa_inventory_item
ALTER COLUMN PartNumber nvarchar(100) COLLATE Latin1_General_BIN not null;
ALTER TABLE dbo.iqa_inventory_item
ALTER COLUMN Description nvarchar(500) COLLATE Latin1_General_BIN not null;

alter table dbo.iqa_inventory_item add unique(PartNumber)

ALTER TABLE dbo.iqa_activation_request
ALTER COLUMN OracleData nvarchar(200) COLLATE Latin1_General_BIN;

ALTER TABLE dbo.iqa_part_location
ADD IsValid bit not null default 1;