CREATE PROCEDURE [dbo].[IQA_DeactivationRequest]
@userId int
AS
BEGIN
	
select 'Deactivation' as TableName , concat(c.PartNumber collate SQL_Latin1_General_CP1_CI_AS,' (', format(b.ActivationDate, 'yyyy-MM-dd'),')') as Activation, b.Id, a.email, 
(b.ConsecutiveLotCount + b.AdditionalConsecutiveLotCount) as ConsecutiveCount
from dbo.iqa_activation_request as b
left join dbo.u_user_informations as a on a.user_id = b.ActivationUserId
left join dbo.iqa_inventory_item as c on c.Id = b.PartId
where b.Status = 2 and b.IsCancel = 0 and b.ActivationUserId = @userId

END
GO