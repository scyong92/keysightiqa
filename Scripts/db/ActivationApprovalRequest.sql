CREATE PROCEDURE [dbo].[IQA_ActivationApprovalRequest]
@status int
AS
BEGIN
	
select u.email as ActivationUser, d.Name as Inventory, e.PartNumber as Part, f.Name as PartLocation, FORMAT(b.ActivationDate, 'yyyy-MM-dd HH:mm:ss') as ActivationTime, 
b.ConsecutiveLotCount, b.RequestDocument, b.TrainingDocument, FORMAT(b.CreatedTime, 'yyyy-MM-dd HH:mm:ss') as CreationDate, b.Remark, 
(case when b.Status = 5 then 'TrainingRequest' else 'ActivationApproval' end) as TableName, b.Id
from dbo.iqa_activation_request as b
left join dbo.iqa_inventory_item as c on c.Id = b.PartId
left join dbo.u_user_informations as u on u.user_id = b.ActivationUserId
left join dbo.iqa_inventory as d on d.Id = b.InventoryId
left join dbo.iqa_inventory_item as e on e.Id = b.PartId
left join dbo.iqa_part_location as f on f.Id = b.PartLocationId
where b.Status = @status and b.IsCancel = 0

END
GO