CREATE PROCEDURE [dbo].[IQA_FailInspectionData]
AS
BEGIN
	
select c.email as Engineer, d.PartNumber, d.Description, a.RejectReason, 
(a.InspectedQty - a.AcceptedQty) as RejectQty, FORMAT(a.InspectionTime, 'MMM yyyy') as Month, FORMAT(a.InspectionTime, 'dd MMM yyyy') as InspectionDate, a.NmrNumber
from dbo.iqa_inspection_record as a 
left join dbo.iqa_activation_request as b on b.Id = a.ActivationId 
left join dbo.u_user_informations as c on c.user_id = a.RequestorId
left join dbo.iqa_inventory_item as d on d.Id = a.PartId
where a.status = 1 and a.InspectedQty != a.AcceptedQty and b.Status != 5 and CAST(a.InspectionTime AS DATE) >= CAST(DATEADD(month, -12, GETDATE()) AS DATE)

END
GO