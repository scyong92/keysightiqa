CREATE PROCEDURE [dbo].[IQA_NmrRequest]
@userId int
AS
BEGIN

select 'Nmr' as TableName, a.Id, concat(c.PartNumber collate SQL_Latin1_General_CP1_CI_AS, ' - ', format(a.InspectionTime, 'yyyy-MM-dd')) as InspectionRecord, 
concat(c.PartNumber collate SQL_Latin1_General_CP1_CI_AS,' - ', format(b.ActivationDate, 'yyyy-MM-dd')) as Activation from dbo.iqa_inspection_record as a 
left join dbo.iqa_activation_request as b on b.Id = a.ActivationId
left join dbo.iqa_inventory_item as c on c.Id = b.PartId
where (NmrNumber is null or NmrNumber = '') and AcceptedQty != InspectedQty and b.Status = 1 and a.RequestorId = @userId and b.IsCancel = 0

END
GO