CREATE PROCEDURE [dbo].[IQA_LotExtension]
@userId int
AS
BEGIN
	
select 'LotExtension' as TableName, concat(c.PartNumber collate SQL_Latin1_General_CP1_CI_AS,' - ', format(b.ActivationDate, 'yyyy-MM-dd')) as Activation , b.Id, d.email, a.LotCount
from dbo.iqa_lot_extension_request as a 
inner join (select * from dbo.iqa_lot_extension_request_approval where UserId = @userId and Status = 0) as e on e.RequestId = a.Id
left join dbo.iqa_activation_request as b on b.Id = a.ActivationId 
left join dbo.iqa_inventory_item as c on c.Id = b.PartId
left join dbo.u_user_informations as d on d.user_id = b.ActivationUserId
where b.Status = 3 and b.IsCancel = 0

END
GO