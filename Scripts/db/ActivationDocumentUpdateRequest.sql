CREATE PROCEDURE [dbo].[IQA_ActivationDocumentUpdateRequest]
AS
BEGIN
	
select FORMAT (a.CreatedTime, 'yyyy-MM-dd HH:mm:ss') as RequestDate, 
u.email, concat(c.PartNumber collate SQL_Latin1_General_CP1_CI_AS,' (',FORMAT (b.ActivationDate, 'yyyy-MM-dd') ,')') as Activation, 'ActivationDocument' as TableName, b.Id, a.Document
from dbo.iqa_activation_document_update_request as a 
left join dbo.iqa_activation_request  as b on b.Id = a.ActivationId
left join dbo.iqa_inventory_item as c on c.Id = b.PartId
left join dbo.u_user_informations as u on u.user_id = b.ActivationUserId
where a.status = 0 and (b.status = 1 or b.Status = 7 or b.Status = 6) and b.IsCancel = 0

END
GO