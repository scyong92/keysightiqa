﻿using Hangfire;
using Hangfire.Dashboard;
using IQA.Model;
using IQA.Services.SettingData;
using Microsoft.Owin;
using Owin;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http;
using System;
using IQA.Services;

[assembly: OwinStartup(typeof(IQA.App_Start.Startup))]
namespace IQA.App_Start
{
    public class Startup
    {
        private const string _REMINDER_TASK = "reminder";
        private const string _MONTHLY_REPORT_TASK = "monthlyReport";
        private const string _SYNC_INVENTORY_ITEM_TASK = "syncInventoryItem";

        public void Configuration(IAppBuilder app)
        {
            Setting.GetInstance().Init();

            Hangfire.GlobalConfiguration.Configuration.UseSqlServerStorage(ConfigurationManager.ConnectionStrings["HangFire"].ConnectionString);
            app.UseHangfireServer();
            app.UseHangfireDashboard("/hangfire" , new DashboardOptions
            {
                Authorization = new[] { new HangfireDashboardAuthorization() }
            });

            InitHangFire();
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        private void InitHangFire()
        {
            RecurringJob.AddOrUpdate(_REMINDER_TASK, () => new ActivationService().InitializeReminder(), "0 0 1 * * ?", TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate(_SYNC_INVENTORY_ITEM_TASK, () => new ConfigurationService().PerformSyncInventoryItemFromPlm(), "0 0 0 * * ?", TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate(_MONTHLY_REPORT_TASK, () => new ActivationService().GenerateMonthlyReport(), "0 0 2 * * ?", TimeZoneInfo.Local);
        }
    }
}
