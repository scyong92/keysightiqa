﻿using Hangfire;
using IQA.Database;
using IQA.Database.CustomModel;
using IQA.Database.DbModel;
using IQA.Services.SettingData;
using IQA.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Runtime.CompilerServices;
using System.Transactions;
using System.Web;
using Xcadia.Controllers.API;
using Xcadia.Models;

namespace IQA.Services
{
    public class ConfigurationService
    {
        private static ConfigurationService _instance;
        private static ConcurrentDictionary<int, int> _lock = new ConcurrentDictionary<int, int>();
        private const int _LOCK_KEY = 1;

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static ConfigurationService GetInstance()
        {
            if (_instance == null)
            {
                _instance = new ConfigurationService();
            }
            return _instance;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result CreateOrSaveConfiguration(string table, NameValueCollection formData)
        {
            Result result = new Result();
            try
            {
                Type tableType = DatabaseModel.GetTypeByName(table);
                bool isNewRecord = string.IsNullOrEmpty(formData["Id"]);

                if (tableType == typeof(iqa_inventory) || tableType == typeof(iqa_manufacturer) || tableType == typeof(iqa_setting))
                {
                    ValidateAgainstDuplication(formData, tableType, "Name");
                }
                else if (tableType == typeof(iqa_inventory_item))
                {
                    ValidateAgainstDuplication(formData, tableType, "PartNumber");
                }
                else if (tableType == typeof(iqa_employee_manager))
                {
                    ValidateAgainstDuplication(formData, tableType, "UserId");
                }

                if (!string.IsNullOrEmpty(formData["Status"]) &&
                    !string.IsNullOrEmpty(formData["Id"]) &&
                    formData["Status"].ToString().ToLower() == bool.FalseString.ToLower() &&
                    tableType.GetProperty(DatabaseService._STATUS_KEY_FOR_SOFT_DELETE) == null)
                {
                    result = DatabaseService.GetInstance().RemoveDataById(tableType, formData["Id"].ToString());
                }
                else
                {
                    result = DatabaseService.GetInstance().CreateOrSave(tableType, formData);
                }
            }
            catch(Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Message = CommonUtil.ExtractExceptionMessage(ex);
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public void ValidateAgainstDuplication(NameValueCollection formData, Type type, string targetColumn)
        {
            try
            {
                if (!string.IsNullOrEmpty(formData[targetColumn]))
                {
                    IQAEntities dbContext = new IQAEntities();
                    DatabaseModel matchModel = dbContext.Set(type).AsQueryable().ToDynamicList().FirstOrDefault(i => ((DatabaseModel)i)[targetColumn].ToString() == formData[targetColumn]);

                    if (matchModel != null)
                    {
                        if (!string.IsNullOrEmpty(formData["Id"]))
                        {
                            int id = int.Parse(formData["Id"].ToString());

                            if (matchModel["Id"].ToString() != id.ToString())
                            {
                                throw new Exception("Duplicate Configuration detected");
                            }
                        }
                        else
                        {
                            throw new Exception("Duplicate Configuration detected");
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result PerformSyncInventoryItemFromPlm()
        {
            return SyncInventoryItemFromPlm(DateTime.Now.AddDays(-1).Date);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result SyncInventoryItemFromPlm(DateTime date)
        {
            Result result = new Result();
            try
            {
                if (_lock.TryAdd(_LOCK_KEY, _LOCK_KEY))
                {
                    IQAEntities dbContext = new IQAEntities();
                    var itemCount = dbContext.iqa_inventory_item.Count();

                    using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Plm"].ConnectionString))
                    {
                        string query = @"select * from (
	                                        SELECT PART_NUMBER as PartNumber, DESCRIPTION as Description, INVENTORY_ITEM_ID as Id, 1 as IsActive, LAST_UPDATE_DATE
	                                        FROM vMTL_SYSTEM_ITEMS
	                                        WHERE ORGANIZATION_ID = 181
	                                        and ITEM_TYPE != 'II' and ITEM_TYPE != 'IA' and INVENTORY_ITEM_STATUS_CODE NOT IN ('Obsolete','Inactive Item')
	                                        union 
	                                        SELECT PART_NUMBER as PartNumber, DESCRIPTION as Description, INVENTORY_ITEM_ID as Id, 0 as IsActive, LAST_UPDATE_DATE
	                                        FROM vMTL_SYSTEM_ITEMS
	                                        WHERE ORGANIZATION_ID = 181
	                                        and (ITEM_TYPE = 'II' or ITEM_TYPE = 'IA') and INVENTORY_ITEM_STATUS_CODE IN ('Obsolete','Inactive Item')
                                        ) as a ";

                        if (itemCount != 0)
                        {
                            //mean this is not fresh fetch, then we have to pull only updated data
                            query += " where LAST_UPDATE_DATE >= '" + date.Date.ToString("yyyy-MM-dd") + "'";
                        }

                        SqlCommand sqlCmd = new SqlCommand(query, conn);
                        conn.Open();

                        List<PlmInventoryItemModel> inventoryItemList = new List<PlmInventoryItemModel>();
                        using (SqlDataReader sqlReader = sqlCmd.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                PlmInventoryItemModel item = new PlmInventoryItemModel();
                                item.PartNumber = sqlReader["PartNumber"].ToString().TrimEnd('\r', '\n').Replace("'","''");
                                item.Description = sqlReader["Description"].ToString().TrimEnd('\r', '\n').Replace("'", "''");
                                item.Id = int.Parse(sqlReader["Id"].ToString());
                                item.IsActive = sqlReader["IsActive"].ToString().TrimEnd('\r', '\n').Replace("'", "''");
                                inventoryItemList.Add(item);
                            }

                            conn.Close();
                        }

                        List<string> createOrUpdateQuery = new List<string>();

                        if (itemCount == 0)
                        {
                            //fresh insert, just perform full insert will do
                            foreach (var ea in inventoryItemList)
                            {
                                string itemQuery = "insert into dbo.iqa_inventory_item (Id, PartNumber, Status, Description) values(" + ea.Id + ",'" + ea.PartNumber + "'," + ea.IsActive + ", '" + ea.Description + "');";
                                createOrUpdateQuery.Add(itemQuery);
                            }
                        }
                        else
                        {
                            foreach (var ea in inventoryItemList)
                            {
                                iqa_inventory_item item = dbContext.iqa_inventory_item.SingleOrDefault(i => i.PartNumber == ea.PartNumber);
                                string itemQuery = string.Empty;
                                if (item != null)
                                {
                                    itemQuery = "update dbo.iqa_inventory_item set PartNumber = '" + ea.PartNumber + "', Status = " + ea.IsActive + ", Description = '" + ea.Description + "' where Id = " + ea.Id + ";";
                                }
                                else
                                {
                                    itemQuery = "insert into dbo.iqa_inventory_item (Id, PartNumber, Status, Description) values(" + ea.Id + ",'" + ea.PartNumber + "'," + ea.IsActive + ", '" + ea.Description + "');";
                                }

                                createOrUpdateQuery.Add(itemQuery);
                            }
                        }

                        if (createOrUpdateQuery.Count != 0)
                        {
                            var transactionOptions = new TransactionOptions();
                            transactionOptions.IsolationLevel = IsolationLevel.ReadUncommitted;
                            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
                            {
                                IQAEntities innerContext = new IQAEntities();
                                innerContext.Database.CommandTimeout = 300;

                                List<string> executionQuery = new List<string>();
                                for (var i = 0; i < createOrUpdateQuery.Count; i++)
                                {
                                    executionQuery.Add(createOrUpdateQuery[i]);

                                    if (i % 10000 == 0 || i == createOrUpdateQuery.Count - 1)
                                    {
                                        executionQuery.Insert(0, "SET IDENTITY_INSERT dbo.iqa_inventory_item ON;");
                                        executionQuery.Add("SET IDENTITY_INSERT dbo.iqa_inventory_item OFF;");
                                        innerContext.Database.ExecuteSqlCommand(string.Join("", executionQuery));

                                        executionQuery.Clear();
                                    }
                                }

                                scope.Complete();
                            }
                        }

                        result.Message = "("+ createOrUpdateQuery.Count + ") records synchronized successfully";
                    }
                }
                else
                {
                    result.Message = "Synchronization is in progress, please try again later";
                }
            }
            catch(Exception ex)
            {
                result.Message = ex.StackTrace;
                result.Code = (int)HttpStatusCode.InternalServerError;
            }
            finally
            {
                _lock.TryRemove(_LOCK_KEY, out _);
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result GetManager(int userId)
        {
            Result result = new Result();
            try
            {
                IQAEntities innerContext = new IQAEntities();
                iqa_employee_manager userManager = innerContext.iqa_employee_manager.SingleOrDefault(i => i.UserId == userId);

                if (userManager != null)
                {
                    result.ReturnObject = userManager.ManagerId;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Code = (int)HttpStatusCode.InternalServerError;
            }

            return result;
        }
    }
}