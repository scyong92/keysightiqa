﻿using IQA.Utils;
using IQA.Database;
using IQA.Database.CustomModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web;
using IQA.Database.DbModel;
using System.Linq.Expressions;

namespace IQA.Services
{
    public class DatabaseService
    {
        public const string _STATUS_KEY_FOR_SOFT_DELETE = "Status";
        private static DatabaseService _instance;

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static DatabaseService GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DatabaseService();
            }
            return _instance;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result GetSingleDataByName(string tableName, string id, bool withDetail = true)
        {
            Result result = new Result();
            try
            {
                IQAEntities db = new IQAEntities();
                Type tableType = DatabaseModel.GetTypeByName(tableName);
                IQueryable queryableData = db.Set(tableType).AsQueryable();

                queryableData = queryableData.Where("Id == "+ id);
                DatabaseModel model = queryableData.ToDynamicList().FirstOrDefault(i => ((DatabaseModel)i)["Id"].ToString() == id);
                result.ReturnObject = model.ConvertFromDbModel(withDetail);
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Message = CommonUtil.ExtractExceptionMessage(ex);
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result GetDataByName(string tableName, int offset =0, int limit = 0, bool ignoreSoftDeleted = true, bool withDetail = false)
        {
            Result result = new Result();
            try
            {
                int totalRecord = 0;
                IQAEntities db = new IQAEntities();
                Type tableType = DatabaseModel.GetTypeByName(tableName);

                IQueryable queryableData = db.Set(tableType).AsQueryable();

                if (ignoreSoftDeleted &&
                    tableType.GetProperty(_STATUS_KEY_FOR_SOFT_DELETE) != null &&
                    tableType.GetProperty(_STATUS_KEY_FOR_SOFT_DELETE).PropertyType == typeof(bool))
                {
                    queryableData = queryableData.Where("Status == true");
                }

                if (limit != 0)
                {
                    totalRecord = queryableData.Count();
                    queryableData = queryableData.OrderBy("Id").Skip(offset).Take(limit);
                }

                List<DatabaseModel> dataList = queryableData.ToDynamicList().Select(i => (DatabaseModel)i).ToList();

                var acquiredData = dataList.Select(i => ((DatabaseModel)i).ConvertFromDbModel(withDetail)).ToList();

                if (limit == 0)
                {
                    result.ReturnObject = acquiredData;
                }
                else
                {
                    result.ReturnObject = new { TotalCount = totalRecord, Data = acquiredData };
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Message = CommonUtil.ExtractExceptionMessage(ex);
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result GetData<T>(bool ignoreSoftDeleted = true, bool withDetail = false)
        {
            Result result = new Result();
            try
            {
                IQAEntities db = new IQAEntities();
                List<DatabaseModel> dataList = db.Set(typeof(T)).OfType<T>().ToList().Cast<DatabaseModel>().ToList();

                if (ignoreSoftDeleted && 
                    typeof(T).GetProperty(_STATUS_KEY_FOR_SOFT_DELETE) != null && 
                    typeof(T).GetProperty(_STATUS_KEY_FOR_SOFT_DELETE).PropertyType == typeof(bool))
                {
                    dataList = dataList.Where(i => (bool)i[_STATUS_KEY_FOR_SOFT_DELETE]).ToList();
                }

                result.ReturnObject = dataList.Select(i => i.ConvertFromDbModel(withDetail)).ToList();
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Message = CommonUtil.ExtractExceptionMessage(ex);
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result GetDataById<T>(object id, string primaryKeyName = "Id", bool withDetail = true)
        {
            Result result = new Result();
            try
            {
                IQAEntities db = new IQAEntities();
                var modelType = typeof(T);
                PropertyInfo prop = modelType.GetProperties().FirstOrDefault(i => i.Name == primaryKeyName);
                object idData = TypeDescriptor.GetConverter(prop.PropertyType).ConvertFromInvariantString(id.ToString());

                DatabaseModel model = (DatabaseModel)db.Set(typeof(T)).Find(idData);

                result.ReturnObject = model.ConvertFromDbModel(withDetail);
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Message = CommonUtil.ExtractExceptionMessage(ex);
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result RemoveDataById(Type modelType, object id, string primaryKeyName = "Id")
        {
            Result result = new Result();
            try
            {
                IQAEntities db = new IQAEntities();
                PropertyInfo prop = modelType.GetProperties().FirstOrDefault(i => i.Name == primaryKeyName);
                object idData = TypeDescriptor.GetConverter(prop.PropertyType).ConvertFromInvariantString(id.ToString());
                DatabaseModel model = (DatabaseModel)db.Set(modelType).Find(idData);

                db.Set(modelType).Remove(model);

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Message = CommonUtil.ExtractExceptionMessage(ex);
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result CreateOrSave<T>(NameValueCollection formCollection, string primaryKeyName = "Id")
        {
            return CreateOrSave(typeof(T), formCollection, primaryKeyName);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result CreateOrSave(Type type, NameValueCollection formCollection, string primaryKeyName = "Id")
        {
            Result result = new Result();
            try
            {
                IQAEntities dbContext = new IQAEntities();
                Type modelType = type;
                DatabaseModel data = null;

                string primaryColumnName = primaryKeyName[0].ToString().ToUpper() + primaryKeyName.Substring(1);
                PropertyInfo primaryPropInfo = modelType.GetProperties().FirstOrDefault(i => i.Name == primaryColumnName);

                if (!string.IsNullOrEmpty(formCollection[primaryKeyName]))
                {
                    object id = TypeDescriptor.GetConverter(primaryPropInfo.PropertyType).ConvertFromInvariantString(formCollection[primaryKeyName].ToString());
                    data = (DatabaseModel)dbContext.Set(type).Find(id);
                }

                if (data == null)
                {
                    data = (DatabaseModel)Activator.CreateInstance(type);

                    if (!string.IsNullOrEmpty(formCollection[primaryKeyName]))
                    {
                        data[primaryColumnName] = TypeDescriptor.GetConverter(primaryPropInfo.PropertyType).ConvertFromInvariantString(formCollection[primaryKeyName]);
                    }

                    if (data.GetType().GetProperties().FirstOrDefault(i => i.Name == "CreatedTime") != null)
                    {
                        data["CreatedTime"] = DateTime.Now;
                    }
                    dbContext.Set(type).Add(data);
                }

                if (data.GetType().GetProperties().FirstOrDefault(i => i.Name == "UpdatedTime") != null)
                {
                    data["UpdatedTime"] = DateTime.Now;
                }

                List<string> keyList = formCollection.AllKeys.ToList();
                foreach (PropertyInfo prop in data.GetType().GetProperties())
                {
                    string propName = prop.Name;

                    if (prop.Module.Name.Contains("dll") && propName != primaryKeyName && keyList.Contains(propName))
                    {
                        var formKey = "";

                        if (!string.IsNullOrEmpty(formCollection[propName]))
                        {
                            formKey = formCollection[propName].ToString();
                        }

                        Type dataType = (Nullable.GetUnderlyingType(prop.PropertyType) != null) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType;

                        if (string.IsNullOrEmpty(formKey) && Nullable.GetUnderlyingType(prop.PropertyType) != null)
                        {
                            data[prop.Name] = null;
                        }
                        else
                        {
                            data[prop.Name] = TypeDescriptor.GetConverter(dataType).ConvertFromInvariantString(formKey);
                        }
                    }
                }

                dbContext.SaveChanges();
                result.ReturnObject = new { id = data[primaryColumnName] };
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Message = CommonUtil.ExtractExceptionMessage(ex);
            }

            return result;
        }
    }
}