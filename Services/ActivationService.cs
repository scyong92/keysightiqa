﻿using Hangfire;
using IQA.Database;
using IQA.Database.CustomModel;
using IQA.Database.DbModel;
using IQA.Model;
using IQA.Services.SettingData;
using IQA.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Transactions;
using System.Web;
using Xcadia.Controllers.API;
using Xcadia.Models;

namespace IQA.Services
{
    public class ActivationService
    {
        private static ActivationService _instance;

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static ActivationService GetInstance()
        {
            if (_instance == null)
            {
                _instance = new ActivationService();
            }
            return _instance;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result GetActivationData(InpectionFilterModel filter)
        {
            Result result = new Result();

            try
            {
                IQAEntities dbContext = new IQAEntities();
                IQueryable<iqa_activation_request> activationRecordQuery = dbContext.iqa_activation_request.AsQueryable();

                if (!string.IsNullOrEmpty(filter.PartNumber))
                {
                    List<int> partList = dbContext.iqa_inventory_item.Where(i => i.PartNumber.Contains(filter.PartNumber)).Select(i => i.Id).ToList();
                    activationRecordQuery = activationRecordQuery.Where(i => partList.Contains(i.PartId));
                }

                if (filter.InventoryId != null)
                {
                    activationRecordQuery = activationRecordQuery.Where(i => filter.InventoryId.Contains(i.InventoryId));
                }

                if (filter.StartTime.HasValue)
                {
                    activationRecordQuery = activationRecordQuery.Where(i => i.ActivationDate >= filter.StartTime.Value);
                }

                if (filter.EndTime.HasValue)
                {
                    activationRecordQuery = activationRecordQuery.Where(i => i.ActivationDate <= filter.EndTime.Value);
                }

                if (filter.UserId.HasValue)
                {
                    activationRecordQuery = activationRecordQuery.Where(i => i.ActivationUserId == filter.UserId.Value);
                }

                if (filter.limit != 0)
                {
                    int totalRecord = activationRecordQuery.Count();
                    activationRecordQuery = activationRecordQuery.Skip(filter.offset).Take(filter.limit);
                }

                result.ReturnObject = activationRecordQuery.ToList().Select(i => i.ConvertFromDbModel()).ToList();
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Message = CommonUtil.ExtractExceptionMessage(ex);
            }

            return result;
        }

        /// <summary>iq
        /// Author : 01016781
        /// </summary>
        public Result GetActivationLog(int activationId)
        {
            Result result = new Result();

            try
            {
                IQAEntities dbContext = new IQAEntities();
                result.ReturnObject = dbContext.iqa_activation_log.Where(i => i.ActivationId == activationId).ToList().Select(k => ActivationLogModel.ConvertModel(k)).ToList();
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Message = CommonUtil.ExtractExceptionMessage(ex);
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result GetInspectionData(InpectionFilterModel filter)
        {
            Result result = new Result();

            try
            {
                IQAEntities dbContext = new IQAEntities();
                IQueryable<iqa_inspection_record> inspectionRecordQuery = dbContext.iqa_inspection_record.AsQueryable();

                if (!string.IsNullOrEmpty(filter.PartNumber))
                {
                    List<int> partList = dbContext.iqa_inventory_item.Where(i => i.PartNumber.Contains(filter.PartNumber)).Select(i => i.Id).ToList();
                    inspectionRecordQuery = inspectionRecordQuery.Where(i => partList.Contains(i.PartId));
                }

                if (filter.ManufacturerId != null)
                {
                    inspectionRecordQuery = inspectionRecordQuery.Where(i => filter.ManufacturerId.Contains(i.ManufacturerId));
                }

                if (filter.ActivationId != null)
                {
                    inspectionRecordQuery = inspectionRecordQuery.Where(i => filter.ActivationId.Contains(i.ActivationId));
                }

                if (filter.StartTime.HasValue)
                {
                    inspectionRecordQuery = inspectionRecordQuery.Where(i => i.InspectionTime >= filter.StartTime.Value);
                }

                if (filter.EndTime.HasValue)
                {
                    inspectionRecordQuery = inspectionRecordQuery.Where(i => i.InspectionTime <= filter.EndTime.Value);
                }

                if (filter.UserId.HasValue)
                {
                    inspectionRecordQuery = inspectionRecordQuery.Where(i => i.RequestorId == filter.UserId.Value || i.InspectorId == filter.UserId.Value);
                }

                inspectionRecordQuery = inspectionRecordQuery.OrderByDescending(i => i.ReceivedTime).ThenByDescending(i => i.PartId).ThenByDescending(i => i.BatchNumber);

                if (filter.limit != 0)
                {
                    int totalRecord = inspectionRecordQuery.Count();
                    inspectionRecordQuery = inspectionRecordQuery.Skip(filter.offset).Take(filter.limit);
                }

                result.ReturnObject = inspectionRecordQuery.ToList().Select(i => i.ConvertFromDbModel()).ToList();
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Message = CommonUtil.ExtractExceptionMessage(ex);
            }

            return result;
        }

        private string SaveFile(HttpPostedFile file, string prefix)
        {
            var relativePath = "";
            if (file != null && file.ContentLength != 0)
            {
                relativePath = CommonUtil.GetActivationDirectory(DateTime.Now.ToString("yyyy-MM"));
                string directory = AppDomain.CurrentDomain.BaseDirectory + relativePath;
                Directory.CreateDirectory(directory);

                string epoch = CommonUtil.GetEpochTime().ToString();
                relativePath += "\\" + prefix + "_" + epoch + Path.GetExtension(file.FileName);
                directory += "\\" + prefix + "_" + epoch + Path.GetExtension(file.FileName);
                file.SaveAs(directory);
            }

            return relativePath;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result CreateOrUpdateActivationRequest(NameValueCollection postData, int userId, HttpPostedFile requestFile = null, HttpPostedFile trainingFile = null)
        {
            Result result = new Result();

            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = IsolationLevel.ReadUncommitted;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                try
                {
                    IQAEntities dbContext = new IQAEntities();
                    Database.DbModel.u_user_informations controlUser = dbContext.u_user_informations.SingleOrDefault(i => i.user_id == userId);

                    iqa_activation_log log = new iqa_activation_log();
                    log.UserId = userId;
                    log.CreatedTime = DateTime.Now;

                    ValidateActivationRequest(postData);

                    bool isNewRecord = string.IsNullOrEmpty(postData["Id"]);

                    var trainingFileRelativePath = SaveFile(trainingFile, "activationTraining");
                    var requestFileRelativePath = SaveFile(requestFile, "activation");

                    NameValueCollection formData = new NameValueCollection();

                    foreach (var ea in postData)
                    {
                        formData.Add(ea.ToString(), postData[ea.ToString()]);
                    }

                    if (!string.IsNullOrEmpty(requestFileRelativePath))
                    {
                        formData.Add("RequestDocument", requestFileRelativePath);
                    }

                    if (!string.IsNullOrEmpty(trainingFileRelativePath))
                    {
                        formData.Add("TrainingDocument", trainingFileRelativePath);
                    }

                    if (!string.IsNullOrEmpty(postData["PartNumber"]) && string.IsNullOrEmpty(postData["PartId"]))
                    {
                        var targetPartNumber = postData["PartNumber"].ToString();
                        iqa_inventory_item item = dbContext.iqa_inventory_item.FirstOrDefault(i => i.PartNumber == targetPartNumber);

                        formData.Remove("PartId");
                        formData.Add("PartId", item.Id.ToString());
                    }

                    gSendMailMsg reqMsg = new gSendMailMsg();

                    string emailSubject = "";
                    string formattedEmailContent = "";
                    List<string> emailReceiver = new List<string>();

                    if (isNewRecord)
                    {
                        OracleSettingObject dataObj = new OracleSettingObject();

                        formData.Add("OracleData", JsonConvert.SerializeObject(dataObj));
                        formData.Add("Status", ActivationStatusEnum._REQUEST_ACTIVATION.GetId().ToString());
                        formData.Add("ActivationDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(postData["Status"]))
                        {
                            //now check if it is changing of status, because different status need many different handling
                            IQAEntities iqaEntity = new IQAEntities();
                            int status = int.Parse(postData["Status"]);
                            int id = int.Parse(postData["Id"]);
                            iqa_activation_request activationRequest = iqaEntity.iqa_activation_request.SingleOrDefault(i => i.Id == id);
                            if (activationRequest.Status != status || (activationRequest.IsCancel && status != ActivationStatusEnum._CANCELED.GetId()))
                            {
                                if (!activationRequest.IsCancel)
                                {
                                    if (status == ActivationStatusEnum._PENDING_LOT_EXTENSION_APPROVAL.GetId())
                                    {
                                        //now we need notify manager to entertain this request
                                        //now we trigger email
                                        emailSubject = "LOT Extension Request for Activation with Part - " + activationRequest.InventoryItem.PartNumber;
                                        formattedEmailContent = "This message is to notify you that " + activationRequest.ActivationUser.email +
                                            " request for LOT extension for Activation Request with Part " + activationRequest.InventoryItem.PartNumber;

                                        string managerQuery = "select concat(u.user_id, ',' , u.email) from [u_user_to_store_ui] b inner join [u_user_to_role_ui] c on " +
                                                              "b.[user_to_store_id] = c.[user_to_store_id] inner join[u_extended_roles_ui] d on c.extended_role_id = d.extended_role_id " +
                                                              "inner join [u_extended_custom_roles_ui] e on d.extended_custom_role_id = e.extended_custom_role_id " +
                                                              "left join dbo.u_user_informations as u on u.user_id = b.user_id " +
                                                              "where extended_custome_role_name = '" + RoleEnum._MANAGER.GetKey() + "'";

                                        List<string> managers = iqaEntity.Database.SqlQuery<string>(managerQuery).ToList();

                                        emailReceiver = managers.Select(i => i.Split(',')[1]).ToList();

                                        iqa_lot_extension_request extensionRequest = new iqa_lot_extension_request();
                                        extensionRequest.ActivationId = id;
                                        extensionRequest.CreatedTime = DateTime.Now;
                                        extensionRequest.UpdatedTime = DateTime.Now;
                                        extensionRequest.UserId = userId;
                                        extensionRequest.LotCount = int.Parse(formData["LotCount"]);
                                        iqaEntity.iqa_lot_extension_request.Add(extensionRequest);
                                        iqaEntity.SaveChanges();

                                        var userTargetManagerToApprove = int.Parse(formData["Managers"].ToString());

                                        iqa_employee_manager userManager = iqaEntity.iqa_employee_manager.SingleOrDefault(i => i.UserId == activationRequest.ActivationUserId);

                                        if (userManager == null)
                                        {
                                            userManager = new iqa_employee_manager();
                                            userManager.UserId = activationRequest.ActivationUserId;
                                            iqaEntity.iqa_employee_manager.Add(userManager);
                                        }

                                        userManager.ManagerId = userTargetManagerToApprove;

                                        List<int> managersToApprove = new List<int> { userTargetManagerToApprove };
                                        if (managers.Count != 0)
                                        {
                                            managersToApprove.Add(int.Parse(managers.FirstOrDefault().Split(',')[0]));
                                        }

                                        foreach (var ea in managersToApprove)
                                        {
                                            iqa_lot_extension_request_approval approval = new iqa_lot_extension_request_approval();
                                            approval.UserId = ea;
                                            approval.Status = CommonUtil._STATUS_PENDING;
                                            approval.RequestId = extensionRequest.Id;
                                            approval.CreatedTime = DateTime.Now;
                                            approval.UpdatedTime = DateTime.Now;
                                            iqaEntity.iqa_lot_extension_request_approval.Add(approval);
                                        }

                                        iqaEntity.SaveChanges();

                                        log.Description = "Requested LOT Extension (" + extensionRequest.LotCount + ") by " + controlUser.email;
                                    }
                                    else if (status == ActivationStatusEnum._PENDING_APPROVAL.GetId() &&
                                             activationRequest.Status == ActivationStatusEnum._ACTIVE.GetId())
                                    {
                                        emailSubject = "IQA Incoming Deactivation for Part - " + activationRequest.InventoryItem.PartNumber;
                                        formattedEmailContent = "Below is the inspection data.<br /><br />";

                                        formattedEmailContent += "<table style='border: 1px solid #000000;border-collapse: collapse;'><thead><tr><th style='border: 1px solid #000000; padding:5px'>Received Date</th>" +
                                                           "<th style='border: 1px solid #000000; padding:5px'>Part Number</th><th style='border: 1px solid #000000; padding:5px'>Manufacturer</th><th style='border: 1px solid #000000; padding:5px'>Requestor</th>" +
                                                           "<th style='border: 1px solid #000000; padding:5px'>Inspected Quantity</th><th style='border: 1px solid #000000; padding:5px'>Accepted Qty</th>" +
                                                           "<th style='border: 1px solid #000000; padding:5px'>Rejected Quantity</th><th style='border: 1px solid #000000; padding:5px'>Status</th></tr></thead><tbody>";

                                        List<iqa_inspection_record> inspectionRecord = activationRequest.InspectionRecord.Where(i => i.Status).ToList();
                                        foreach (var ea in inspectionRecord)
                                        {
                                            InspectionRecordModel recordModel = ea.ConvertFromDbModel();

                                            if (recordModel.RecordStatus == InspectionRecordModel._RECORD_COMPLETED_STATUS)
                                            {
                                                var resultStatus = recordModel.InspectedQty - recordModel.AcceptedQty == 0 ? "Pass" : "Fail";
                                                formattedEmailContent += "<tr><td style='border: 1px solid #000000; padding:5px'>" + recordModel.ReceivedTime + "</td><td style='border: 1px solid #000000; padding:5px'>" + recordModel.PartNumber + "</td>" +
                                                    "<td style='border: 1px solid #000000; padding:5px'>" + recordModel.Manufacturer + "</td><td style='border: 1px solid #000000; padding:5px'>" + recordModel.Requestor + "</td>" +
                                                    "<td style='border: 1px solid #000000; padding:5px'>" + recordModel.InspectedQty + "</td><td style='border: 1px solid #000000; padding:5px'>" + recordModel.AcceptedQty + "</td>" +
                                                    "<td style='border: 1px solid #000000; padding:5px'>" + (recordModel.InspectedQty - recordModel.AcceptedQty) + "</td><td style='border: 1px solid #000000; padding:5px'>" + resultStatus + "</td></tr>";
                                            }
                                        }

                                        formattedEmailContent += "</tbody></table>";

                                        emailReceiver = new List<string> { activationRequest.ActivationUser.email };

                                        log.Description = "Activation completed with consecutive count (" + (activationRequest.AdditionalConsecutiveLotCount + activationRequest.ConsecutiveLotCount) + ")";
                                    }
                                    else if (status == ActivationStatusEnum._CANCELED.GetId())
                                    {
                                        //handling cancelation of request
                                        formData.Remove("Status");
                                        formData.Add("IsCancel", bool.TrueString);

                                        log.Description = "Activation canceled with by " + controlUser.email;
                                    }
                                    else if ((status == ActivationStatusEnum._ACTIVE.GetId() || status == ActivationStatusEnum._PENDING_APPROVAL.GetId()) &&
                                             activationRequest.Status == ActivationStatusEnum._PENDING_LOT_EXTENSION_APPROVAL.GetId())
                                    {
                                        //mean it is either approved or rejected for extension request
                                        emailSubject = "Lot Extension Request for Part - " + activationRequest.InventoryItem.PartNumber;

                                        iqa_lot_extension_request extensionRequest = activationRequest.LotExtensionRequest
                                            .SingleOrDefault(i => i.LotExtensionApproval.SingleOrDefault(k => k.Status == CommonUtil._STATUS_PENDING && k.UserId == userId) != null);

                                        if (extensionRequest!= null)
                                        {
                                            iqa_lot_extension_request_approval lotApprovalRequest = extensionRequest.LotExtensionApproval.SingleOrDefault(i => i.UserId == userId);

                                            if (!string.IsNullOrEmpty(formData["Remark"]))
                                            {
                                                lotApprovalRequest.Remark = formData["Remark"].ToString();
                                            }

                                            if (!string.IsNullOrEmpty(formData["ExtensionStatus"]) && formData["ExtensionStatus"].ToString().ToLower() == bool.TrueString.ToLower())
                                            {
                                                lotApprovalRequest.Status = CommonUtil._STATUS_APPROVED;

                                                log.Description = "Lot Extension Request approved by " + controlUser.email;
                                                if (extensionRequest.LotExtensionApproval.FirstOrDefault(i => i.UserId != userId && i.Status == CommonUtil._STATUS_PENDING) != null)
                                                {
                                                    formData.Remove("Status");
                                                }
                                                else
                                                {
                                                    log.Description = ", Lot Extension Request all approval acquired";
                                                    formData.Add("AdditionalConsecutiveLotCount", (activationRequest.AdditionalConsecutiveLotCount + extensionRequest.LotCount).ToString());
                                                    formattedEmailContent = "This message is to notify you that Lot Extension Request with Part " + activationRequest.InventoryItem.PartNumber + " is approved"; emailReceiver = new List<string> { activationRequest.ActivationUser.email };
                                                }
                                            }
                                            else
                                            {
                                                lotApprovalRequest.Status = CommonUtil._STATUS_REJECTED;
                                                log.Description = "Lot Extension Request rejected by " + controlUser.email;
                                                formattedEmailContent = "This message is to notify you that Lot Extension Request with Part " + activationRequest.InventoryItem.PartNumber + " is rejected by " + controlUser.email + "";

                                                if (!string.IsNullOrEmpty(formData["Remark"]))
                                                {
                                                    formattedEmailContent += " due to " + formData["Remark"];
                                                }
                                                else
                                                {
                                                    throw new Exception("Please specify reason in remark");
                                                }

                                                emailReceiver = new List<string> { activationRequest.ActivationUser.email };
                                            }

                                            iqaEntity.SaveChanges();
                                        }
                                        else
                                        {
                                            throw new Exception("Request is approved");
                                        }
                                    }
                                    else if (status == ActivationStatusEnum._COMPLETED.GetId())
                                    {
                                        log.Description = "Activation Completed by " + controlUser.email;
                                        formData.Add("DeactivationDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                    }
                                    else if (status == ActivationStatusEnum._REQUEST_ACTIVATION.GetId())
                                    {
                                        emailSubject = "Activation Request for Part - " + activationRequest.InventoryItem.PartNumber;
                                        formData.Add("Remark", string.Empty);

                                        if (string.IsNullOrEmpty(formData["TrainingDocument"]))
                                        {
                                            log.Description = controlUser.email + "requested Activation for part " + activationRequest.InventoryItem.PartNumber;

                                            formattedEmailContent = "This message is to notify you that Activation with Part " + activationRequest.InventoryItem.PartNumber + " is requested";
                                        }
                                        else
                                        {
                                            log.Description = controlUser.email + "requested Activation for part " + activationRequest.InventoryItem.PartNumber + " with training document provided";

                                            formattedEmailContent = "This message is to notify you that Activation with Part " + activationRequest.InventoryItem.PartNumber + " is requested and training document is provided";
                                        }

                                        emailReceiver = GetUserList(RoleEnum._ENGINEER);
                                    }
                                    else if (status == ActivationStatusEnum._ACTIVE.GetId() &&
                                             activationRequest.Status == ActivationStatusEnum._REQUEST_ACTIVATION.GetId())
                                    {
                                        //handle case when user approve activation request and switch it to pending training document
                                        //else set it to active if already have training document
                                        emailSubject = "Activation Request for Part - " + activationRequest.InventoryItem.PartNumber + " is approved";

                                        if (string.IsNullOrEmpty(activationRequest.TrainingDocument))
                                        {
                                            formData.Remove("Status");
                                            formData.Add("Status", ActivationStatusEnum._PENDING_TRAINING_RECORD.GetId().ToString());
                                            log.Description = "Activation Approved by " + controlUser.email + " and pending training document";

                                            emailReceiver = new List<string> { activationRequest.ActivationUser.email };
                                            formattedEmailContent = "This message is to notify you that Activation with Part " + activationRequest.InventoryItem.PartNumber + " is approved and currently pending for training document";
                                        }
                                        else
                                        {
                                            emailReceiver = GetUserList(RoleEnum._PERSONNEL);
                                            log.Description = "Activation Approved by " + controlUser.email + " and activated";

                                            formattedEmailContent = "This message is to notify you that Activation request with Part " + activationRequest.InventoryItem.PartNumber + " is approved, please proceed with oracle process";
                                        }

                                        formData.Add("Remark", string.Empty);

                                    }
                                    else if (!activationRequest.IsCancel && status == ActivationStatusEnum._REJECT.GetId())
                                    {
                                        //handle case when user reject activation request or training document
                                        emailSubject = "Rejected Activation Request for Part - " + activationRequest.InventoryItem.PartNumber;

                                        if (!string.IsNullOrEmpty(activationRequest.TrainingDocument))
                                        {
                                            formData.Remove("Status");
                                            formData.Add("Status", ActivationStatusEnum._PENDING_TRAINING_RECORD.GetId().ToString());
                                            log.Description = "Activation training document rejected by " + controlUser.email;

                                            formattedEmailContent = "This message is to notify you that Activation Training document with Part " + activationRequest.InventoryItem.PartNumber + " is rejected";

                                            if (!string.IsNullOrEmpty(formData["Remark"]))
                                            {
                                                formattedEmailContent += " due to " + formData["Remark"];
                                            }
                                        }
                                        else
                                        {
                                            log.Description = "Activation rejected by " + controlUser.email;

                                            formattedEmailContent = "This message is to notify you that Activation request with Part " + activationRequest.InventoryItem.PartNumber + " is rejected";

                                            if (!string.IsNullOrEmpty(formData["Remark"]))
                                            {
                                                formattedEmailContent += " due to " + formData["Remark"];
                                            }
                                        }

                                        emailReceiver = new List<string> { activationRequest.ActivationUser.email };
                                    }
                                }
                                else if (status == ActivationStatusEnum._ACTIVE.GetId() && activationRequest.IsCancel)
                                {
                                    //handle case when switch from cancel to enable
                                    NameValueCollection constructedObj = new NameValueCollection();
                                    constructedObj.Add("PartId", activationRequest.PartId.ToString());
                                    ValidateActivationRequest(constructedObj);
                                    formData.Remove("Status");
                                    formData.Add("IsCancel", bool.FalseString);

                                    log.Description = "Activation enabled by " + controlUser.email;
                                }
                            }
                            else if (status == ActivationStatusEnum._CANCELED.GetId() && !activationRequest.IsCancel)
                            {
                                //now cancel the activation
                                log.Description = "Activation canceled by " + controlUser.email;
                            }
                        }
                    }

                    result = DatabaseService.GetInstance().CreateOrSave<iqa_activation_request>(formData);

                    if (!result.IsSuccess())
                    {
                        throw new Exception(result.Message);
                    }
                    else
                    {
                        var activationId = int.Parse(result.ReturnObject.GetType().GetProperty("id").GetValue(result.ReturnObject).ToString());
                        if (isNewRecord)
                        {
                            iqa_activation_request activationRequest = dbContext.iqa_activation_request.SingleOrDefault(i => i.Id == activationId);

                            emailSubject = "Activation Request for Part - " + activationRequest.InventoryItem.PartNumber;
                            formattedEmailContent = "This message is to notify you that Activation Request with Part " + activationRequest.InventoryItem.PartNumber + " is created";

                            emailReceiver = GetUserList(RoleEnum._ENGINEER);

                            log.Description = "Created Activation for part " + activationRequest.InventoryItem.PartNumber + " (" + activationRequest.ActivationDate.ToString("yyyy-MM-dd HH:mm:ss") + ")";
                        }

                        log.ActivationId = activationId;
                        dbContext.iqa_activation_log.Add(log);
                        dbContext.SaveChanges();
                    }

                    if (emailReceiver != null && emailReceiver.Count != 0)
                    {
                        reqMsg.Subject = emailSubject;
                        reqMsg.To = emailReceiver;
                        reqMsg.Body = String.Format(File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/Template/Notification.html")),
                            formattedEmailContent,
                            Setting.GetInstance().GetStringValue(SettingEnum._HOST_URL) + "IQA/Main/Dashboard?MenuId=menuIQADashboard");

                        MailController.SendEmail(reqMsg);
                    }

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    result.Code = (int)HttpStatusCode.InternalServerError;
                    result.Message = CommonUtil.ExtractExceptionMessage(ex);
                }
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        private void ValidateActivationRequest(NameValueCollection postData)
        {
            try
            {
                IQAEntities iqaEntity = new IQAEntities();

                bool isNewRecord = string.IsNullOrEmpty(postData["Id"]);

                if (!string.IsNullOrEmpty(postData["PartId"]) && isNewRecord)
                {
                    int partId = int.Parse(postData["PartId"]);

                    int rejectStatusId = ActivationStatusEnum._REJECT.GetId();
                    int completeStatusId = ActivationStatusEnum._COMPLETED.GetId();
                    iqa_activation_request matchedRequest = iqaEntity.iqa_activation_request.
                        FirstOrDefault(i => i.PartId == partId && i.Status != completeStatusId && i.Status != rejectStatusId && !i.IsCancel);

                    if (matchedRequest != null)
                    {
                        throw new Exception("Another Activation request with same part is found");
                    }
                }

                if (string.IsNullOrEmpty(postData["PartId"]) && !string.IsNullOrEmpty(postData["PartNumber"]))
                {
                    var targetPartNumber = postData["PartNumber"].ToString();
                    if (iqaEntity.iqa_inventory_item.FirstOrDefault(i => i.PartNumber == targetPartNumber) == null)
                    {
                        throw new Exception("Invalid part provided");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public int ValidateInspectionRecordOwner(NameValueCollection postData)
        {
            int locatedActivationId = 0;

            try
            {
                IQAEntities iqaEntity = new IQAEntities();

                if (!string.IsNullOrEmpty(postData["PartNumber"]) && string.IsNullOrEmpty(postData["PartId"]))
                {
                    var targetPartNumber = postData["PartNumber"].ToString();
                    iqa_inventory_item item = iqaEntity.iqa_inventory_item.FirstOrDefault(i => i.PartNumber == targetPartNumber);

                    if (item == null)
                    {
                        throw new Exception("Invalid Part provided");
                    }

                    postData.Remove("PartId");
                    postData.Add("PartId", item.Id.ToString());
                }

                //checking for new record
                if (string.IsNullOrEmpty(postData["Id"]))
                {
                    int activeStatus = ActivationStatusEnum._ACTIVE.GetId();
                    int partId = int.Parse(postData["partId"].ToString());
                    DateTime inspectionDatetime = DateTime.ParseExact(postData["InspectionTime"].ToString(), "yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                    iqa_activation_request activationRequest = iqaEntity.iqa_activation_request
                        .SingleOrDefault(i => i.PartId == partId && i.Status == activeStatus && !i.IsCancel);

                    if (activationRequest == null)
                    {
                        throw new Exception("No activation request found");
                    }

                    ActivationRequestModel activationModel = activationRequest.ConvertFromDbModel();

                    if (activationRequest.ActivationDate > inspectionDatetime)
                    {
                        throw new Exception("Inspection date is earlier than activation date, Please check again");
                    }
                    else if (!activationModel.OracleSettingObject.KeyX1Q1R1)
                    {
                        throw new Exception("Oracle Data is not updated");
                    }

                    locatedActivationId = activationRequest.Id;
                }
                else
                {
                    //if it is existing record
                    int id = int.Parse(postData["Id"].ToString());
                    iqa_inspection_record record = iqaEntity.iqa_inspection_record.SingleOrDefault(i => i.Id == id);

                    if (record.ActivationRequest.Status != ActivationStatusEnum._ACTIVE.GetId() && record.ActivationRequest.Status != ActivationStatusEnum._PENDING_APPROVAL.GetId())
                    {
                        throw new Exception("Not allowed to update record, activation is in " + record.ActivationRequest.ActivationStatus.Name);
                    }

                    locatedActivationId = record.ActivationId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return locatedActivationId;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result CreateOrUpdateInspectionRecord(NameValueCollection postData, int userId)
        {
            Result result = new Result();

            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = IsolationLevel.ReadUncommitted;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                try
                {
                    IQAEntities dbContext = new IQAEntities();
                    Database.DbModel.u_user_informations controlUser = dbContext.u_user_informations.SingleOrDefault(i => i.user_id == userId);
                    iqa_activation_log log = new iqa_activation_log();
                    log.UserId = userId;
                    log.CreatedTime = DateTime.Now;

                    NameValueCollection formData = new NameValueCollection();

                    foreach (var ea in postData)
                    {
                        formData.Add(ea.ToString(), postData[ea.ToString()]);
                    }

                    int activationId = ValidateInspectionRecordOwner(formData);

                    iqa_activation_request activationRequest = dbContext.iqa_activation_request.SingleOrDefault(i => i.Id == activationId);

                    formData.Add("ActivationId", activationId.ToString());
                    log.ActivationId = activationId;

                    if (!string.IsNullOrEmpty(postData["AcceptedQty"]) && postData["AcceptedQty"] == postData["InspectedQty"])
                    {
                        formData.Remove("RejectedPartDescription");
                        formData.Remove("NmrNumber");
                        formData.Remove("RejectReason");
                        formData.Add("RejectedPartDescription", string.Empty);
                        formData.Add("NmrNumber", string.Empty);
                        formData.Add("RejectReason", string.Empty);
                    }

                    result = DatabaseService.GetInstance().CreateOrSave<iqa_inspection_record>(formData);

                    bool isNewRecord = string.IsNullOrEmpty(postData["Id"]);
                    log.Description = "Inspection record is " + (isNewRecord ? "added" : "editted") + " by " + controlUser.email;

                    if (result.IsSuccess())
                    {
                        //now we need to check if it is need to complete the activation to pending status
                        ActivationRequestModel parsedActivationRequest = activationRequest.ConvertFromDbModel();

                        NameValueCollection newFormData = new NameValueCollection();

                        if (parsedActivationRequest.TotalCummulativeCount == parsedActivationRequest.ConsecutiveLotCount + parsedActivationRequest.AdditionalConsecutiveLotCount)
                        {
                            if (activationRequest.Status == ActivationStatusEnum._ACTIVE.GetId())
                            {
                                newFormData.Add("Id", activationRequest.Id.ToString());
                                newFormData.Add("Status", ActivationStatusEnum._PENDING_APPROVAL.GetId().ToString());

                                log.Description = "Activation switched to pending state from pending approval due to update of inspection record by " + controlUser.email;
                            }
                        }
                        else if (activationRequest.Status == ActivationStatusEnum._PENDING_LOT_EXTENSION_APPROVAL.GetId())
                        {
                            //if it is not meeting cummulative count and it is in extension flow, reject this
                            throw new Exception("Not allowed to update due to activation is already being process and pending for LOT Extension");
                        }
                        else if (activationRequest.Status == ActivationStatusEnum._PENDING_APPROVAL.GetId())
                        {
                            //if it is in pending approval we put it back to active
                            newFormData.Add("Id", activationRequest.Id.ToString());
                            newFormData.Add("Status", ActivationStatusEnum._ACTIVE.GetId().ToString());

                            log.Description = "Activation switched to active state from pending approval due to update of inspection record by " + controlUser.email;
                        }

                        if (newFormData.Count != 0)
                        {
                            result = CreateOrUpdateActivationRequest(newFormData, userId);
                            if (!result.IsSuccess())
                            {
                                throw new Exception(result.Message);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception(result.Message);
                    }

                    dbContext.SaveChanges();
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    result.Code = (int)HttpStatusCode.InternalServerError;
                    result.Message = CommonUtil.ExtractExceptionMessage(ex);
                }
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result UpdateActivationOracleKey(int activationId, int oracleKey, int userId)
        {
            Result result = new Result();

            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = IsolationLevel.ReadUncommitted;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                try
                {
                    iqa_activation_log log = new iqa_activation_log();
                    log.UserId = userId;
                    log.ActivationId = activationId;
                    log.Description = "Oracle Key ";

                    IQAEntities dbContext = new IQAEntities();
                    var activation = dbContext.iqa_activation_request.SingleOrDefault(i => i.Id == activationId);
                    OracleSettingObject settingObject = JsonConvert.DeserializeObject<OracleSettingObject>(activation.OracleData);

                    NameValueCollection formData = new NameValueCollection();
                    formData.Add("Id", activation.Id.ToString());

                    if (oracleKey == OracleSettingObject._KEY_X1Q1R1)
                    {
                        settingObject.KeyX1Q1R1 = true;
                        log.Description += "X1Q1R1";
                        formData.Add("ActivationDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    }
                    else
                    {
                        settingObject.KeyX1WPR1 = true;
                        log.Description += "X1WPR1";
                        formData.Add("DeactivationDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    }

                    log.Description += "is updated";
                    log.CreatedTime = DateTime.Now;

                    formData.Add("OracleData", JsonConvert.SerializeObject(settingObject));

                    result = DatabaseService.GetInstance().CreateOrSave<iqa_activation_request>(formData);

                    if (result.IsSuccess())
                    {
                        dbContext.iqa_activation_log.Add(log);
                        dbContext.SaveChanges();
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    result.Code = (int)HttpStatusCode.InternalServerError;
                    result.Message = CommonUtil.ExtractExceptionMessage(ex);
                }
            }
            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result CreateOrUpdateDocumentUpdateRequest(int activationId, int userId, HttpPostedFile file)
        {
            Result result = new Result();
            try
            {
                IQAEntities dbContext = new IQAEntities();

                Database.DbModel.u_user_informations controlUser = dbContext.u_user_informations.SingleOrDefault(i => i.user_id == userId);
                iqa_activation_log log = new iqa_activation_log();
                log.UserId = userId;
                log.CreatedTime = DateTime.Now;
                log.Description = controlUser.email + " requested to update activation document";
                log.ActivationId = activationId;
                dbContext.iqa_activation_log.Add(log);
                NameValueCollection formattedData = new NameValueCollection();

                var relativePath = SaveFile(file, "ActivationDocumentUpdate");

                iqa_activation_document_update_request matchedRequest = dbContext.iqa_activation_document_update_request
                    .FirstOrDefault(i => i.ActivationId == activationId && i.Status == CommonUtil._STATUS_PENDING);

                if (matchedRequest != null)
                {
                    formattedData.Add("Id", matchedRequest.Id.ToString());

                    if (userId != matchedRequest.ActivationRequest.ActivationUserId)
                    {
                        formattedData.Add("TargetUserId", userId.ToString());
                    }
                }

                formattedData.Add("ActivationId", activationId.ToString());
                formattedData.Add("UserId", userId.ToString());
                formattedData.Add("Status", CommonUtil._STATUS_PENDING.ToString());
                formattedData.Add("Document", relativePath);

                result = DatabaseService.GetInstance().CreateOrSave<iqa_activation_document_update_request>(formattedData);

                if (result.IsSuccess())
                {
                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                result.Code = (int)HttpStatusCode.InternalServerError;
                result.Message = CommonUtil.ExtractExceptionMessage(ex);
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public Result UpdateDocumentRequestStatus(int activationId, bool status, string remark, int userId)
        {
            Result result = new Result();

            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = IsolationLevel.ReadUncommitted;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                try
                {
                    IQAEntities dbContext = new IQAEntities();

                    NameValueCollection formattedData = new NameValueCollection();

                    iqa_activation_document_update_request matchedRequest = dbContext.iqa_activation_document_update_request.FirstOrDefault(i => i.ActivationId == activationId && i.Status == 0);

                    if (matchedRequest == null)
                    {
                        throw new Exception("No request found, it might be approved by some other user");
                    }

                    formattedData.Add("Id", matchedRequest.Id.ToString());
                    formattedData.Add("ApproverId", userId.ToString());
                    formattedData.Add("Remark", remark);
                    formattedData.Add("Status", status ? CommonUtil._STATUS_APPROVED.ToString() : CommonUtil._STATUS_REJECTED.ToString());

                    result = DatabaseService.GetInstance().CreateOrSave<iqa_activation_document_update_request>(formattedData);

                    if (result.IsSuccess() && status)
                    {
                        iqa_activation_request activationRequest = dbContext.iqa_activation_request.SingleOrDefault(i => i.Id == matchedRequest.ActivationId);

                        Database.DbModel.u_user_informations controlUser = dbContext.u_user_informations.SingleOrDefault(i => i.user_id == userId);
                        iqa_activation_log log = new iqa_activation_log();
                        log.UserId = userId;
                        log.CreatedTime = DateTime.Now;
                        log.Description = controlUser.email + " approved document update request for activation - " + activationRequest.InventoryItem.PartNumber + "(" + activationRequest.ActivationDate.ToString("yyyy-MM-dd HH:mm") + ")";
                        log.ActivationId = activationRequest.Id;

                        NameValueCollection activationData = new NameValueCollection();
                        activationData.Add("Id", activationRequest.Id.ToString());
                        activationData.Add("DocumentVersion", (activationRequest.DocumentVersion + 1).ToString());
                        activationData.Add("RequestDocument", matchedRequest.Document);

                        if (matchedRequest.TargetUserId.HasValue)
                        {
                            activationData.Add("ActivationUserId", matchedRequest.TargetUserId.Value.ToString());
                        }

                        result = DatabaseService.GetInstance().CreateOrSave<iqa_activation_request>(activationData);

                        if (!result.IsSuccess())
                        {
                            throw new Exception(result.Message);
                        }
                        else
                        {
                            dbContext.iqa_activation_log.Add(log);
                            dbContext.SaveChanges();
                        }
                    }

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    result.Code = (int)HttpStatusCode.InternalServerError;
                    result.Message = CommonUtil.ExtractExceptionMessage(ex);
                }
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        private List<string> GetUserList(RoleEnum role)
        {
            List<string> userList = new List<string>();
            try
            {
                IQAEntities iqaEntity = new IQAEntities();
                string userQuery = "select u.email from [u_user_to_store_ui] b inner join [u_user_to_role_ui] c on " +
                                                              "b.[user_to_store_id] = c.[user_to_store_id] inner join[u_extended_roles_ui] d on c.extended_role_id = d.extended_role_id " +
                                                              "inner join [u_extended_custom_roles_ui] e on d.extended_custom_role_id = e.extended_custom_role_id " +
                                                              "left join dbo.u_user_informations as u on u.user_id = b.user_id " +
                                                              "where extended_custome_role_name = '" + role.GetKey() + "'";

                userList = iqaEntity.Database.SqlQuery<string>(userQuery).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return userList;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [DisableConcurrentExecution(timeoutInSeconds: 1800)]
        public Result InitializeReminder()
        {
            Result result = new Result();
            try
            {
                var link = Setting.GetInstance().GetStringValue(SettingEnum._HOST_URL) + "IQA/Main/Dashboard?MenuId=menuIQADashboard";
                List<gSendMailMsg> emailToBeGenerated = new List<gSendMailMsg>();
                IQAEntities iqaEntity = new IQAEntities();

                //check for requested but yet approve activation
                int requestActivationStatus = ActivationStatusEnum._REQUEST_ACTIVATION.GetId();
                List<iqa_activation_request> listOfActivationToBeRemind = iqaEntity.iqa_activation_request.Where(i => !i.IsCancel && i.Status == requestActivationStatus).ToList();

                foreach (var ea in listOfActivationToBeRemind)
                {
                    var dayDifferent = CommonUtil.GetDayDifferent(ea.UpdatedTime, DateTime.Now);
                    if (dayDifferent >= 1)
                    {
                        gSendMailMsg reqMsg = new gSendMailMsg();

                        reqMsg.Subject = "Reminder For Activation Review";
                        reqMsg.To = GetUserList(RoleEnum._ENGINEER);
                        reqMsg.Body = String.Format(File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/Template/Notification.html")),
                            "This is a reminder email to review activation request for part " + ea.InventoryItem.PartNumber,
                            link);

                        if (dayDifferent >= 3)
                        {
                            reqMsg.Cc = new List<string> { ea.ActivationUser.email };
                        }

                        emailToBeGenerated.Add(reqMsg);
                    }
                }

                //check for pending document
                int pendingTrainingDocumentActivationStatus = ActivationStatusEnum._PENDING_TRAINING_RECORD.GetId();
                List<iqa_activation_request> listOfActivationToUpdateTrainingDoc = iqaEntity.iqa_activation_request.Where(i => !i.IsCancel && i.Status == pendingTrainingDocumentActivationStatus).ToList();

                foreach (var ea in listOfActivationToUpdateTrainingDoc)
                {
                    var dayDifferent = CommonUtil.GetDayDifferent(ea.UpdatedTime, DateTime.Now);
                    if (dayDifferent >= 3)
                    {
                        gSendMailMsg reqMsg = new gSendMailMsg();

                        reqMsg.Subject = "Reminder For Activation Training Document";
                        reqMsg.To = new List<string> { ea.ActivationUser.email };
                        reqMsg.Body = String.Format(File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/Template/Notification.html")),
                            "This is a reminder email to update activation training request for part " + ea.InventoryItem.PartNumber,
                            link);

                        if (dayDifferent >= 5)
                        {
                            reqMsg.Cc = GetUserList(RoleEnum._ENGINEER);
                        }

                        emailToBeGenerated.Add(reqMsg);
                    }
                }

                //check for oracle key
                int activeActivationStatus = ActivationStatusEnum._ACTIVE.GetId();
                int completeActivationStatus = ActivationStatusEnum._COMPLETED.GetId();
                List<iqa_activation_request> listOfActivationToUpdateOracle = iqaEntity.iqa_activation_request.Where(i => !i.IsCancel &&
                    ((i.OracleData.Contains("\"KeyX1Q1R1\":false") && i.Status == activeActivationStatus) || (i.OracleData.Contains("\"KeyX1WPR1\":false") && i.Status == completeActivationStatus))).ToList();

                foreach (var ea in listOfActivationToUpdateOracle)
                {
                    var dayDifferent = CommonUtil.GetDayDifferent(ea.UpdatedTime, DateTime.Now);
                    if (dayDifferent >= 1)
                    {
                        gSendMailMsg reqMsg = new gSendMailMsg();

                        reqMsg.Subject = "Reminder For Activation Default SI In Oracle";
                        reqMsg.To = GetUserList(RoleEnum._PERSONNEL);
                        reqMsg.Body = String.Format(File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/Template/Notification.html")),
                            "This is a reminder email to update activation default SI in Oracle part " + ea.InventoryItem.PartNumber,
                            link);

                        if (dayDifferent >= 3)
                        {
                            reqMsg.Cc = GetUserList(RoleEnum._ENGINEER);
                        }

                        emailToBeGenerated.Add(reqMsg);
                    }
                }

                //check deactivation
                int pendingApprovalStatus = ActivationStatusEnum._PENDING_APPROVAL.GetId();
                List<iqa_activation_request> listOfDeactivation = iqaEntity.iqa_activation_request.Where(i => !i.IsCancel && i.Status == pendingApprovalStatus).ToList();

                foreach (var ea in listOfDeactivation)
                {
                    var dayDifferent = CommonUtil.GetDayDifferent(ea.UpdatedTime, DateTime.Now);
                    if (dayDifferent >= 3)
                    {
                        gSendMailMsg reqMsg = new gSendMailMsg();

                        reqMsg.Subject = "Reminder For Deactivation Request";
                        reqMsg.To = new List<string> { ea.ActivationUser.email };
                        reqMsg.Body = String.Format(File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/Template/Notification.html")),
                            "This is a reminder email to review deactivation for part " + ea.InventoryItem.PartNumber,
                            link);

                        if (dayDifferent >= 5)
                        {
                            reqMsg.Cc = GetUserList(RoleEnum._ENGINEER);
                        }

                        emailToBeGenerated.Add(reqMsg);
                    }
                }

                //check lot extension Request
                int pendingExtensionStatus = ActivationStatusEnum._PENDING_LOT_EXTENSION_APPROVAL.GetId();

                List<iqa_activation_request> listOfActivationWithLotExtension = iqaEntity.iqa_activation_request
                    .Where(i => !i.IsCancel && i.Status == pendingExtensionStatus).OrderByDescending(i => i.Id).ToList();

                foreach (var ea in listOfActivationWithLotExtension)
                {
                    var dayDifferent = CommonUtil.GetDayDifferent(ea.UpdatedTime, DateTime.Now);
                    if (dayDifferent >= 1)
                    {
                        var lotExtensionRequest = ea.LotExtensionRequest.OrderByDescending(i => i.Id).FirstOrDefault();
                        if (lotExtensionRequest != null)
                        {
                            gSendMailMsg reqMsg = new gSendMailMsg();

                            reqMsg.Subject = "Reminder For Lot Extension Request";
                            reqMsg.To = lotExtensionRequest.LotExtensionApproval.Select(i => i.Approver.email).ToList();
                            reqMsg.Body = String.Format(File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/Template/Notification.html")),
                                "This is a reminder email to review deactivation for part " + ea.InventoryItem.PartNumber,
                                link);

                            if (dayDifferent >= 3)
                            {
                                reqMsg.Cc = new List<string> { ea.ActivationUser.email };
                            }

                            emailToBeGenerated.Add(reqMsg);
                        }
                    }
                }

                foreach (var email in emailToBeGenerated)
                {
                    //MailController.SendEmail(email);
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Code = (int)HttpStatusCode.InternalServerError;
            }

            return result;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [DisableConcurrentExecution(timeoutInSeconds: 1800)]
        public Result GenerateMonthlyReport()
        {
            Result result = new Result();
            try
            {
                //check if it is the day to generate report
                if (Setting.GetInstance().GetIntegerValue(SettingEnum._MONTHLY_REPORT_DAY) == int.Parse(DateTime.Now.ToString("dd")))
                {
                    string tableData = "<table style='border: 1px solid #000000;border-collapse: collapse;'><thead><tr><th style='border: 1px solid #000000; padding:5px'>Date</th>" +
                        "<th style='border: 1px solid #000000; padding:5px'>NMR#</th><th style='border: 1px solid #000000; padding:5px'>Engineer</th><th style='border: 1px solid #000000; padding:5px'>Part Number</th>" +
                        "<th style='border: 1px solid #000000; padding:5px'>Part Description</th><th style='border: 1px solid #000000; padding:5px'>Reject Description</th><th style='border: 1px solid #000000; padding:5px'>Reject Qty</th></tr></thead><tbody>";
                    IQAEntities iqaEntity = new IQAEntities();
                    Dictionary<string, List<int>> monthAgainstRejectQty = new Dictionary<string, List<int>>();
                    System.Data.DataTable table = new CommonController().GetData("execute petafield_develop.dbo.[IQA_FailInspectionData]", iqaEntity.Database.Connection.ConnectionString);

                    foreach (System.Data.DataRow ea in table.Rows)
                    {
                        var month = ea["Month"].ToString();
                        var rejectQty = int.Parse(ea["RejectQty"].ToString());
                        if (monthAgainstRejectQty.ContainsKey(month))
                        {
                            monthAgainstRejectQty[month].Add(rejectQty);
                        }
                        else
                        {
                            monthAgainstRejectQty.Add(month, new List<int> { rejectQty });
                        }

                        tableData += "<tr><td style='border: 1px solid #000000; padding:5px'>" + ea["InspectionDate"] + "</td><td style='border: 1px solid #000000; padding:5px'>" + ea["NmrNumber"] + "</td><td style='border: 1px solid #000000; padding:5px'>" + ea["Engineer"] + "</td><td style='border: 1px solid #000000; padding:5px'>" + ea["PartNumber"] +
                            "</td><td style='border: 1px solid #000000; padding:5px'>" + ea["Description"] + "</td><td style='border: 1px solid #000000; padding:5px'>" + ea["RejectReason"] + "</td><td style='border: 1px solid #000000; padding:5px'>" + ea["RejectQty"] + "</td></tr>";
                    }

                    tableData += "</tbody></table>";

                    DateTime startDate = DateTime.ParseExact(DateTime.Now.Date.ToString("yyyy") + "-11", "yyyy-MM", CultureInfo.InvariantCulture);
                    DateTime currentDate = DateTime.ParseExact(DateTime.Now.Date.ToString("yyyy-MM"), "yyyy-MM", CultureInfo.InvariantCulture);

                    if (DateTime.Now.Date < startDate)
                    {
                        startDate = startDate.AddYears(-1);
                    }

                    Dictionary<string, int> reportData = new Dictionary<string, int>();
                    while (currentDate >= startDate)
                    {
                        var monthKey = startDate.ToString("MMM yyyy");
                        var sumOfData = 0;

                        if (monthAgainstRejectQty.ContainsKey(monthKey))
                        {
                            sumOfData = monthAgainstRejectQty[monthKey].Sum();
                        }

                        reportData.Add(monthKey, sumOfData);

                        startDate = startDate.AddMonths(1);
                    }

                    var chart = new System.Web.UI.DataVisualization.Charting.Chart();
                    System.Web.UI.DataVisualization.Charting.Series series = new System.Web.UI.DataVisualization.Charting.Series("FailedInspection");
                    series.ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Column;
                    series.IsValueShownAsLabel = true;
                    chart.Series.Add(series);

                    //define the chart area
                    System.Web.UI.DataVisualization.Charting.ChartArea chartArea = new System.Web.UI.DataVisualization.Charting.ChartArea();
                    chart.Series["FailedInspection"].Points.DataBindXY(reportData.Keys.ToArray(), reportData.Values.ToArray());
                    chart.Series["FailedInspection"].Color = System.Drawing.Color.Red;
                    chartArea.AxisX.MajorGrid.Enabled = false;
                    chartArea.AxisX.MinorGrid.Enabled = false;
                    chartArea.AxisY.MajorGrid.Enabled = false;
                    chartArea.AxisY.MinorGrid.Enabled = false;

                    chart.ChartAreas.Add(chartArea);

                    chart.Width = new System.Web.UI.WebControls.Unit(600, System.Web.UI.WebControls.UnitType.Pixel);
                    chart.Height = new System.Web.UI.WebControls.Unit(400, System.Web.UI.WebControls.UnitType.Pixel);
                    var reportImage = "";

                    using (MemoryStream stream = new MemoryStream())
                    {
                        chart.SaveImage(stream, System.Web.UI.DataVisualization.Charting.ChartImageFormat.Png);

                        //write the stream to a byte array
                        System.Drawing.Image img = System.Drawing.Image.FromStream(stream);

                        var directory = AppDomain.CurrentDomain.BaseDirectory + CommonUtil.GetActivationTempDirectory();
                        Directory.CreateDirectory(directory);
                        reportImage = directory + "\\image_" + CommonUtil.GetEpochTime() + ".png";

                        img.Save(reportImage);
                        Thread.Sleep(1000);
                    }

                    string dateOfMonth = Setting.GetInstance().GetIntegerValue(SettingEnum._MONTHLY_REPORT_DAY).ToString().PadLeft(2, '0');

                    string rawReceiver = Setting.GetInstance().GetStringValue(SettingEnum._MONTHLY_REPORT_TARGET_LIST);
                    string ccString = Setting.GetInstance().GetStringValue(SettingEnum._MONTHLY_REPORT_CC_LIST);

                    List<string> receiverList = rawReceiver.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    List<string> ccList = ccString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                    if (receiverList.Count != 0)
                    {
                        gSendMailMsg reqMsg = new gSendMailMsg();

                        reqMsg.Subject = "Monthly IQA Report " + DateTime.Now.ToString("MMM yyyy");
                        reqMsg.To = receiverList;
                        reqMsg.Cc = ccList;
                        reqMsg.Attach = new List<string> { reportImage };
                        reqMsg.Body = String.Format(File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/Template/Report.html")),
                            DateTime.Now.ToString("MMM yyyy"), tableData,
                            Setting.GetInstance().GetStringValue(SettingEnum._HOST_URL)+ "IQA/Main/Report?MenuId=menuIQAReport");

                        MailController.SendEmail(reqMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Code = (int)HttpStatusCode.InternalServerError;
            }

            return result;
        }
    }
}