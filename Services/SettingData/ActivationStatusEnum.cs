﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IQA.Services.SettingData
{
    /// <summary>
    /// Author : 01016781
    /// </summary>
    public class ActivationStatusEnum : Utils.Enum
    {
        private static int _index = 0;
        public static Dictionary<int, ActivationStatusEnum> _statusDic = new Dictionary<int, ActivationStatusEnum>();

        public static ActivationStatusEnum _ACTIVE = new ActivationStatusEnum(++_index, "Active");
        public static ActivationStatusEnum _PENDING_APPROVAL = new ActivationStatusEnum(++_index, "Pending Approval");
        public static ActivationStatusEnum _PENDING_LOT_EXTENSION_APPROVAL = new ActivationStatusEnum(++_index, "Pending Lot Extension Approval");
        public static ActivationStatusEnum _COMPLETED = new ActivationStatusEnum(++_index, "Completed");
        public static ActivationStatusEnum _PENDING_TRAINING_RECORD = new ActivationStatusEnum(++_index, "Pending Training Record");
        public static ActivationStatusEnum _REQUEST_ACTIVATION = new ActivationStatusEnum(++_index, "Request Activation");
        public static ActivationStatusEnum _REJECT = new ActivationStatusEnum(++_index, "Reject");
        public static ActivationStatusEnum _CANCELED = new ActivationStatusEnum(++_index, "Cancel");
        public static ActivationStatusEnum _PENDING_ORACLE_UPDATE = new ActivationStatusEnum(++_index, "Pending Oracle Update");

        private string _settingKey;

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public ActivationStatusEnum(int index, string settingKey)
            : base(index)
        {
            _settingKey = settingKey;
            _statusDic.Add(index, this);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public static ActivationStatusEnum GetStatusEnumByIndex(int index)
        {
            return _statusDic[index];
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public string GetKey()
        {
            return _settingKey;
        }
    }
}