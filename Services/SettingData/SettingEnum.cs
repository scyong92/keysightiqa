﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IQA.Services.SettingData
{
    public class SettingEnum : Utils.Enum
    {
        private static int _index = -1;
        public static Dictionary<string, SettingEnum> _settingDic = new Dictionary<string, SettingEnum>();

        public static SettingEnum _DEFAULT_LOT_EXTENSION_COUNT = new SettingEnum(++_index, "defaultLotExtensionCount", "5");
        public static SettingEnum _HOST_URL = new SettingEnum(++_index, "hostUrl", "https://manufacturing.png.is.keysight.com/");
        public static SettingEnum _MONTHLY_REPORT_DAY = new SettingEnum(++_index, "monthlyReportDay", "1");
        public static SettingEnum _MONTHLY_REPORT_TARGET_LIST = new SettingEnum(++_index, "monthlyReportTargetList", "sheng-chuan.yong@keysight.com");
        public static SettingEnum _MONTHLY_REPORT_CC_LIST = new SettingEnum(++_index, "monthlyReportCcList", "");

        private object _value;
        private string _settingKey;

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public SettingEnum(int index, string settingKey, object defaultValue)
            : base(index)
        {
            _settingKey = settingKey;
            _value = defaultValue;
            _settingDic.Add(settingKey, this);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public static List<SettingEnum> GetAllSettingKey()
        {
            return _settingDic.Values.ToList();
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public static SettingEnum GetSettingByString(string key)
        {
            return _settingDic[key];
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public object GetDefaultValue()
        {
            return _value;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public string GetKey()
        {
            return _settingKey;
        }
    }
}