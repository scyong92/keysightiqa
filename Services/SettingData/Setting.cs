﻿using IQA.Database;
using IQA.Database.CustomModel;
using IQA.Database.DbModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using Xcadia.Models;

namespace IQA.Services.SettingData
{
    public class Setting
    {
        private static Setting _instance;

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static Setting GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Setting();
            }

            return _instance;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public void Init()
        {
            IQAEntities iqaEntity = new IQAEntities();
            List<iqa_setting> parsedSettingList = iqaEntity.iqa_setting.ToList();

            foreach (var setting in SettingEnum.GetAllSettingKey())
            {
                iqa_setting locatedSetting = parsedSettingList.FirstOrDefault(i => i.Name == setting.GetKey());

                if (locatedSetting == null)
                {
                    locatedSetting = new iqa_setting();
                    locatedSetting.Name = setting.GetKey();
                    locatedSetting.Description = setting.GetKey();
                    locatedSetting.Value = setting.GetDefaultValue().ToString();
                    locatedSetting.UpdatedTime = DateTime.Now;
                    iqaEntity.iqa_setting.Add(locatedSetting);
                }
            }

            iqaEntity.SaveChanges();
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public string GetStringValue(SettingEnum settingEnum)
        {
            IQAEntities iqaEntity = new IQAEntities();
            string key = settingEnum.GetKey();
            SettingModel data = iqaEntity.iqa_setting.FirstOrDefault(i => i.Name == key).ConvertFromDbModel();
            return data.Value;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public int GetIntegerValue(SettingEnum settingEnum)
        {
            string value = GetStringValue(settingEnum);
            return string.IsNullOrEmpty(value) ? 0 : int.Parse(value);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public decimal GetDecimalValue(SettingEnum settingEnum)
        {
            string value = GetStringValue(settingEnum);
            return string.IsNullOrEmpty(value) ? 0 : decimal.Parse(value);
        }
    }
}