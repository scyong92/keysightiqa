﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IQA.Services.SettingData
{
    /// <summary>
    /// Author : 01016781
    /// </summary>
    public class RoleEnum : Utils.Enum
    {
        private static int _index = 0;
        public static Dictionary<int, RoleEnum> _statusDic = new Dictionary<int, RoleEnum>();

        public static RoleEnum _MANAGER = new RoleEnum(++_index, "iqa_module_manager");
        public static RoleEnum _INSPECTOR = new RoleEnum(++_index, "iqa_module_inspector");
        public static RoleEnum _ENGINEER = new RoleEnum(++_index, "iqa_module_engineer");
        public static RoleEnum _EMPLOYEE = new RoleEnum(++_index, "iqa_module_employee");
        public static RoleEnum _PERSONNEL = new RoleEnum(++_index, "iqa_module_personnel");
        public static RoleEnum _VISITOR = new RoleEnum(++_index, "iqa_module_visitor");

        private string _roleKey;

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public RoleEnum(int index, string settingKey)
            : base(index)
        {
            _roleKey = settingKey;
            _statusDic.Add(index, this);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public string GetKey()
        {
            return _roleKey;
        }
    }
}