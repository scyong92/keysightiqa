﻿using IQA.Database.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IQA.Database.CustomModel
{
    public class DatabaseModel
    {
        private static Dictionary<string, Type> _keyToTypeDic = new Dictionary<string, Type> { 
            { "iqa_setting", typeof(iqa_setting) },
            { "u_user_informations", typeof(u_user_informations) },
            { "iqa_inventory", typeof(iqa_inventory) },
            { "iqa_inventory_item", typeof(iqa_inventory_item) },
            { "iqa_manufacturer", typeof(iqa_manufacturer) },
            { "iqa_activation_request", typeof(iqa_activation_request) },
            { "iqa_activation_status", typeof(iqa_activation_status) },
            { "iqa_inspection_record", typeof(iqa_inspection_record) },
            { "iqa_lot_extension_request", typeof(iqa_lot_extension_request) },
            { "iqa_activation_document_update_request", typeof(iqa_activation_document_update_request) },
            { "iqa_part_location", typeof(iqa_part_location) },
            { "iqa_inspection_record_category", typeof(iqa_inspection_record_category) },
            { "iqa_activation_log", typeof(iqa_activation_log) },
            { "iqa_employee_manager", typeof(iqa_employee_manager) }
        };

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public static Type GetTypeByName(string name)
        {
            return _keyToTypeDic[name];
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public object this[string propertyName]
        {
            get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
            set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public virtual dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return null;
        }
    }
}