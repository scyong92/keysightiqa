﻿using IQA.Database.DbModel;
using System;

namespace IQA.Database.CustomModel
{
    public class InventoryItemModel
    {
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static InventoryItemModel ConvertModel(iqa_inventory_item manufacturer)
        {
            InventoryItemModel model = new InventoryItemModel();
            model.Id = manufacturer.Id;
            model.PartNumber = manufacturer.PartNumber;
            model.Status = manufacturer.Status;
            model.Description = manufacturer.Description;
            return model;
        }
    }
}