﻿using IQA.Database.DbModel;
using System;

namespace IQA.Database.CustomModel
{
    public class UserModel
    {
        public int user_id { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static UserModel ConvertModel(u_user_informations user)
        {
            UserModel model = new UserModel();
            model.user_id = user.user_id;
            model.email = user.email;
            model.phone = user.phone;
            model.address = user.address;
            return model;
        }
    }
}