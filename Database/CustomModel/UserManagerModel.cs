﻿using IQA.Database.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IQA.Database.CustomModel
{
    public class UserManagerModel
    {
        public int Id { get; set; }
        public string User { get; set; }
        public int UserId { get; set; }
        public string Manager { get; set; }
        public int ManagerId { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static UserManagerModel ConvertModel(iqa_employee_manager userManager)
        {
            UserManagerModel model = new UserManagerModel();
            model.Id = userManager.Id;
            model.UserId = userManager.UserId;
            model.ManagerId = userManager.ManagerId;
            model.User = userManager.User.email;
            model.Manager = userManager.Manager.email;
            return model;
        }
    }
}