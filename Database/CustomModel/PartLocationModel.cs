﻿using IQA.Database.DbModel;
using System;

namespace IQA.Database.CustomModel
{
    public class PartLocationModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public bool IsValid { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static PartLocationModel ConvertModel(iqa_part_location manufacturer)
        {
            PartLocationModel model = new PartLocationModel();
            model.Id = manufacturer.Id;
            model.Name = manufacturer.Name;
            model.Status = manufacturer.Status;
            model.IsValid = manufacturer.IsValid;
            model.Description = manufacturer.Description;
            return model;
        }
    }
}