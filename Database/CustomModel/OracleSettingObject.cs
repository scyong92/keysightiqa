﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IQA.Database.CustomModel
{
    /// <summary>
    /// Author : 01016781 Yong Sheng Chuan
    /// </summary>
    public class OracleSettingObject
    {
        public const int _KEY_X1Q1R1 = 1;
        public const int _KEY_X1WPR1 = 2;

        public bool KeyX1Q1R1 { get; set; }
        public bool KeyX1WPR1 { get; set; }
    }
}