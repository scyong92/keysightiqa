﻿using IQA.Database.DbModel;
using System;

namespace IQA.Database.CustomModel
{
    public class InspectionRecordCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static InspectionRecordCategoryModel ConvertModel(iqa_inspection_record_category category)
        {
            InspectionRecordCategoryModel model = new InspectionRecordCategoryModel();
            model.Id = category.Id;
            model.Name = category.Name;
            model.Status = category.Status;
            model.Description = category.Description;
            return model;
        }
    }
}