﻿using IQA.Database.DbModel;
using IQA.Services.SettingData;
using IQA.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IQA.Database.CustomModel
{
	/// <summary>
	/// Author : 01016781
	/// </summary>
    public class ActivationRequestModel
	{
        public int Id { get; set; }

		public string Status
		{
			get
			{
				if (OracleSettingObject != null && ((StatusId == ActivationStatusEnum._ACTIVE.GetId() && !OracleSettingObject.KeyX1Q1R1) || (StatusId == ActivationStatusEnum._COMPLETED.GetId() && !OracleSettingObject.KeyX1WPR1)))
                {
					return ActivationStatusEnum._PENDING_ORACLE_UPDATE.GetKey();
                }
                else
				{
					return ActivationStatusEnum.GetStatusEnumByIndex(StatusId).GetKey();
				}
			}
		}

		public int PartId { get; set; }
		public int InventoryId { get; set; }
		public int PartLocationId { get; set; }
		public int ActivationUserId { get; set; }
		public string PartNumber { get; set; }
        public string Inventory { get; set; }
		public string ActivationUser { get; set; }
		public string PartLocation { get; set; }
		public string ActivationTime { get; set; }
		public int ConsecutiveLotCount { get; set; }
		public int AdditionalConsecutiveLotCount { get; set; }
		public int TotalReceivedCount { get; set; }
		public int TotalCummulativeCount { get; set; }
		public string Remark { get; set; }
		public string RequestDocument { get; set; }
		public string TrainingDocument { get; set; }
		public string DeactivationTime { get; set; }
		public OracleSettingObject OracleSettingObject { get; set; }
		public List<InspectionRecordModel> InspectionRecordList { get; set; }
		public int StatusId { get; set; } = ActivationStatusEnum._ACTIVE.GetId();
		public string UpdatedTime { get; set; }
		public string CreatedTime { get; set; }
		public ActivationDocumentUpdateRequestModel PendingDocumentUpdate { get; set; }
		public ActivationLotExtensionRequestModel PendingLotUpdate { get; set; }
		public bool IsSafetyComponent { get; set; }

		/// <summary>
		/// 01016781
		/// </summary>
		internal static ActivationRequestModel ConvertModel(iqa_activation_request request, bool withDetail = false)
		{
			ActivationRequestModel model = new ActivationRequestModel();
			model.Id = request.Id;
			model.PartNumber = request.InventoryItem.PartNumber;
			model.Inventory = request.Inventory.Name;
			model.ActivationUser = request.ActivationUser.email;
			model.PartId = request.PartId;
			model.InventoryId = request.InventoryId;
			model.ActivationUserId = request.ActivationUserId;
			model.IsSafetyComponent = request.IsSafetyComponent;
			model.PartLocationId = request.PartLocationId;
			model.OracleSettingObject = JsonConvert.DeserializeObject<OracleSettingObject>(request.OracleData);

			model.ActivationTime = request.ActivationDate.ToString("yyyy-MM-dd HH:mm");
			model.DeactivationTime = request.DeactivationDate.HasValue ? request.DeactivationDate.Value.ToString("yyyy-MM-dd HH:mm") : string.Empty;
			model.UpdatedTime = request.UpdatedTime.ToString("yyyy-MM-dd HH:mm");
			model.CreatedTime = request.CreatedTime.ToString("yyyy-MM-dd HH:mm");
			model.Remark = request.Remark;
			model.RequestDocument = request.RequestDocument;
			model.TrainingDocument = request.TrainingDocument;
			model.PartLocation = request.PartLocation.Name;
			model.ConsecutiveLotCount = request.ConsecutiveLotCount;
			model.AdditionalConsecutiveLotCount = request.AdditionalConsecutiveLotCount;

			if (request.IsCancel)
			{
				model.StatusId = ActivationStatusEnum._CANCELED.GetId();
			}
			else if (model.OracleSettingObject != null && ((request.Status == ActivationStatusEnum._ACTIVE.GetId() && !model.OracleSettingObject.KeyX1Q1R1) || (request.Status == ActivationStatusEnum._COMPLETED.GetId() && !model.OracleSettingObject.KeyX1WPR1)))
			{
				model.StatusId = ActivationStatusEnum._PENDING_ORACLE_UPDATE.GetId();
			}
			else
			{
				model.StatusId = request.Status;
			}

			model.TotalReceivedCount = request.InspectionRecord.Count;

			IQueryable<iqa_inspection_record> allInspectionRecord = request.InspectionRecord.Where(i => i.Status).OrderByDescending(i => i.InspectionTime).AsQueryable();

			iqa_inspection_record lastFailedRecord = allInspectionRecord.FirstOrDefault(i => i.AcceptedQty < i.InspectedQty);

			DateTime? startingTime = (lastFailedRecord != null) ? (DateTime?)lastFailedRecord.InspectionTime : null;

			var filterInspectionRecord = allInspectionRecord.Where(i => startingTime.HasValue ? i.InspectionTime > startingTime.Value : true).ToList();

			model.TotalCummulativeCount = filterInspectionRecord.Where(i => string.IsNullOrEmpty(i.BatchNumber)).Count() + 
				filterInspectionRecord.Where(i=> !string.IsNullOrEmpty(i.BatchNumber)).Select(i => i.BatchNumber + "_" + i.ReceivedTime.ToString("yyyy-MM-dd") + "_" + i.PartId).Distinct().Count();

			iqa_activation_document_update_request documentRequest = request.ActivationDocumentUpdateRequest.FirstOrDefault(i => i.Status == CommonUtil._STATUS_PENDING);
			
			if (documentRequest != null)
			{
				model.PendingDocumentUpdate = documentRequest.ConvertFromDbModel();
			}

			iqa_lot_extension_request lotRequest = request.LotExtensionRequest.LastOrDefault(i => i.LotExtensionApproval.Any(k=>k.Status == CommonUtil._STATUS_PENDING));

			if (lotRequest != null)
			{
				model.PendingLotUpdate = lotRequest.ConvertFromDbModel();
			}

			if (withDetail)
            {
				List<object> rawRecordList = (List<object>)request.InspectionRecord.Select(i => i.ConvertFromDbModel(false)).ToList();
				model.InspectionRecordList = rawRecordList.Cast<InspectionRecordModel>().ToList();
			}

			return model;
		}
	}
}