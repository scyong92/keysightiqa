﻿using IQA.Database.DbModel;
using System;

namespace IQA.Database.CustomModel
{
    public class ActivationLotApprovalModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string User { get; set; }
        public string Remark { get; set; }
        public int Status { get; set; }
        public string ApprovalDate { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static ActivationLotApprovalModel ConvertModel(iqa_lot_extension_request_approval approval)
        {
            ActivationLotApprovalModel model = new ActivationLotApprovalModel();
            model.Id = approval.Id;
            model.UserId = approval.UserId;
            model.User = approval.Approver.email;
            model.Remark = approval.Remark;
            model.Status = approval.Status;
            model.ApprovalDate = approval.UpdatedTime.ToString("yyyy-MM-dd HH:mm:ss");
            return model;
        }
    }
}