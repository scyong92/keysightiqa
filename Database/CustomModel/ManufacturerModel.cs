﻿using IQA.Database.DbModel;
using System;

namespace IQA.Database.CustomModel
{
    public class ManufacturerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static ManufacturerModel ConvertModel(iqa_manufacturer manufacturer)
        {
            ManufacturerModel model = new ManufacturerModel();
            model.Id = manufacturer.Id;
            model.Name = manufacturer.Name;
            model.Status = manufacturer.Status;
            model.Description = manufacturer.Description;
            return model;
        }
    }
}