﻿using IQA.Database.DbModel;
using System;

namespace IQA.Database.CustomModel
{
    public class ActivationDocumentUpdateRequestModel
    {
        public int Id { get; set; }
        public int ActivationId { get; set; }
        public int UserId { get; set; }
        public Nullable<int> TargetUserId { get; set; }
        public Nullable<int> ApproverId { get; set; }
        public string User { get; set; }
        public string TargetUser { get; set; }
        public string Approver { get; set; }
        public int Status { get; set; }
        public string Document { get; set; }
        public string Remark { get; set; }
        public string CreatedTime { get; set; }
        public string UpdatedTime { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static ActivationDocumentUpdateRequestModel ConvertModel(iqa_activation_document_update_request request)
        {
            ActivationDocumentUpdateRequestModel model = new ActivationDocumentUpdateRequestModel();
            model.Id = request.Id;
            model.ActivationId = request.ActivationId;
            model.UserId = request.UserId;
            model.User = request.Requestor.email;
            
            if (request.Approver != null)
            {
                model.Approver = request.Approver.email;
            }

            if (request.TargetUser != null)
            {
                model.TargetUser = request.TargetUser.email;
            }

            model.Status = request.Status;
            model.Document = request.Document;
            model.Remark = request.Remark;
            model.CreatedTime = request.CreatedTime.ToString("yyyy-MM-dd HH:mm:ss");
            model.UpdatedTime = request.UpdatedTime.ToString("yyyy-MM-dd HH:mm:ss");
            return model;
        }
    }
}