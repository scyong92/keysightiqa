﻿using IQA.Database.DbModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IQA.Database.CustomModel
{
    public class ActivationLotExtensionRequestModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string User { get; set; }
        public int LotCount { get; set; }
        public string Remark { get; set; }
        public string CreatedTime { get; set; }
        public string UpdatedTime { get; set; }
        public List<ActivationLotApprovalModel> ApprovalList { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static ActivationLotExtensionRequestModel ConvertModel(iqa_lot_extension_request request)
        {
            ActivationLotExtensionRequestModel model = new ActivationLotExtensionRequestModel();
            model.Id = request.Id;
            model.UserId = request.UserId;
            model.ApprovalList = request.LotExtensionApproval.Select(i => ActivationLotApprovalModel.ConvertModel(i)).ToList();
            model.LotCount = request.LotCount;
            model.Remark = request.Remark;
            model.CreatedTime = request.CreatedTime.ToString("yyyy-MM-dd HH:mm:ss");
            model.UpdatedTime = request.UpdatedTime.ToString("yyyy-MM-dd HH:mm:ss");
            return model;
        }
    }
}