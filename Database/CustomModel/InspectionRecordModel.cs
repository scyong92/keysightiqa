﻿using IQA.Database.DbModel;
using System;

namespace IQA.Database.CustomModel
{
    public class InspectionRecordModel
    {
        public const string _RECORD_COMPLETED_STATUS = "Completed";
        public const string _RECORD_INCOMPLETED_STATUS = "Incomplete";
        public int Id { get; set; }
        public string RecordStatus
        {
            get
            {
                if (FailedQty != 0 && string.IsNullOrEmpty(NmrNumber))
                {
                    return _RECORD_INCOMPLETED_STATUS;
                }
                else
                {
                    return _RECORD_COMPLETED_STATUS;
                }
            }
        }
        public string ReceivedTime { get; set; }
        public string InspectionTime { get; set; }
        public int ActivationId { get; set; }
        public int PartId { get; set; }
        public int ManufacturerId { get; set; }
        public int RequestorId { get; set; }
        public int InspectorId { get; set; }
        public string PartNumber { get; set; }
        public string Manufacturer { get; set; }
        public string Requestor { get; set; }
        public string Inspector { get; set; }
        public string Category { get; set; }
        public int InspectedQty { get; set; }
        public int AcceptedQty { get; set; }
        public int FailedQty
        {
            get
            {
                return InspectedQty - AcceptedQty;
            }
        }
        public string BatchNumber { get; set; }
        public string GPBNumber { get; set; }
        public string MtiFormNumber { get; set; }
        public string Remark { get; set; }
        public string RejectedPartDescription { get; set; }
        public string RejectReason { get; set; }
        public string NmrNumber { get; set; }
        public string CreatedTime { get; set; }
        public string UpdatedTime { get; set; }

        public bool Status { get; set; }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        internal static InspectionRecordModel ConvertModel(iqa_inspection_record record)
        {
            InspectionRecordModel model = new InspectionRecordModel();
            model.Id = record.Id;
            model.ReceivedTime = record.ReceivedTime.ToString("yyyy-MM-dd HH:mm");
            model.InspectionTime = record.InspectionTime.ToString("yyyy-MM-dd HH:mm");
            model.ActivationId = record.ActivationId;
            model.BatchNumber = record.BatchNumber;
            model.GPBNumber = record.GPBNumber;
            model.MtiFormNumber = record.MtiFormNumber;
            model.Category = record.InspectionRecordCategory.Name;
            model.PartId = record.PartId;
            model.PartNumber = record.InventoryItem.PartNumber;
            model.Manufacturer = record.Manufacturer.Name;
            model.Requestor = record.Requestor.email;
            model.Inspector = record.Inspector.email;
            model.ManufacturerId = record.ManufacturerId;
            model.InspectorId = record.InspectorId;
            model.RequestorId = record.RequestorId;
            model.InspectedQty = record.InspectedQty;
            model.AcceptedQty = record.AcceptedQty;
            model.Remark = record.Remark;
            model.RejectedPartDescription = record.RejectedPartDescription;
            model.RejectReason = record.RejectReason;
            model.Status = record.Status;
            model.NmrNumber = record.NmrNumber; 
            model.UpdatedTime = record.UpdatedTime.ToString("yyyy-MM-dd HH:mm");
            model.CreatedTime = record.CreatedTime.ToString("yyyy-MM-dd HH:mm");
            return model;
        }
    }
}