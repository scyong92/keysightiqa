﻿using IQA.Database.DbModel;
using System;

namespace IQA.Database.CustomModel
{
    public class SettingModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string UpdatedTime { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static SettingModel ConvertModel(iqa_setting setting)
        {
            SettingModel model = new SettingModel();
            model.Id = setting.Id;
            model.Name = setting.Name;
            model.Value = setting.Value;
            model.Description = setting.Description;
            model.UpdatedTime = setting.UpdatedTime.ToString("yyyy-MM-dd HH:mm");
            return model;
        }
    }
}