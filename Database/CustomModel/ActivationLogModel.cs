﻿using IQA.Database.DbModel;
using System;

namespace IQA.Database.CustomModel
{
    public class ActivationLogModel
    {
        public int Id { get; set; }
        public string User { get; set; }
        public string Activation { get; set; }
        public string Description { get; set; }
        public string CreatedTime { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static ActivationLogModel ConvertModel(iqa_activation_log activationLog)
        {
            ActivationLogModel model = new ActivationLogModel();
            model.Id = activationLog.Id;
            model.User = activationLog.User.email;
            model.Activation = activationLog.Activation.InventoryItem.PartNumber + " - "+activationLog.Activation.ActivationDate.ToString("yyyy-MM-dd HH:mm:ss");
            model.Description = activationLog.Description;
            model.CreatedTime = activationLog.CreatedTime.ToString("yyyy-MM-dd HH:mm:ss");
            return model;
        }
    }
}