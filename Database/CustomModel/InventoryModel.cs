﻿using IQA.Database.DbModel;
using System;

namespace IQA.Database.CustomModel
{
    public class InventoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }

        /// <summary>
        /// 01016781
        /// </summary>
        internal static InventoryModel ConvertModel(iqa_inventory inventory)
        {
            InventoryModel model = new InventoryModel();
            model.Id = inventory.Id;
            model.Name = inventory.Name;
            model.Description = inventory.Description;
            model.Status = inventory.Status;
            return model;
        }
    }
}