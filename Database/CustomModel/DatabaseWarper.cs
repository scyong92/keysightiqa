﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web;
using Xcadia.Controllers.API;

namespace IQA.Database.CustomModel
{
    public class DatabaseWarper
    {
        CommonController _cc = new CommonController();
        private static DatabaseWarper _instance;

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static DatabaseWarper GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DatabaseWarper();
            }

            return _instance;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        /*public DatabaseModel ConvertFormValueToDbModel<T>(NameValueCollection formData)
        {
            DatabaseModel requestModel = (DatabaseModel)Activator.CreateInstance(typeof(T));
            foreach (PropertyInfo prop in requestModel.GetType().GetProperties())
            {
                string propName = prop.Name;

                if (!string.IsNullOrEmpty(formData[propName]))
                {
                    var formKey = formData[propName].ToString();
                    Type dataType = (Nullable.GetUnderlyingType(prop.PropertyType) != null) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType;

                    if (string.IsNullOrEmpty(formKey) && Nullable.GetUnderlyingType(prop.PropertyType) != null)
                    {
                        requestModel[prop.Name] = null;
                    }
                    else
                    {
                        requestModel[prop.Name] = TypeDescriptor.GetConverter(dataType).ConvertFromInvariantString(formKey);
                    }
                }
            }

            return requestModel;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        private List<DatabaseModel> InitObject(Type type, DataTable datatable)
        {
            List<DatabaseModel> dataList = new List<DatabaseModel>();
            foreach (DataRow ea in datatable.Rows)
            {
                DatabaseModel data = (DatabaseModel)Activator.CreateInstance(type);

                for (int j = 0; j < datatable.Columns.Count; j++)
                {
                    if (ea[datatable.Columns[j].ColumnName] != DBNull.Value)
                    {
                        data[datatable.Columns[j].ColumnName] = ea[datatable.Columns[j].ColumnName];
                    }
                }

                dataList.Add(data);
            }

            return dataList;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public DatabaseModel GetData<T>(string query)
        {
            DataTable datatable = _cc.GetData(query);

            DatabaseModel data = InitObject(typeof(T), datatable)[0];

            return data;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public List<DatabaseModel> GetDataList(string query, string table)
        {
            DataTable datatable = _cc.GetData(query);
            Type newType = DatabaseModel.GetTypeByTable(table);

            return InitObject(newType, datatable);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public List<DatabaseModel> GetDataList<T>(string query = "")
        {
            if (string.IsNullOrEmpty(query))
            {
                DatabaseModel data = (DatabaseModel)Activator.CreateInstance(typeof(T));
                query = "select * from dbo." + data.GetLinkedTable();
            }
            DataTable datatable = _cc.GetData(query);

            return InitObject(typeof(T), datatable);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public int PerformInsertOrUpdateWithValues(Dictionary<string, string> values, string table, int? targetId = null)
        {
            int affectedCount = 0;
            if (!targetId.HasValue)
            {
                PerformInsertAction(values, table);
            }
            else
            {
                PerformUpdateAction(values, table, targetId.Value);
            }

            return affectedCount;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public int PerformInsertOrUpdateWithModel(DatabaseModel model)
        {
            int affectedCount = 0;
            bool isUpdate = model["Id"] != null && int.Parse(model["Id"].ToString()) != 0;
            if (isUpdate)
            {
                affectedCount = PerformUpdate(model);
            }
            else
            {
                affectedCount = PerformInsert(model);
            }

            return affectedCount;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        private int PerformUpdate(DatabaseModel model)
        {
            int targetId = 0;
            Dictionary<string, string> valuesDic = new Dictionary<string, string>();
            foreach (PropertyInfo prop in model.GetType().GetProperties())
            {
                if (prop.Name == "Id")
                {
                    targetId = int.Parse(model["Id"].ToString());
                }
                if (prop.Name != "Id" && prop.Name != "Item")
                {
                    string valueString = ConvertDataToString(model[prop.Name], prop);

                    valuesDic.Add(prop.Name, valueString);
                }
            }

            return PerformUpdateAction(valuesDic, model.GetLinkedTable(), targetId);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public int PerformUpdateAction(Dictionary<string, string> values, string table, int targetId)
        { 
            string insertQuery = "update dbo." + table + " set ";
            foreach (var ea in values)
            {
                insertQuery += ea.Key + " = " + ea.Value + ",";
            }
            insertQuery = insertQuery.Substring(0, insertQuery.Length - 1);
            insertQuery += " where Id = " + targetId;

            return _cc.UpdateData(insertQuery);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        private int PerformInsert(DatabaseModel model)
        {
            Dictionary<string, string> valueList = new Dictionary<string, string>();
            foreach (PropertyInfo prop in model.GetType().GetProperties())
            {
                if (prop.Name != "Id" && prop.Name != "Item")
                {
                    string valueString = ConvertDataToString(model[prop.Name], prop);

                    if (!string.IsNullOrEmpty(valueString))
                    {
                        valueList.Add(prop.Name, valueString);
                    }
                }
            }

            return PerformInsertAction(valueList, model.GetLinkedTable());
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public int PerformInsertAction(Dictionary<string, string> valueList, string table)
        {
            string insertQuery = "insert into dbo." + table + " (";
            insertQuery += string.Join(",", valueList.Keys);
            insertQuery += ") values (" + string.Join(",", valueList.Values) + ")";

            return _cc.UpdateData(insertQuery);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public int PerformDelete(DatabaseModel model)
        {
            int affectedCount = 0;

            if (model["Id"] != null && int.Parse(model["Id"].ToString()) != 0)
            {
                var query = "delete from dbo." + model.GetLinkedTable() + " where Id = "+ int.Parse(model["Id"].ToString());
                _cc.DeleteData(query);
                affectedCount = 1;
            }

            return affectedCount;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public string ConvertDataToString(object data, PropertyInfo prop)
        {
            string convertedString = "";
            Type dataType = (Nullable.GetUnderlyingType(prop.PropertyType) != null) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType;

            if (dataType == typeof(double) || dataType == typeof(float) || dataType == typeof(int))
            {
                convertedString = data != null ? data.ToString() : String.Empty;
            }
            else if (dataType == typeof(DateTime))
            {
                convertedString = data != null ? "'"+((DateTime)data).ToString("yyyy-MM-dd HH:mm:ss")+"'" : Nullable.GetUnderlyingType(prop.PropertyType) != null ? "Null" : String.Empty;
            }
            else
            {
                convertedString = "'"+ (data != null ? data.ToString() : string.Empty) + "'";
            }

            return convertedString;
        }*/
    }
}