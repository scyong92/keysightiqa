//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IQA.Database
{
    using IQA.Database.CustomModel;
    using System;
    using System.Collections.Generic;
    
    public partial class iqa_setting : DatabaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public System.DateTime UpdatedTime { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return SettingModel.ConvertModel(this);
        }
    }
}
