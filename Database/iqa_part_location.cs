namespace IQA.Database
{
    using IQA.Database.CustomModel;
    using System;
    using System.Collections.Generic;
    
    public partial class iqa_part_location : DatabaseModel
    {
        public iqa_part_location()
        {
            this.iqa_activation_request = new HashSet<iqa_activation_request>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<iqa_activation_request> iqa_activation_request { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return PartLocationModel.ConvertModel(this);
        }
    }
}
