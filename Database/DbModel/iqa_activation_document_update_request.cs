namespace IQA.Database.DbModel
{
    using global::IQA.Database.CustomModel;
    using System;

    public partial class iqa_activation_document_update_request : DatabaseModel
    {
        public int Id { get; set; }
        public int ActivationId { get; set; }
        public int UserId { get; set; }
        public int? TargetUserId { get; set; }
        public int? ApproverId { get; set; }
        public int Status { get; set; }
        public string Document { get; set; }
        public string Remark { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }

        public virtual iqa_activation_request ActivationRequest { get; set; }
        public virtual u_user_informations Requestor { get; set; }
        public virtual u_user_informations TargetUser { get; set; }
        public virtual u_user_informations Approver { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return ActivationDocumentUpdateRequestModel.ConvertModel(this);
        }
    }
}
