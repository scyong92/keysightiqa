namespace IQA.Database.DbModel
{
    using global::IQA.Database.CustomModel;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class u_user_informations : DatabaseModel
    {
        public u_user_informations()
        {
            DocumentUpdateRequestor = new HashSet<iqa_activation_document_update_request>();
            DocumentUpdateTargetUser = new HashSet<iqa_activation_document_update_request>();
            DocumentUpdateApproval = new HashSet<iqa_activation_document_update_request>();
            ActivationRequest = new HashSet<iqa_activation_request>();
            InspectionRecordInspector = new HashSet<iqa_inspection_record>();
            InspectionRecordRequestor = new HashSet<iqa_inspection_record>();
            LotExtensionRequest = new HashSet<iqa_lot_extension_request>();
            LotExtensionRequestApproval = new HashSet<iqa_lot_extension_request_approval>();
            ActivationLog = new HashSet<iqa_activation_log>();
            EmployeeUser = new HashSet<iqa_employee_manager>();
        }

        [Key]
        public int user_id { get; set; }
        public Guid user_key { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string company_name { get; set; }
        public string division_name { get; set; }
        public string address { get; set; }
        public Guid country_id { get; set; }
        public DateTime? last_login_date { get; set; }
        public byte status { get; set; }
        public string employee_id { get; set; }
        public byte? subscription { get; set; }

        public virtual ICollection<iqa_activation_document_update_request> DocumentUpdateRequestor { get; set; }
        public virtual ICollection<iqa_activation_document_update_request> DocumentUpdateTargetUser { get; set; }
        public virtual ICollection<iqa_activation_document_update_request> DocumentUpdateApproval { get; set; }
        public virtual ICollection<iqa_activation_request> ActivationRequest { get; set; }
        public virtual ICollection<iqa_inspection_record> InspectionRecordInspector { get; set; }
        public virtual ICollection<iqa_inspection_record> InspectionRecordRequestor { get; set; }
        public virtual ICollection<iqa_lot_extension_request> LotExtensionRequest { get; set; }
        public virtual ICollection<iqa_lot_extension_request_approval> LotExtensionRequestApproval { get; set; }
        public virtual ICollection<iqa_activation_log> ActivationLog { get; set; }
        public virtual ICollection<iqa_employee_manager> EmployeeUser { get; set; }
        public virtual ICollection<iqa_employee_manager> EmployeeManager { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return UserModel.ConvertModel(this);
        }
    }
}
