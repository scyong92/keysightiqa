namespace IQA.Database.DbModel
{
    using global::IQA.Database.CustomModel;
    using System;
    using System.Collections.Generic;

    public partial class iqa_lot_extension_request : DatabaseModel
    {
        public iqa_lot_extension_request()
        {
            LotExtensionApproval = new HashSet<iqa_lot_extension_request_approval>();
        }

        public int Id { get; set; }
        public int ActivationId { get; set; }
        public int UserId { get; set; }
        public int LotCount { get; set; }
        public string Remark { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }

        public virtual iqa_activation_request ActivationRequest { get; set; }
        public virtual u_user_informations Requestor { get; set; }
        public virtual ICollection<iqa_lot_extension_request_approval> LotExtensionApproval { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return ActivationLotExtensionRequestModel.ConvertModel(this);
        }
    }
}
