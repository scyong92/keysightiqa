namespace IQA.Database.DbModel
{
    using System;
    using System.Collections.Generic;

    public partial class iqa_activation_status
    {
        public iqa_activation_status()
        {
            ActivationRequest = new HashSet<iqa_activation_request>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<iqa_activation_request> ActivationRequest { get; set; }
    }
}
