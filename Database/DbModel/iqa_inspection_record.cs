namespace IQA.Database.DbModel
{
    using global::IQA.Database.CustomModel;
    using System;

    public partial class iqa_inspection_record : DatabaseModel
    {
        public int Id { get; set; }
        public DateTime ReceivedTime { get; set; }
        public DateTime InspectionTime { get; set; }
        public int PartId { get; set; }
        public int ManufacturerId { get; set; }
        public int RequestorId { get; set; }
        public int InspectorId { get; set; }
        public int ActivationId { get; set; }
        public int InspectedQty { get; set; }
        public int AcceptedQty { get; set; }
        public int CategoryId { get; set; }
        public string MtiFormNumber { get; set; }
        public string GPBNumber { get; set; }
        public string BatchNumber { get; set; }
        public string Remark { get; set; }
        public string RejectedPartDescription { get; set; }
        public string RejectReason { get; set; }
        public string NmrNumber { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public bool Status { get; set; }

        public virtual iqa_activation_request ActivationRequest { get; set; }
        public virtual iqa_inspection_record_category InspectionRecordCategory { get; set; }
        public virtual u_user_informations Requestor { get; set; }
        public virtual iqa_manufacturer Manufacturer { get; set; }
        public virtual iqa_inventory_item InventoryItem { get; set; }
        public virtual u_user_informations Inspector { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return InspectionRecordModel.ConvertModel(this);
        }
    }
}
