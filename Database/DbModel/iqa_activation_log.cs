namespace IQA.Database.DbModel
{
    using global::IQA.Database.CustomModel;
    using System;

    public partial class iqa_activation_log : DatabaseModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ActivationId { get; set; }
        public string Description { get; set; }
        public DateTime CreatedTime { get; set; }

        public virtual iqa_activation_request Activation { get; set; }
        public virtual u_user_informations User { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return ActivationLogModel.ConvertModel(this);
        }
    }
}
