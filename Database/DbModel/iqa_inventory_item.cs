namespace IQA.Database.DbModel
{
    using global::IQA.Database.CustomModel;
    using System;
    using System.Collections.Generic;

    public partial class iqa_inventory_item : DatabaseModel
    {
        public iqa_inventory_item()
        {
            ActivationRequest = new HashSet<iqa_activation_request>();
            InspectionRecord = new HashSet<iqa_inspection_record>();
        }

        public int Id { get; set; }
        public string PartNumber { get; set; }
        public bool Status { get; set; }
        public string Description { get; set; }

        public virtual ICollection<iqa_activation_request> ActivationRequest { get; set; }
        public virtual ICollection<iqa_inspection_record> InspectionRecord { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return InventoryItemModel.ConvertModel(this);
        }
    }
}
