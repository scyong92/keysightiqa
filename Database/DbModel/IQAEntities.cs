using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace IQA.Database.DbModel
{
    public partial class IQAEntities : DbContext
    {
        public IQAEntities()
            : base("name=xcadia")
        {
        }

        public virtual DbSet<iqa_activation_document_update_request> iqa_activation_document_update_request { get; set; }
        public virtual DbSet<iqa_activation_request> iqa_activation_request { get; set; }
        public virtual DbSet<iqa_activation_status> iqa_activation_status { get; set; }
        public virtual DbSet<iqa_inspection_record> iqa_inspection_record { get; set; }
        public virtual DbSet<iqa_inspection_record_category> iqa_inspection_record_category { get; set; }
        public virtual DbSet<iqa_inventory> iqa_inventory { get; set; }
        public virtual DbSet<iqa_inventory_item> iqa_inventory_item { get; set; }
        public virtual DbSet<iqa_lot_extension_request> iqa_lot_extension_request { get; set; }
        public virtual DbSet<iqa_lot_extension_request_approval> iqa_lot_extension_request_approval { get; set; }
        public virtual DbSet<iqa_manufacturer> iqa_manufacturer { get; set; }
        public virtual DbSet<iqa_part_location> iqa_part_location { get; set; }
        public virtual DbSet<iqa_setting> iqa_setting { get; set; }
        public virtual DbSet<u_user_informations> u_user_informations { get; set; }
        public virtual DbSet<iqa_activation_log> iqa_activation_log { get; set; }
        public virtual DbSet<iqa_employee_manager> iqa_employee_manager { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<iqa_activation_document_update_request>().ToTable("iqa_activation_document_update_request", "dbo");
            modelBuilder.Entity<iqa_activation_request>().ToTable("iqa_activation_request", "dbo");
            modelBuilder.Entity<iqa_activation_status>().ToTable("iqa_activation_status", "dbo");
            modelBuilder.Entity<iqa_inspection_record>().ToTable("iqa_inspection_record", "dbo");
            modelBuilder.Entity<iqa_inspection_record_category>().ToTable("iqa_inspection_record_category", "dbo");
            modelBuilder.Entity<iqa_inventory>().ToTable("iqa_inventory", "dbo");
            modelBuilder.Entity<iqa_inventory_item>().ToTable("iqa_inventory_item", "dbo");
            modelBuilder.Entity<iqa_lot_extension_request>().ToTable("iqa_lot_extension_request", "dbo");
            modelBuilder.Entity<iqa_lot_extension_request_approval>().ToTable("iqa_lot_extension_request_approval", "dbo");
            modelBuilder.Entity<iqa_manufacturer>().ToTable("iqa_manufacturer", "dbo");
            modelBuilder.Entity<iqa_part_location>().ToTable("iqa_part_location", "dbo");
            modelBuilder.Entity<iqa_setting>().ToTable("iqa_setting", "dbo");
            modelBuilder.Entity<u_user_informations>().ToTable("u_user_informations", "dbo");
            modelBuilder.Entity<iqa_activation_log>().ToTable("iqa_activation_log", "dbo");
            modelBuilder.Entity<iqa_employee_manager>().ToTable("iqa_employee_manager", "dbo");

            modelBuilder.Entity<iqa_activation_document_update_request>().HasRequired(u => u.TargetUser).WithMany(u => u.DocumentUpdateTargetUser).HasForeignKey(u => u.TargetUserId);
            modelBuilder.Entity<iqa_activation_document_update_request>().HasRequired(u => u.Approver).WithMany(u => u.DocumentUpdateApproval).HasForeignKey(u => u.ApproverId);
            modelBuilder.Entity<iqa_activation_document_update_request>().HasRequired(u => u.Requestor).WithMany(u => u.DocumentUpdateRequestor).HasForeignKey(u => u.UserId);
            modelBuilder.Entity<iqa_activation_document_update_request>().HasRequired(u => u.ActivationRequest).WithMany(u => u.ActivationDocumentUpdateRequest).HasForeignKey(u => u.ActivationId);

            modelBuilder.Entity<iqa_activation_request>().HasRequired(u => u.ActivationStatus).WithMany(u => u.ActivationRequest).HasForeignKey(u => u.Status);
            modelBuilder.Entity<iqa_activation_request>().HasRequired(u => u.PartLocation).WithMany(u => u.ActivationRequest).HasForeignKey(u => u.PartLocationId);
            modelBuilder.Entity<iqa_activation_request>().HasRequired(u => u.ActivationUser).WithMany(u => u.ActivationRequest).HasForeignKey(u => u.ActivationUserId);
            modelBuilder.Entity<iqa_activation_request>().HasRequired(u => u.Inventory).WithMany(u => u.ActivationRequest).HasForeignKey(u => u.InventoryId);
            modelBuilder.Entity<iqa_activation_request>().HasRequired(u => u.InventoryItem).WithMany(u => u.ActivationRequest).HasForeignKey(u => u.PartId);

            modelBuilder.Entity<iqa_inspection_record>().HasRequired(u => u.ActivationRequest).WithMany(u => u.InspectionRecord).HasForeignKey(u => u.ActivationId);
            modelBuilder.Entity<iqa_inspection_record>().HasRequired(u => u.InspectionRecordCategory).WithMany(u => u.InspectionRecord).HasForeignKey(u => u.CategoryId);
            modelBuilder.Entity<iqa_inspection_record>().HasRequired(u => u.InventoryItem).WithMany(u => u.InspectionRecord).HasForeignKey(u => u.PartId);
            modelBuilder.Entity<iqa_inspection_record>().HasRequired(u => u.Manufacturer).WithMany(u => u.InspectionRecord).HasForeignKey(u => u.ManufacturerId);
            modelBuilder.Entity<iqa_inspection_record>().HasRequired(u => u.Inspector).WithMany(u => u.InspectionRecordInspector).HasForeignKey(u => u.InspectorId);
            modelBuilder.Entity<iqa_inspection_record>().HasRequired(u => u.Requestor).WithMany(u => u.InspectionRecordRequestor).HasForeignKey(u => u.RequestorId);

            modelBuilder.Entity<iqa_lot_extension_request>().HasRequired(u => u.ActivationRequest).WithMany(u => u.LotExtensionRequest).HasForeignKey(u => u.ActivationId);
            modelBuilder.Entity<iqa_lot_extension_request>().HasRequired(u => u.Requestor).WithMany(u => u.LotExtensionRequest).HasForeignKey(u => u.UserId);

            modelBuilder.Entity<iqa_lot_extension_request_approval>().HasRequired(u => u.LotExtensionRequest).WithMany(u => u.LotExtensionApproval).HasForeignKey(u => u.RequestId);
            modelBuilder.Entity<iqa_lot_extension_request_approval>().HasRequired(u => u.Approver).WithMany(u => u.LotExtensionRequestApproval).HasForeignKey(u => u.UserId);

            modelBuilder.Entity<iqa_activation_log>().HasRequired(u => u.Activation).WithMany(u => u.ActivationLog).HasForeignKey(u => u.ActivationId);
            modelBuilder.Entity<iqa_activation_log>().HasRequired(u => u.User).WithMany(u => u.ActivationLog).HasForeignKey(u => u.UserId);

            modelBuilder.Entity<iqa_employee_manager>().HasRequired(u => u.User).WithMany(u => u.EmployeeUser).HasForeignKey(u => u.UserId);
            modelBuilder.Entity<iqa_employee_manager>().HasRequired(u => u.Manager).WithMany(u => u.EmployeeManager).HasForeignKey(u => u.ManagerId);
        }
    }
}
