namespace IQA.Database.DbModel
{
    using global::IQA.Database.CustomModel;
    using System;
    using System.Collections.Generic;

    public partial class iqa_activation_request : DatabaseModel
    {
        public iqa_activation_request()
        {
            ActivationDocumentUpdateRequest = new HashSet<iqa_activation_document_update_request>();
            LotExtensionRequest = new HashSet<iqa_lot_extension_request>();
            InspectionRecord = new HashSet<iqa_inspection_record>();
            ActivationLog = new HashSet<iqa_activation_log>();
        }

        public int Id { get; set; }
        public int PartId { get; set; }
        public int InventoryId { get; set; }
        public int PartLocationId { get; set; }
        public int ActivationUserId { get; set; }
        public DateTime ActivationDate { get; set; }
        public DateTime? DeactivationDate { get; set; }
        public int ConsecutiveLotCount { get; set; }
        public int AdditionalConsecutiveLotCount { get; set; }
        public string Remark { get; set; }
        public string RequestDocument { get; set; }
        public string TrainingDocument { get; set; }
        public int DocumentVersion { get; set; }
        public int Status { get; set; }
        public bool IsCancel { get; set; }
        public bool IsSafetyComponent { get; set; }
        public string OracleData { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }

        public virtual ICollection<iqa_activation_document_update_request> ActivationDocumentUpdateRequest { get; set; }
        public virtual iqa_inventory Inventory { get; set; }
        public virtual iqa_inventory_item InventoryItem { get; set; }
        public virtual iqa_part_location PartLocation { get; set; }
        public virtual iqa_activation_status ActivationStatus { get; set; }
        public virtual u_user_informations ActivationUser { get; set; }
        public virtual ICollection<iqa_lot_extension_request> LotExtensionRequest { get; set; }
        public virtual ICollection<iqa_inspection_record> InspectionRecord { get; set; }
        public virtual ICollection<iqa_activation_log> ActivationLog { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return ActivationRequestModel.ConvertModel(this, withDetail);
        }
    }
}
