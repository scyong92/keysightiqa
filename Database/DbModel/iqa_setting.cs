namespace IQA.Database.DbModel
{
    using global::IQA.Database.CustomModel;
    using System;

    public partial class iqa_setting : DatabaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public DateTime UpdatedTime { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return SettingModel.ConvertModel(this);
        }
    }
}
