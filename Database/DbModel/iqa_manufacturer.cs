namespace IQA.Database.DbModel
{
    using global::IQA.Database.CustomModel;
    using System;
    using System.Collections.Generic;

    public partial class iqa_manufacturer : DatabaseModel
    {
        public iqa_manufacturer()
        {
            InspectionRecord = new HashSet<iqa_inspection_record>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public string Description { get; set; }

        public virtual ICollection<iqa_inspection_record> InspectionRecord { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return ManufacturerModel.ConvertModel(this);
        }
    }
}
