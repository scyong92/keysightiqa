namespace IQA.Database.DbModel
{
    using global::IQA.Database.CustomModel;
    using System;

    public partial class iqa_lot_extension_request_approval : DatabaseModel
    {
        public int Id { get; set; }
        public int RequestId { get; set; }
        public int UserId { get; set; }
        public int Status { get; set; }
        public string Remark { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }

        public virtual iqa_lot_extension_request LotExtensionRequest { get; set; }
        public virtual u_user_informations Approver { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return ActivationLotApprovalModel.ConvertModel(this);
        }
    }
}
