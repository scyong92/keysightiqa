namespace IQA.Database.DbModel
{
    using global::IQA.Database.CustomModel;
    using System;
    using System.Collections.Generic;

    public partial class iqa_employee_manager : DatabaseModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ManagerId { get; set; }

        public virtual u_user_informations User { get; set; }
        public virtual u_user_informations Manager { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return UserManagerModel.ConvertModel(this);
        }
    }
}
