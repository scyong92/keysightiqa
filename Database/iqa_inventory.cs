//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IQA.Database
{
    using IQA.Database.CustomModel;
    using System;
    using System.Collections.Generic;
    
    public partial class iqa_inventory : DatabaseModel
    {
        public iqa_inventory()
        {
            this.iqa_activation_request = new HashSet<iqa_activation_request>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<iqa_activation_request> iqa_activation_request { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return InventoryModel.ConvertModel(this);
        }
    }
}
