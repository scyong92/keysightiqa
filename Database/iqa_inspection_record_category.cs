namespace IQA.Database
{
    using IQA.Database.CustomModel;
    using System;
    using System.Collections.Generic;
    
    public partial class iqa_inspection_record_category : DatabaseModel
    {
        public iqa_inspection_record_category()
        {
            this.iqa_inspection_record = new HashSet<iqa_inspection_record>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public string Description { get; set; }

        public virtual ICollection<iqa_inspection_record> iqa_inspection_record { get; set; }

        public override dynamic ConvertFromDbModel(bool withDetail = false)
        {
            return InspectionRecordCategoryModel.ConvertModel(this);
        }
    }
}
