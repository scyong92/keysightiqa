﻿using IQA.Services.SettingData;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using Xcadia.Controllers.API;
using Xcadia.Models;

namespace IQA.Utils
{
    /// <summary>
    /// Author : 01016781
    /// </summary>
    public class CommonUtil
    {
        public const string _IQA_ACTIVATION_DOCUMENT_NAME = "ATM-15-Q0046.docx";

        public const int _STATUS_PENDING = 0;
        public const int _STATUS_APPROVED = 1;
        public const int _STATUS_REJECTED = 2;

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public static int GetEpochTime()
        {
            TimeSpan currentTimeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1);
            return (int)currentTimeSpan.TotalSeconds;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public static string GetActivationDirectory(string month = "")
        {
            string path = GetActiveDirectory() + "\\Activation";
            if (!string.IsNullOrEmpty(month))
            {
                path += "\\"+ month ;
            }

            return path;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public static string GetActivationDocumentUpdateDirectory(string month = "")
        {
            string path = GetActiveDirectory() + "\\ActivationDocumentUpdate";
            if (!string.IsNullOrEmpty(month))
            {
                path += "\\" + month;
            }

            return path;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public static string GetActivationTempDirectory()
        {
            return GetActiveDirectory() + "\\temp"; ;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public static string GetActiveDirectory()
        {
            return "\\Document";
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public static string ExtractExceptionMessage(Exception ex)
        {
            string message = "";
            Exception currentException = ex;

            while (currentException.InnerException != null)
            {
                message += currentException.InnerException.Message + "\n";
                currentException = currentException.InnerException;
            }

            message += ex.Message;

            return message;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public static int GetDayDifferent(DateTime dateFrom, DateTime dateTo)
        {
            int dayCount = 0;

            DateTime startDate = dateFrom.Date;
            while (dateTo.Date > startDate)
            {
                if (!startDate.DayOfWeek.Equals(DayOfWeek.Saturday) && !startDate.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    dayCount++;
                }

                startDate = startDate.AddDays(1);
            }

            return dayCount;
        }
    }
}