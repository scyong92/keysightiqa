﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IQA.Utils
{
    public class Enum : IComparable
    {
        private int _id;
        private int _hashCode;
        private static Dictionary<Type, Dictionary<int, Enum>> _enumClassToIdToInstanceMapMap = new Dictionary<Type, Dictionary<int, Enum>>();

        /// <summary>
        /// Author : 01016781
        /// </summary>
        protected Enum(int id)
        {
            _id = id;
            _hashCode = 37 * GetType().GetHashCode() + id;

            Dictionary<int, Enum> instanceMap = new Dictionary<int, Enum>();
            if (!_enumClassToIdToInstanceMapMap.ContainsKey(GetType()))
            {
                instanceMap = new Dictionary<int, Enum>();
                _enumClassToIdToInstanceMapMap.Add(GetType(), instanceMap);
            }
            else
            {
                instanceMap = _enumClassToIdToInstanceMapMap[GetType()];
            }
            instanceMap.Add(id, this);
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public override int GetHashCode()
        {
            return _hashCode;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public override bool Equals(Object rhs)
        {
            if (rhs == null)
            {
                return false;
            }

            if (rhs == this)
            {
                return true;
            }

            if (GetType() == rhs.GetType())
            {
                Enum enumeration = (Enum)rhs;
                if (_id == enumeration._id)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public int GetId()
        {
            return _id;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public int CompareTo(object obj)
        {
            Enum enumeration = (Enum)obj;
            if (_id > enumeration._id)
            {
                return 1;
            }

            if (_id < enumeration._id)
            {
                return -1;
            }

            return 0;
        }
    }
}