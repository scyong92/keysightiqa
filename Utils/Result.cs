﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IQA.Utils
{
    public class Result
    {
        public int Code { get; set; }

        public string Message { get; set; }

        public object ReturnObject { get; set; }

        public Result()
        {

        }

        public Result(int code, string message)
        {
            this.Code = code;
            this.Message = message;
        }

        public bool IsSuccess()
        {
            return Code == 0;
        }
    }
}