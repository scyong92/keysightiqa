﻿using IQA.Database;
using IQA.Database.CustomModel;
using IQA.Services;
using IQA.Utils;
using Petafield_MT;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebMatrix.WebData;
using Xcadia.Controllers.API;

namespace IQA.Controllers.API
{
    [Authorize]
    public class ConfigurationController : ApiController
    {
        /// <summary>
        /// Author : 01016781
        /// </summary>
        public object GetData(string table, int offset = 0, int limit = 0)
        {
            object returnData = null;
            try
            {
                Result result = DatabaseService.GetInstance().GetDataByName(table, offset, limit);
                if (result.IsSuccess())
                {
                    if (limit != 0)
                    {
                        returnData = result.ReturnObject;
                    }
                    else
                    {
                        wMessage msg = new wMessage();
                        msg.Body = result.ReturnObject;
                        returnData = msg;
                    }
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
            }

            return returnData;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpGet]
        public wMessage GetDataById(string table, string id)
        {
            wMessage wMsg = new wMessage();
            try
            {
                Result result = DatabaseService.GetInstance().GetSingleDataByName(table,id);
                if (result.IsSuccess())
                {
                    wMsg.Body = result.ReturnObject;
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpPost]
        [AccessQueueFilter]
        public wMessage CreateOrUpdateConfiguration(string database)
        {
            wMessage wMsg = new wMessage();

            try
            {
                Result result = ConfigurationService.GetInstance().CreateOrSaveConfiguration(database, HttpContext.Current.Request.Form);

                if (result.IsSuccess())
                {
                    wMsg.Status = wMessageStatus.Passed;
                    wMsg.Description = "Save Configuration Successfully";
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpPost]
        [AccessQueueFilter]
        public wMessage UpdatePartFromPlm()
        {
            wMessage wMsg = new wMessage();

            try
            {
                Result result = ConfigurationService.GetInstance().PerformSyncInventoryItemFromPlm();

                if (result.IsSuccess())
                {
                    wMsg.Status = wMessageStatus.Passed;
                    wMsg.Description = result.Message;
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpGet]
        public wMessage GetManager()
        {
            wMessage wMsg = new wMessage();

            try
            {
                Result result = ConfigurationService.GetInstance().GetManager(WebSecurity.CurrentUserId);

                if (result.IsSuccess())
                {
                    wMsg.Status = wMessageStatus.Passed;
                    wMsg.Body = result.ReturnObject;
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpGet]
        public wMessage GetActivationDocument()
        {
            wMessage wMsg = new wMessage();
            try
            {
                var sourceFile = "C:\\WebApps\\uploads\\HTC\\ATM-15-Q0046\\" + CommonUtil._IQA_ACTIVATION_DOCUMENT_NAME;
                string documentDir = AppDomain.CurrentDomain.BaseDirectory + CommonUtil.GetActiveDirectory();
                var targetFile = documentDir + "\\" + CommonUtil._IQA_ACTIVATION_DOCUMENT_NAME;

                FileInfo sourceFileInfo = new FileInfo(sourceFile);
                FileInfo targetFileInfo = new FileInfo(targetFile);

                if (!targetFileInfo.Exists || !sourceFileInfo.LastWriteTime.Equals(targetFileInfo.LastWriteTime))
                {
                    File.Copy(sourceFileInfo.FullName, targetFileInfo.FullName, true);
                }

                wMsg.Body = "\\"+Request.RequestUri.Segments[1].Replace("/","")+CommonUtil.GetActiveDirectory() + "\\" + CommonUtil._IQA_ACTIVATION_DOCUMENT_NAME;
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }
            return wMsg;
        }
    }
}