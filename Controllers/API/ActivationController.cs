﻿using IQA.Database;
using IQA.Database.CustomModel;
using IQA.Model;
using IQA.Services;
using IQA.Services.SettingData;
using IQA.Utils;
using Petafield_MT;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using Xcadia.Controllers.API;

namespace IQA.Controllers.API
{
    [Authorize]
    public class ActivationController : ApiController
    {
        private readonly CommonController _controller = new CommonController();

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpPost]
        public wMessage GetInspectionData([FromBody] InpectionFilterModel filter)
        {
            wMessage wMsg = new wMessage();

            try
            {
                Result result = ActivationService.GetInstance().GetInspectionData(filter);

                if (result.IsSuccess())
                {
                    wMsg.Body = result.ReturnObject;
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpPost]
        public wMessage GetActivationData([FromBody] InpectionFilterModel filter)
        {
            wMessage wMsg = new wMessage();

            try
            {
                Result result = ActivationService.GetInstance().GetActivationData(filter);

                if (result.IsSuccess())
                {
                    wMsg.Body = result.ReturnObject;
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpPost]
        [AccessQueueFilter]
        public wMessage CreateOrUpdateInspectionRecord()
        {
            wMessage wMsg = new wMessage();

            try
            {
                Result result = ActivationService.GetInstance().CreateOrUpdateInspectionRecord(HttpContext.Current.Request.Form, WebSecurity.CurrentUserId);

                if (result.IsSuccess())
                {
                    wMsg.Status = wMessageStatus.Passed;
                    wMsg.Description = "Save Inspection Successfully";
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpPost]
        [AccessQueueFilter]
        public wMessage CreateOrUpdateActivation()
        {
            wMessage wMsg = new wMessage();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                HttpPostedFile trainingFile = null;
                HttpPostedFile requestFile = null;

                if (httpRequest.Files.Count > 0)
                {
                    requestFile = httpRequest.Files["documentTxt"];
                    trainingFile = httpRequest.Files["trainingDocumentTxt"];
                }

                Result result = ActivationService.GetInstance().CreateOrUpdateActivationRequest(httpRequest.Form, WebSecurity.CurrentUserId, requestFile, trainingFile);

                if (result.IsSuccess())
                {
                    wMsg.Status = wMessageStatus.Passed;
                    wMsg.Description = "Save Activation Request Successfully";
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpPost]
        [AccessQueueFilter]
        public wMessage CreateOrUpdateRequestDocument()
        {
            wMessage wMsg = new wMessage();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                HttpPostedFile file = null;

                if (httpRequest.Files.Count > 0)
                {
                    file = httpRequest.Files[0];
                }

                int activationId = int.Parse(httpRequest.Form["ActivationId"].ToString());
                Result result = ActivationService.GetInstance().CreateOrUpdateDocumentUpdateRequest(activationId, WebSecurity.CurrentUserId, file);

                if (result.IsSuccess())
                {
                    wMsg.Status = wMessageStatus.Passed;
                    wMsg.Description = "Save Document Update Request Successfully";
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpPost]
        [AccessQueueFilter]
        public wMessage UpdateRequestDocumentStatus(wMessage msgReceived)
        {
            wMessage wMsg = new wMessage();
            dynamic data = msgReceived.Body;
            try
            {
                string remark = data.Remark;
                bool status = data.Status;
                int activationId = data.ActivationId;

                Result result = ActivationService.GetInstance().UpdateDocumentRequestStatus(activationId, status, remark, WebSecurity.CurrentUserId);

                if (result.IsSuccess())
                {
                    wMsg.Status = wMessageStatus.Passed;
                    wMsg.Description = "Save Document Update Request Successfully";
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpPost]
        [AccessQueueFilter]
        public wMessage RequestActivationLotExtension()
        {
            wMessage wMsg = new wMessage();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                Result result = ActivationService.GetInstance().CreateOrUpdateActivationRequest(httpRequest.Form, WebSecurity.CurrentUserId);

                if (result.IsSuccess())
                {
                    wMsg.Status = wMessageStatus.Passed;
                    wMsg.Description = "Save Activation Request Successfully";
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpPost]
        [AccessQueueFilter]
        public wMessage ActivationLotRequestRespond(wMessage msgReceived)
        {
            wMessage wMsg = new wMessage();
            dynamic content = msgReceived.Body;

            try
            {
                NameValueCollection formData = new NameValueCollection();

                formData.Add("Id", (string)(content["Id"]));
                formData.Add("ExtensionStatus", ((bool)content["ExtensionStatus"]).ToString());
                formData.Add("Remark", content["Remark"].ToString());

                if ((bool)content["ExtensionStatus"])
                {
                    formData.Add("Status", ActivationStatusEnum._ACTIVE.GetId().ToString());
                }
                else
                {
                    formData.Add("Status", ActivationStatusEnum._PENDING_APPROVAL.GetId().ToString());
                }

                Result result = ActivationService.GetInstance().CreateOrUpdateActivationRequest(formData, WebSecurity.CurrentUserId);

                if (result.IsSuccess())
                {
                    wMsg.Status = wMessageStatus.Passed;
                    wMsg.Description = "Save Activation Request Successfully";
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpGet]
        public wMessage ReadActivationLog(int activationId)
        {
            wMessage wMsg = new wMessage();
            try
            {
                Result result = ActivationService.GetInstance().GetActivationLog(activationId);

                if (result.IsSuccess())
                {
                    wMsg.Body = result.ReturnObject;
                }
                else
                {
                    throw new Exception(result.Message);
                }
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        [HttpPost]
        [AccessQueueFilter]
        public wMessage UpdateOracleData(int activationId, int key)
        {
            wMessage wMsg = new wMessage();
            try
            {
                Result result = ActivationService.GetInstance().UpdateActivationOracleKey(activationId, key, WebSecurity.CurrentUserId);

                wMsg.Status = result.IsSuccess() ? wMessageStatus.Passed : wMessageStatus.Fail;
                wMsg.Description = result.IsSuccess() ? "Update successfully" : "Update failed ("+result.Message+")";
            }
            catch (Exception ex)
            {
                wMsg.Status = wMessageStatus.Fail;
                wMsg.Description = ex.Message;
            }

            return wMsg;
        }
    }
}