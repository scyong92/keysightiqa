﻿using IQA.Database;
using IQA.Database.CustomModel;
using IQA.Database.DbModel;
using IQA.Services;
using IQA.Services.SettingData;
using IQA.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Xcadia.Controllers;
using Xcadia.Controllers.API;

namespace IQA.Controllers
{
    /// <summary>
    /// Author : 01016781
    /// </summary>
    [ViewFilter]
    public class MainController : BaseController
    {
        /// <summary>
        /// Author : 01016781
        /// </summary>
        public ActionResult Configuration(string MenuID)
        {
            return View();
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public ActionResult Report(string MenuID)
        {
            return View();
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public ActionResult Dashboard(string MenuID)
        {
            return View();
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public ActionResult Activation(string MenuID)
        {
            ViewBag.ConsecutiveCount = Setting.GetInstance().GetIntegerValue(SettingEnum._DEFAULT_LOT_EXTENSION_COUNT);
            InitFormSelectionData();
            return View();
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public ActionResult Inspection(string MenuID)
        {
            InitFormSelectionData();
            List<object> activationList = (List<object>)DatabaseService.GetInstance().GetData<iqa_activation_request>().ReturnObject;
            ViewBag.Activation = activationList.Cast<ActivationRequestModel>().ToList();

            return View();
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        private void InitFormSelectionData()
        {
            List<object> inventoryList = (List<object>)DatabaseService.GetInstance().GetData<iqa_inventory>().ReturnObject;
            List<object> manufacturerList = (List<object>)DatabaseService.GetInstance().GetData<iqa_manufacturer>().ReturnObject;
            List<object> userList = (List<object>)DatabaseService.GetInstance().GetData<u_user_informations>().ReturnObject;
            List<object> partLocation = (List<object>)DatabaseService.GetInstance().GetData<iqa_part_location>().ReturnObject;
            List<object> recordCategory = (List<object>)DatabaseService.GetInstance().GetData<iqa_inspection_record_category>().ReturnObject;

            ViewBag.Inventory = inventoryList.Cast<InventoryModel>().ToList();
            ViewBag.Manufacturer = manufacturerList.Cast<ManufacturerModel>().ToList();
            ViewBag.User = userList.Cast<UserModel>().ToList();
            ViewBag.PartLocation = partLocation.Cast<PartLocationModel>().ToList();
            ViewBag.RecordCategory = recordCategory.Cast<InspectionRecordCategoryModel>().ToList();
        }
    }
}