﻿using Newtonsoft.Json;
using Petafield_MT;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using WebMatrix.WebData;

namespace IQA.Controllers
{
    /// <summary>
    /// Author : 01016781
    /// </summary>
    public class AccessQueueFilter : ActionFilterAttribute
    {
        public static ConcurrentDictionary<int, int> _userList = new ConcurrentDictionary<int, int>();

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!_userList.TryAdd(WebSecurity.CurrentUserId, WebSecurity.CurrentUserId))
            {
                wMessage wMsg = new wMessage();
                wMsg.Description = "Server is processing your request, please try again later";
                wMsg.Status = wMessageStatus.Fail;

                actionContext.Response = new HttpResponseMessage(HttpStatusCode.OK) 
                { 
                    Content = new StringContent(JsonConvert.SerializeObject(wMsg), 
                    System.Text.Encoding.UTF8, "application/json") 
                };
            }
        }

        /// <summary>
        /// Author : 01016781
        /// </summary>
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            _userList.TryRemove(WebSecurity.CurrentUserId, out _);
        }
    }
}