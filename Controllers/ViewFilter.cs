﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using Xcadia.Controllers;
using Xcadia.Controllers.API;
using Xcadia.Models;

namespace IQA.Controllers
{
    /// <summary>
    /// Author : 01016781
    /// </summary>
    public class ViewFilter : ActionFilterAttribute
    {
        /// <summary>
        /// Author : 01016781
        /// </summary>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = filterContext.Controller as BaseController;
            var menuId = (string)filterContext.ActionParameters["MenuID"];
            filterContext.Controller.ViewBag.erUrl = controller.CheckUserRole(menuId);
            controller.GetMenuInfo(menuId);

            List<string> roleList = new List<string>();
            CommonController cc = new CommonController();
            DataTable dataTable = null;

            if (Account.CheckSuperAdmin(WebSecurity.CurrentUserId) || Account.CheckSuperUserAll(WebSecurity.CurrentUserId))
            {
                dataTable = cc.GetData("select extended_custome_role_name from dbo.u_extended_custom_roles_ui where extended_custome_role_name like '%iqa_module%'");
            }
            else
            {
                dataTable = cc.GetData("select extended_custome_role_name from [u_user_to_store_ui] b " +
                                                "inner join [u_user_to_role_ui] c " +
                                                "on b.[user_to_store_id] = c.[user_to_store_id] " +
                                                "inner join[u_extended_roles_ui] d " +
                                                "on c.extended_role_id = d.extended_role_id " +
                                                "inner join[u_extended_custom_roles_ui] e " +
                                                "on d.extended_custom_role_id = e.extended_custom_role_id " +
                                                "where user_id = " + WebSecurity.CurrentUserId + " and extended_custome_role_name like '%iqa_module%'");
            }

            foreach (DataRow row in dataTable.Rows)
            {
                roleList.Add(row["extended_custome_role_name"].ToString().Trim());
            }

            filterContext.Controller.ViewBag.RoleList = roleList;

            base.OnActionExecuting(filterContext);
        }
    }
}