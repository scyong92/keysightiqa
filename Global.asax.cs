﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebMatrix.WebData;
using Xcadia.Controllers;
using Xcadia.Controllers.API;
using Xcadia.Global;

namespace IQA
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = false;
            WebSecurity.InitializeDatabaseConnection("xcadia", "UserProfile", "UserId", "UserName", autoCreateTables: true);
        }
        protected void Application_Error()
        {
            if (Context.IsCustomErrorEnabled)
                ShowCustomErrorPage(Server.GetLastError());
        }

        private void ShowCustomErrorPage(Exception exception)
        {
            var httpException = exception as HttpException ?? new HttpException(500, "Internal Server Error", exception);

            //Response.Clear();
            var routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("fromAppErrorEvent", true);

            switch (httpException.GetHttpCode())
            {
                case 401:
                    routeData.Values.Add("action", "HttpError401");
                    break;

                case 403:
                    routeData.Values.Add("action", "HttpError403");
                    break;

                case 404:
                    routeData.Values.Add("action", "HttpError404");
                    break;

                case 500:
                    routeData.Values.Add("action", "HttpError500");
                    break;

                default:
                    routeData.Values.Add("action", "GeneralError");
                    routeData.Values.Add("httpStatusCode", httpException.GetHttpCode());
                    break;
            }

            Server.ClearError();

            IController controller = new ErrorController();
            controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }

        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            HttpContext.Current.Items["renderStartTime"] = DateTime.Now;
            FilterSaveLog output = new FilterSaveLog(HttpContext.Current, Response.Filter);
            Response.Filter = output;
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e) //Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            try
            {
                new AccessibleController()._Application_AuthenticateRequest(typeof(WebApiApplication).Namespace + "/", Request.QueryString["MenuID"]);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                //do nothing;
            }
        }
        protected void Application_EndRequest()
        {
            new LogController().LogWebApi2();
        }
    }
}
