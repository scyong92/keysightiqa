﻿using System;
using System.Collections.Generic;

namespace IQA.Model
{
    /// <summary>
    /// Author : 01016781
    /// </summary>
    public class InpectionFilterModel : FilterableModel
    {
        public List<int> InventoryId { get; set; }
        public string PartNumber { get; set; }
        public List<int> ManufacturerId { get; set; }
        public List<int> ActivationId { get; set; }
        public int? UserId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}