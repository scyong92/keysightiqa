﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IQA.Model
{
    public class FilterableModel
    {
        public int offset { get; set; } = 0;
        public int limit { get; set; } = 0;
    }
}