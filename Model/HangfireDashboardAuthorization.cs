﻿using Hangfire.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;
using Xcadia.Models;

namespace IQA.Model
{
    /// <summary>
    /// Author : 01016781 Yong Sheng Chuan
    /// </summary>
    public class HangfireDashboardAuthorization : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            bool superadmin = Account.CheckSuperAdmin(WebSecurity.CurrentUserId);
            bool superuser = Account.CheckSuperUserAll(WebSecurity.CurrentUserId);

            return superadmin || superuser || WebSecurity.CurrentUserId == 1204;
        }
    }
}